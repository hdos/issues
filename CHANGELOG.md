# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [563] 2025-3-12
### Fixed
- Fixed gnomes/children skel compatibility

## [562] 2025-3-5
### Fixed
- Updated bank component IDs
- Updated cache

## [561] 2025-3-3
### Changed
- Restored the 'walk-over' animation used in agility shortcuts
- Restored 21 NPCs related to the **Hazeel cult** and **Prince Ali rescue** quests
- Added **POH themes** for Hosidius, Wood Cabin, Halloween and Twisted (Civitas and Canifis now also work)
  - Remastered terrain for Hosidius
  - Improved indoor flooring for Twisted
### Fixed
- Improved the lighting for delta floors in dynamic regions (fixes **Akkha**'s lighting while retaining POH lighting)
- Restored mappings shift for **POH mounted heads**
- Fixed some downgraded guards during the **Children of the Sun** quest
- Fixed POH atmosphere not working correctly


## [560] 2025-3-2
### Added
- **Engine**: Added capabilities for lights to work in true dynamic regions (affects POH, NMZ) (experimental)
### Changed
- #### **HD Pack 30**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack targets **Myth's Guild and Oog'log** (corsair cove) and **Player-owned houses** as a bonus! The following changes are introduced:
  - Remastered all **Objects** related to Southern Feldip hills
    - Created many new objects to fit the different themes
    - Added many new additional entities
    - Remastered over 200 **Player-owned-house** assets *(Mostly thanks to `J3`)*
      - Restored pet houses and feeders
  - Remastered **NPCs**
    - Added many new pirate NPCs used by Tock, Ithoi, Gnocci, Arsen, Yusuf, Francois, Haris and others!
    - *Some of the Myth's guild NPCs are only partially affected, and Ogres have not yet been restored due to technical constraints*
  - Re-invigorated the **Oo'glog theme** into Corsair Cove
    - Remastered the old terrain
    - Restored **Eucalyptus and Achey trees** to be compatible with OS maps
    - Restored much of the **plantlife** that was present outside of Oo'glog
    - Restored **Oo'glog**, its chasms and its props to the best of our abilities
  - Fully remastered the **Myth's Guild/Corsair Dungeon**
    - Upgraded wallkits and stalagmites
    - Heavily improved terrain complexity and added details
    - New atmospheres
    - Upgraded fires and lighting
    - Fixed several bugs related to rendering and object mapping within the dungeon
  - Added underworld and **shorelines**
  - Restored (part of) **Feldip Hills** beach as part of this remaster
  - Made several improvements to the recent **Player-Owned-House** update:
    - Remastered **additional wall/roof-kit assets** to complete the Halloween, Twisted, Hosidius, Varlamore (Civitas), Christmas (Snowy Cabin) and Morytania (Canifis) themes
    - Added **lighting and torches** for Canifis
    - Heavily improved **Terrain** complexity for the remastered Varlamore Savannah and the restored Morytania outdoor areas
      - Additionally, restored some of the **indoor flooring**
    - Added atmospheres to the Varlamore and Morytania themes (applies to your own selected theme)
  - Added easter eggs
- Restored Barkers' **robe bottoms** (grey, red, yellow, teal, purple)
  - Remastered **Sandwich lady bottoms** (light pink)
- **Engine**: **Animations** can now be looped indefinitely (fixes resting animations)
- Reduced fog in Occulus Orb mode (freecam)
- Restored some misc fishing spots (fixes shadows)
- Restored castle wars doors on server reboot
### Fixed
- Clue scrolls that are dropped by the player now use the 60-minute timer
- **MapFilter**: Added **Myth's Guild dungeon**, Fixed **Taverley Dungeon**
-**ExtendedNPC**: Improved static for **Corsair Cove**
- Fixed a bug that prevented a map for new oldschool POH templates to be affected by any restoration changes

## [559] 2025-2-19
### Fixed
- Fix skeletal animations not loading

## [558] 2025-2-18
### Changed
- Update **proto** to 229
- Restored misc OS wallkit objects used by unrestored areas
### Fixed
- Fixed a web model used in the **Sarachnis** boss encounter to be closer in size to its oldschool model (1x1), which may impact other usages such as ToB
- Fixed an issue that caused the sound engine to be overloaded, cutting out sound effects during the **Scurrius** boss encounter (fixes hdos/issues#2310)
  - Reduced the swamp bubble's sound radius from 7 to 3
- Fully restored the **Ectofungus** once again (fixes hdos/issues#2312)
  - Added a border between the Araxyte spider caves and the Ectofungus to hide the maps even when MapFilter is disabled or when in the login screen
- Update map compatability for **Falador**
- Fixed scripts for new Royal Titans **prayers** (fixes quick-prayer and tooltips)

## [557] 2025-2-12
### Changed
- Allow **immersive toggles** to be un-toggled even when you don't have the item in your possession
  - *Although the immersive toggles are intended to be only accessible to those who have unlocked the equivalent of the respective item, it felt unfair to those trying to go back on this decision after the item was no longer in their possession.*
- Remastered additional misc **Royal titans** assets (such as pet chat-heads)
- Restored all variants of the **Monk of Zamorak** (fixes hdos/issues#2282)
- Updated some item IDs, Discord status areas and Slayer task fire/ice giants
- When defending against dragons with a shield that protects you from dragonfire, the defend animation will now actually utilize your shield (fixes hdos/issues#2257)
  - *This was not a bug, and happens in Oldschool to this day. This change was made to reflect more accurately what animation was used in HD*
- Restored the following NPCs:
  - **Bert** in Yanille
  - **Slave**, **Lady Servil** and **Justin Servil** by the Fight Arena
- Add new Hunter's loot sack (supplies) to LootTrackerPlugin
### Fixed
- Fixed a tunnel in the **Dorgeshuun Mines** that would disappear or be oriented in the wrong direction depending on the quest state you were in
- **King Black Dragon**, **Brother Tranquility** and **Zombie Monks** will no longer attempt to speak with a HD chat-head animation, as these cases were intended to stay oldschool (fixes hdos/issues#2291)
  - Additionally, all existing cases like the Demon Butler, Postie Pete and others will now use their original expression, instead of falling back to one animation, despite being forced to downgrade.
- Fixed a **stitching** issue caused by the combination of Ferocious Gloves and the Abyssal Whip (fixes hdos/issues#2301)
- Fixed an **ExtendedNPCs** that caused it to spawn the wrong npc under rare circumstances (fixes hdos/issues#2296)
- Disguised the new Taverley dungeon (loose railing) demon shortcut as a stalagmite to better fit the restored maps (fixes hdos/issues#2295)
- Fixed a bug where **area sound effects** would not be heard under certain conditions (fixes hdos/issues#2293 hdos/issues#2305 hdos/issues#2229)
- Fixed a missing trim on the remastered treasure trail platebody (g)'s (fixes hdos/issues#2289)
- Fixed the following for **Theatre of Blood**:
  - Updated maps (upped brightness slightly)
  - **Pestilent Bloat**'s shadows are now more visible
  - Added forced decor to prevent the floor from going black when ground decorations are turned off
- Missing plank bridges in **The Warrens** have been re-added
- Updated map compatability for **Yanille** shortcuts (fixes hdos/issues#2271)
  - Fixed Z-Fighting and improved pointlight on northern wall
  - Improved odd indent in the wall that is used for the grappling on the southern wall
- Fixed transparent **chat dialogues** window
- Update client scripts (fixes crash)
- Update game cache

## [556] 2025-2-7
### Changed
- #### **HD Pack 29**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack targets the **Royal Titans** and introduces the following changes:
  - Remastered all **Objects**, **NPCs**, **Projectiles** and **Items**
    - Improved offsets for the new amulet(s)
  - Overhauled new **icy and lava caves**, added back the void that prevents you to see through walls
    - Upgraded lava
    - Improved terrain complexity
    - Added new floor decos
    - Added **lights**
    - Added **shorelines** to the pool of ice
    - Disabled shadow casting on transluscent or light-emitting objects
### Fixed
- Updated **Asgarnia Ice Dungeon** and **Karamja Volcano** restore map compatability
  - Fixed **MapFilter**
### Removed
- Lumbridge's leagues-related map changes have been removed

## [555] 2025-2-5
### Added
- Bank XP Value plugin
### Changed
- Updated plugins:
  - Ammo (Supports dizanas quiver)
  - Razor kebbit tracking (IDs updated)
  - Item prices
  - Combat level (Precise combat level)
  - Item stats (Extended anti-venom+ and sunlit bracers)
  - Time tracking reminder (Fixed farming contracts, supports celastrus tree and anima patch)
### Removed
- Hard reset the **Asgarnian Ice Dungeon** (temporarily disabled any restoration changes)

## [554] 2025-1-31
### Fixed
- Permit OS Submenu parents/branches to be selectable

## [553] 2025-1-29
### Fixed
- Fix bank searching

## [552] 2025-1-29
### Fixed
- Fix chat scrolling

## [551] 2025-1-29
### Changed
- Restored resting animations (including variations)
### Fixed
- Updated the gameplay settings section for immersive toggle hints
- Updated game components and scripts that were causing buggy UI and plugin behaviour

## [550] 2025-1-22
### Added
- Finish support for OS sub menus 

## [549] 2025-1-22
### Fixed
- Fixed bug in 548 causing undefined UI behavior

## [548] 2025-1-22
### Fixed
- Fixed os submenus from crashing the client (working on re-enabling them)

## [547] 2025-1-21
### Changed
- #### **HD Pack 28**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack targets **Cerberus** and introduces the following changes:
  - Remastered all **Objects**, **NPCs**, **Projectiles** and **Items**
    - Some particle effects have been added to graphics (static projectiles)
    - Added an **Immersive toggle** to swap between Glacor boots (Remastered OS), Glacor boots (Faithful) and Cerberus' boots (OS)
  - Overhauled **caves** and heavily improved terrain complexity
    - Upgraded lava
    - Added various improved **blending** to edges
    - Added void to hide map edge cut-offs, and removed random terrain in the void
    - Added new floor deco's
  - Added **lights** and immersive **atmosphere**
  - Added **shorelines** to the soul river

## [546] 2025-1-18
### Changed
- Remastered the broken and part variants of the **Leagues** Dwarf Multicannon
### Fixed
- Improved flickering particles on MacOS

## [545] 2025-1-15
### Fixed
- Fix portal nexus not spawning correctly

## [544] 2025-1-15
### Changed
- Updated **proto** to 228
- Remastered the latest update's **Lobby icons**



## [544] 2025-1-15
### Changed
- Update protocol to 228

## [543] 2025-1-8
### Changed
- Restored **Wizard Mizgog** and **Archmage Sedridor**
### Fixed
- Fixed miss-map for **decorative armour** (gold) (locked)
- Updated **cache** with the latest game update
### Removed
- **Christmas** has been removed

## [542] 2024-12-31
### Changed
- Some changes have been made related to the client's **memory**:
  - Upped the default jvm heap size to 1GB (from 512mb) for 64-bit systems to better deal with high stress
  - Draw debug now displays the maximum heap allocation (xmx argument in the launcher config) underneath the FPS meter
  - The memory debug no longer becomes red above 512mb and can now appear green, yellow or red depending on the amount of memory used
    - *The indicator turning red is now an indication that you may want to up the limit (type `/increasememory` in our Discord on how to do this*
### Fixed
- **ExtendedNPCs**: Fixed some overpopulation in Varlamore

## [541] 2024-12-28
### Fixed
- Staged some missing assets intended for v540

## [540] 2024-12-28
### Changed
- #### **HD Pack 27**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack targets Zeah: **Port Piscarilius** and introduces the following changes:
  - Remastered hundreds of **objects**
    - Improved some existing assets
    - Added new underwater objects
  - Remastered numerous **NPCs**, and added new chat-heads to existing ones
  - Restored assets where possible
  - Remastered the **Angler** and **Spirit Angler** outfits
  - Added immersive **atmosphere**
    - Added fog that intensifies towards the sea
    - Improved sun angle and brightness
    - Added pointlights
  - Completely overhauled **The Warrens** (Piscarilius sewers)
    - Also applied an improved shoreline water depth with smaller channels
  - Added **shorelines**
  - Improved **terrain** complexity
- Restored **Avan** (family crest)
- **Improved ExtendedNPCs**:
  - Improved detection for static (non-moving) NPCs. Over 2000 new NPCs that should not move have been made static.
  - Removed implings and random events from Varlamore
  - Removed Chaos elemental
- **Christmas**: Added the snowmakers to the particle toggle
### Fixed
- Drastically increased the client's performance at the **Soul Altar**
- **Improved MapFilter**: Fix an issue with the Underground Pass
- Upgraded the crush animations for the **Zamorakian Hasta**
- The **Orb of Occulus** freecam now compensates fog
- Fix possible transmogrify crash

## [539] 2024-12-18
### Fixed
- Moved **Castle Seal I** secret due to npc tick skip

## [538] 2024-12-18
### Changed
- Made the following changes related to **Christmas**
  - Last year's Christmas has made a return:
    - It has started to snow! (Toggle using `::xmas particles`)
    - Areas have been decorated
    - Your footprints now show in the snow
  - Changed area music in the affected areas to be more Christmassy (Toggle using `::xmas music`)
  - Added secrets (Toggle using `::xmas all` which also disables particles and music, or `::xmas npc`)
  - Remastered the **Black santa hat** and **Inverted santa hat**
  - Fixed an issue with the underworld in the Falador-Taverley lake (fixes hdos/issues#2283)
  - Fixed some untextured terrain by the Falador entrance
- The **shielded max hit**, that was recently remastered, now uses the HD font
### Fixed
- Fixed an issue that caused the **Dragon pickaxe** animation to downgrade
- Moved the fire where **Gilbert** is standing to be behind him, as he was changed to be stationary
- **Hans**' Santa hat has slipped off, and the imps have graciously ordered a return
- Fixed a bug where **Entity Hider** would sometimes hide ExtendedNPCs
- Fixed a bug where **ContextCursor** would not display the default interaction cursor when dealing with NPCs
- ~30 additional locked (LMS) items have been restored (fixes hdos/issues#2279)
### Removed
- Removed **Thanksgiving**
### Audio
- Added Christmas music
- Remastered the following tracks (thanks to 'rsorchestra'):
  - Watch Your Step (HD Remix)

## [537] 2024-12-10
### Changed
- Improved the South-West corner of the GE
- #### **HD Pack 26**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack targets **Christmas 2024** and introduces the following changes:
  - Upgraded all Christmas Quest NPCs and equipped them with Santa Hats
  - Textured all Objects
  - Fixed conflicting issues with Falador
  - Added a login Widget for the Queen of Snow
  - Fully remastered the following items:
    - Four variants of the Present box hat
    - Dog disguise
    - Festive scarf
    - Dog boots
  - Note: *HDOS snow has also been enabled, but Christmas props, particles, music and secrets are scheduled for next week*
### Fixed
- Regressed HD bosses back where the OS Santa hat is not compatible

## [536] 2024-12-6
### Changed
- #### **HD Pack 25**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This pack will add a smaller portion of remasters to **Leagues V**
  - Textured over a hundred models used in **NPCs**, **Objects** and **Projectiles**
  - Fully remastered the **Leagues Tutor**
  - Attempted to fix some animation label behaviour on the **bosses**
    - _If they are still frozen, please use_ `::unsafe`
  - Updated the **Lumbridge** scenery
  - Remastered **Items** partially:
    - Gloves of the Damned
    - Drygore Blowpipe
### Fixed
- Fixed a **plugin** bug where old behaviour of the Ground Items plugin clashed with the Menu Entry Swapper plugin (fixes hdos/issues#1455)
  - The '**take**' option is no longer always hidden, and only hides when:
    - **Ground items**: `Hide` is enabled on a specific item AND the option `Deprioritize hidden items` has been enabled
    - **Menu Entry Swapper**: `Enable Shift-click "Walk here" ground items` has been enabled
    - *This behaviour is also compatible in combination with the NPC and Object menu swaps*

## [535] 2024-12-4
### Fixed
- Fix remaining **MapFilter** issue in the Underground Pass
- Updated map data for Aldarin and Harmony
- Updated game cache with todays update

## [534] 2024-12-2
### Changed
- #### **HD Pack 24**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the **Uzer Oasis**
  - Remastered the **Oasis water** itself
  - Heavily improved **terrain**
    - Added many new blending underlays to improve the overall transition into the oasis
    - Restored house flooring
    - Heights are now much smoother
  - Textured all **objects**
    - Restored mining rocks, rockslides and red carpets
  - Added the Desert Sanctuary **atmosphere** (Al Kharid-esque)
    - Added lights
  - Remastered **NPCs**
    - Textured **Morgiana** and added chat-head
    - Upgraded **Ali, the Tea Seller** fully
    - Remastered **Cassim** fully
  - Added **shorelines**
  - Other changes:
    - Upgraded **Fairy Ring** DLQ (North of Nardah) to use the desert sand emitter
    - Remastered 3 barrier (shield) **hitsplat sprites**
    - Remastered all 4 variants of the **Keris Partisan**
    - Remastered the **Masori Assembler** and improved internal properties
### Fixed
- The corrupted **voidwaker** now applies the same idle animation as the regular variant
- Ava now uses hands from revision 592, fixing an old bug that caused her hands to stretch (fixes hdos/issues#2190)
- Made the following changes to the **Underground Pass** (fixes hdos/issues#2269)
  - An issue where certain transforming objects that appear during the **Song of the Elves** quest would not show has been fixed
  - Updated the MapFilter to be more accurate
  - Rebuilt the maps to fix the unicorn regressions
  - Fixed an issue causing the agility fail shortcut to null (fixed type miss-map and object placement error)

## [533] 2024-11-27
### Added
- Add Thanksgiving secret
### Changed
- Adjusted the **Amulet of rancour** models to fit closer to the player models
### Fixed
- Update game cache and fix item regressions

## [532] 2024-11-20
### Changed
- Updated **proto** to 227
- The following changes have been made to the **Tumeken's Shadow**
  - The staff is now capable of performing HD movement
    - Note: *The attack animation has been preserved*
  - You no longer slide whilst performing interleaved animations (eating, crushing etc.)
  - Some of the translations on the relevant models have been adjusted to occomodate the new animations
  - Remastered the remaining casting graphic and added bloom
  - Defend now uses the Staff of Light's defence animation
  - Fixed some jittery translations with the staff whilst performing the attack animation
- Updated some **plugin stats**
### Fixed
- Updated map compatibility for **Falador**

## [531] 2024-11-13
### Changed
- Update lobby widgets
### Fixed
- Updated game cache and fixed item regressions
- Fixed an issue that prevented the **white apron** in Gerrant's Fishy Business from being rendered correctly
- Fixed game crash
### Removed
- Removed Halloween

## [530] 2024-11-6
### Fixed
- Updated game cache and mappings for the **Curse of Arrav** update
  - Fix chat-heads
  - Restore Mahjarrat regressions
  - Restore some new Armoured Zombies
  - Restore new Weaponsmaster
  - Restore new Jonny the Beard
  - Update lobby widget
- Temporarily disabled the **Attack Styles** plugin
  - Update componentIDs and fixed weapon styles

## [529] 2024-10-24
### Fixed
- The ToA bank camel no longer squishes the bank tags height
- Implement new **Tormented Demon** visuals

## [528] 2024-10-23
### Changed
- Updated some cache related to this weeks game update
  - Remastered the new inactive version of the **Arclight**
  - Restored vanity **Torva**
  - Remastered the new **Tormented Demon** graphic
  - Update game cache

## [527] 2024-10-20
### Fixed
- Fixed a bug affecting projectiles, spot graphics and object animations
  - Fixed projectiles not coming from the right spot at the **The Hueycoatl**
- Fixed **potion storage**

## [526] 2024-10-18
### Changed
- #### **HD Pack 23**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack addition targets Oldschool's **Halloween 2024** event
  - Textured 347 **objects**
  - Remastered **NPCs**
    - Fully remastered **Marika** and **Ghost cat**
  - Textured pumpkin **graphic**
  - Remastered 140 **items**
    - Improved model compatability for **scarecrow shirt** and **pumpkin helms**
    - Fully remastered last year's **spider hats** and **cobweb cape**
    - Added **chat-heads** for spider hats and pumpkin helms
    - Fixed incorrect labels causing animation glitching on the evil variant of the pumpkin helm
  - Fixed Z-Fighting on meshes where possible
  - Fixed some shadows
- Updated Lobby widget
- Remastered projectiles for **The Hueycoatl**
  - Improved animation of the blue variant
### Fixed
- Update skeleton compatability for all cats (fixes regressions)
- Improved the cold hitsplat for others, as it was darker than intended
- Fixed projectiles not appearing at the **The Hueycoatl**

## [525] 2024-10-16
### Changed
- Updated proto to 226
- Remastered the new cold hitsplat for others
### Fixed
- Mastering Mixology animations will now cancel eachother out 
- Update mesh of Aga Lever

## [524] 2024-10-14
### Fixed
- Certain object animations no longer stop animating if their transform tells it to (fixes raids)

## [523] 2024-10-14
### Fixed
- Fixed a bug where transforming objects would not animate under special conditions (fixes **Mastering Mixology**) (fixes hdos/issues#2252)

## [522] 2024-10-14
### Fixed
- Fix known crashes

## [521] 2024-10-14
### Fixed
- Fix object flickering

## [520] 2024-10-14
### Changed
- The **Halloween** login halloween music and atmosphere now depend on your in-game settings
- Remastered the **Zombie Axe**
### Fixed
- Update ComponentIDs (fixes bank ui)
- Fixed **Varrock** museum chunks for new quest
- **Potion storage** now aligns with the bank tags
- Fixed **Varlamore dungeon**/login background regression

## [519] 2024-10-9
### Fixed
- Fixed displaying null in the chat
- Fixed buggy command arg

## [518] 2024-10-9
### Added
- Added the ability for sounds to be upgraded to music samples (vorbis)
  - AnnoyanceMute can also mute these sounds
### Changed
#### Halloween
- **Halloween** has returned!
  - What's changed this year?
    - The music and atmosphere can now be **toggled** more reliably (at the Grand Exchange)
    - Discontinued last year's quest
    - Added some easter eggs
    - Applied Halloween Atmosphere to all login backgrounds
  - Several NPCs have magically transformed into spooky variants
  - Music changes
    - Brought back several music tracks
  - Pyromancer clothing has turned into Witch and Warlock clothing for females and males respectively
  - Halloween theme is now active in:
    - Lumbridge, Draynor, Port Sarim, Rimmington, Varrock and Edgeville
- Restored the **Teleport Animation** sound
  - Added an additional sound to the **Pendant of Ates**
  - The Pendant of Ates sound effects now use area sound effect
- Upgraded the **lvl-up fireworks graphic** and upgraded the sound with a high quality music sample
- Restored a version **Caleb**
- Upgraded chat-heads for **Clanila** and **Alaxxi**
- Deprioritized the Varlamore login backgrounds; it is now random once again
- A new blue dragon has had one of its models restored with todays game update
- Remastered the new **Wintertodt* reward cart
- Remastered the new Wintertodt **cold hitsplat**
- Updated login widgets
### Fixed
- Fixed a bug where the 'All Settings' window would go haywire when toggling plugins while it is opened
- A variant of the **Varlamore Graceful hood** now uses the remastered asset
- The item charges plugin now always updates the **fish barrel** correctly
- Fixed a bug with a change for the Pendant of Ates where **teleport-out animations** for other items would no longer play
- The **Summoning plugin** no longer attempts to override HD npcs
- **AnnoyanceMute**: Lower level magic Thralls and the Inferno Adze can now be muted
- Updated map compatibility for **Hosidius**, **Wintertodt** and **Aldarin**
- Fixed map regressions for **Warrior's Guild**
- Updated mesh labels for the grape storage on **Aldarin**
- Updated game cache

## [517] 2024-10-4
### Changed
- The Hueycoatl inside the cave entrances are now remastered
- Textured **Rune scimitar** ornament kits
### Fixed
- The Hueycoatl fixes:
  - Increased skeletal bone threshold (fixes T-Posing)
  - Fixed an uncaught exception
  - **Note:** *Projectiles are still invisible*
- Skyboxes no longer reset when exiting a cave (fixes hdos/issues#2244)
- Fixed Loot Tracker **plugin** not detecting the Lunar Chest correctly
- Fixed known client crashes
- Fixed a collision indicator in the **Taverley dungeon** upstairs area (fixes hdos/issues#2241)


## [516] 2024-10-2
### Added
- Added the 'override cave fog' option for the **Skybox plugin** (fixes hdos/issus#2233)
  - **False (default)**: The atmosphere will change without altering the fog or skybox itself when in an area with the cave fog present
  - **True (old behaviour)**: The plugin will always override the fog and skybox
### Changed
- Improved **Immersive Toggles**:
  - Implemented multi-state toggles
  - Added the **Zaryte crossbow** toggles to Al Kharid
  - Tweaked the colors on the Zaryte crossbow
  - Upgraded the Zaryte crossbow animation
- **HD Pack 22.1.5**
  - Added some **new assets** for Varlamore North and Aldarin
  - Updated **game maps** for the latest game update
  - Updated the **Aldarin house portal** dimensions to fit the new size
  - Upgraded the **Children** on Aldarin
### Fixed
- Update game cache


## [515] 2024-9-30
### Changed
- **Plugins** fixes & updates:
  - Added **death indicator** plugin
  - **Loot tracking** supports Arraxor, Colosseum reward, Lunar chest and Hunters' loot sack
  - **Clue scroll plugin** has been updated
  - **Time tracking** supports new varlamore patches
- #### **HD Pack 22.1**

  This HD Pack addition targets the **Varlamore Ice Dungeon** (Ruins of Tapoyauik) and serves to refine and add additional assets to this area
  - The following changes are introduced:
    - Fully remastered the maps
      - **Void** (black darkness) has been improved
      - Added **Shorelines**
      - Fixed **MapFilter**
      - Added and improved **lighting**
      - Added complex stalagmite objects
      - Improved the atmos
    - Added new **Runite Rocks**
    - Remastered all boss and monster **GFX**s
    - Remastered many more **objects** and wallkits
    - Some shadows will no longer render when they shouldn't
    - Remastered items:
      - Sulphur blades
      - Glacial temotli
      - Moon key, loop half moon key, tooth half moon key
      - Frozen tear
    - Fully remastered NPCs:
      - Chilled Cave Crawler
      - Chilled Jelly
    - Remastered varlamore map **widgets**
    - Remastered **boss health bars**
    - Remastered the **Pendant of ates** teleport
    - Improved **context cursors**
    - Added **ExtendedNPCs**
    - Added an additional login track
    - Added an **immersive toggle** to the dungeon
- Texture 339 (rare elemental magic) now uses bloom
- The Duke vents show a shadow once again
### Fixed
- NPC chatter no longer contains a pipe symbol (fixes hdos/issues#2161)


## [514] 2024-9-28
### Changed
- #### **HD Pack 22**

  This HD Pack targets **Varlamore: Part 2**, and will also be the second batch of this region.
  The following changes are introduced to **Northern Varlamore** and **Aldarin**
  - Remastered **Aldarin** fully
    - Added **ExtendedNPCs**
    - Remastered all objects
  - Textured 420 **NPCs**
    - Restored misc NPCs
  - Added **immersive atmospheres**
    - Also added **snow particles**
    - Added additional **pointlights**
  - Blended terrain **heights** on many mountains
    - Remastered the **Ice Dungeon**
  - Drastically improved **new terrain** complexity
    - Also fixed **miss-mapped** terrain
  - Fixed some **Z-Fighting**
  - Added **shorelines** and underworld objects
  - Upgraded several **objects**, such as fires and braziers
    - Restored backported objects
    - Added **waterfalls**
  - Improved **MapFilter**
  - Remastered the **Dire Wolf Den**
  - Remastered several **items**:
    - Alchemist outfit (full)
    - Hueycoatl hide (partial)
    - Dragon hunter wand (full)
    - Graceful (full)
    - Tome of earth (full)
    - Pendant of ates (full)
  - Added new **login background** (Aldarin)
  - Improved several aspects of internal tooling, allowing for a more efficient and refined overhaul
    - Slightly improved texture projection accuracy
    - Improved shoreline accuracy
    - Improved height blending accuracy
    - Added more terrain sub-biomes
    - Improved asset conversion
  - *Due to the size of the update, not everything may be included in this pack, including some Hueycoatl assets*
- Upgraded **login widgets**
- Improved **context cursors**
- Updated **plugins**: Hiscores, World map, Loot tracker, Boss timer, Fairy ring, Item stats, Slayer plugins to support most of Varlamore part 2
### Fixed
- Update **ComponentIDs**
- Re-update game cache and mappings
- Fixed skeletal model artifacting on merge with static mesh
- Fixed an issue with a Varlamore banker's chat-head
### Audio (thanks to `rsorchestra`)
- Updated **music** to use HD instruments, up to date with Varlamore Part 2 update
- Added a new staccato rock guitar power chord on Bank 5, Preset 28.

## [513] 2024-9-25
### Fixed
- Fix bank related components
- Fix getdown arg for Java 17

## [512] 2024-9-25
### Changed
- Restored the Invandis Flail 'bloom' animation
- Added global Varlamore object upgrades from previous remasters
### Fixed
- Attempt recovery on hard client crash at the Hueycoatl
- Update game cache (fix chat-heads)

## [511] 2024-9-21
### Added
- Added an easter egg...
### Changed
- **ExtendedNPCs**: Added Tormented Demons, improved static npcs
- **MapFilter**: Added Catacombs of Kourend and the Warrens
- Made some improvements to **Duke Succulus**
  - Added ExtendedNPC
  - Brightened the atmosphere
  - Added additional lighting
- Remastered the special attack graphics of the **Emberlight**, **Purging Staff** and **Scorching Bow**
  - Fixed additional animation and alpha rendering issues with the Emberlight special
- Upgraded current welcome screen icons
- Remastered **black wizard robes** (t) and (g) (fixes hdos/issues#2219)
### Fixed
- Re-enabled the **Experience Lamp Interface** restore
- Fixed a bug where **fog** would be too dense on the live client
- Updated **Weiss** maps (fixes hdos/issues#2231)
- Improved performance during the **Phantom Muspah** encounter
- The darkness during the **Phantom Muspah** encounter can no longer stick when dying during the phase and subsequently entering another dynamic region like the POH
- Fixed stitching of the **Chaotic Longsword** with the Fire cape
- The **Chaotic Rapier**'s textures have been improved slightly
- Overlay highlights now render when character shadows are disabled and client is not focus (fixes hdos/issues#2234)
- The Lilypad in the quest "The Ribbiting Tale of a Lily Pad Labour Dispute" now shows correctly (fixes hdos/issues#2212)
- Fixed some collision flags in the Observatory (fixes hdos/issues#2196)
- Fixed a problem with **Hazelmere** not adhering to his upgraded animations when in a transformable state
- Fixed several human animation miss-maps that caused **Sir Prysin** to misbehave (fixes hdos/issues#2182)
- Fixed a null-type preventing a trapdoor in the **H.A.M. area** from showing in a certain quest stage (fixes hdos/issues#2184)

## [510] 2024-9-15
### Fixed
- Fix login background sometimes displaying the wrong map

## [509] 2024-9-15
### Added
- Implemented **Multi-Graphic** (engine)
  - Fixes NPCs using multiple graphics at once without it being tile-bound, such as Manticores and Tormented Demons.
  - Players are now also able to render multiple graphics
### Changed
- Improved **Particles** (engine)
  - Rewrote how particles are recycled, which may fix lingering systems
    - *While the full extent of its impact is uncertain, this change is expected to improve the overall behavior of particles.*
  - Emitters are no longer abruptly killed when a projectile is cleared. Particles now fade out before removing the projectile
    - *This should now mimic the behaviour of still graphics. Phantom Muspah's ranged attack trail and the projectiles within the Inferno should now look much better when they impact the player.*
- The **Keris partisan** will now use the upgraded spear crushing animation
- Restored the **Cat- and Wolf masks**
- #### **HD Pack 21**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Packs targets the Desert Treasure 2 region: **The Scar** and introduces the following changes:
  - Remastered **The Leviathan** boss and all other related **NPCs**
    - Remastered all loot
  - Created new fleshy **terrain** and **water** variants
    - Added shorelines and real water depth
    - Added new underworld objects
  - Added immersive **atmosphere and lighting**, and improved the brightness for the boss-fight and essence mine areas
  - Textured all **objects** and **graphical effects**
    - Added new particle effect
  - Remastered the **Ring of Shadows** teleports animations
  - Fixed misc Z-Fighting and displaced objects
  - Other:
    - Remastered the **Ferocious Gloves** and improved the mesh compatibility (fixes hdos/issues#2197)
    - Remastered the **Cape of Skulls** and fixed the clipping (fixes hdos/issues#436)
    - Redone **Ancestral robes**
      - Full remastered chat-heads, improved textures, improves mesh compatibility
      - The previously untouched **Twisted Ancestral** has received the same treatment
    - Remastered the **Tormented Bracelet** and added faint glow
### Fixed
- Certain dragons have been whitelisted for **ExtendedNPCs** and now render once again (fixes hdos/issues#2228)
- Updated Varrock and Lumbridge maps (fixes issues related to the border of the two areas and roofs)
- Updated Rogues Den maps (fixes regressions)
- Fixed a vanilla bug where the priority of The Leviathan's attack animation could not cancel an already playing animation on higher attack speeds
- Fixed the inventory sprite of the **Frozen key** (fixes hdos/issues#2230)

## [508] 2024-9-11
### Changed
- Update protocol to 225
- Changed back the run movement of the **Scythe of Vitur**
  - *Oldschool recently changed its run animation to that of the Dharok's Greataxe, however, this change also had impact on our HD mappings. As such, this change has been reverted.*
- A particular type of flame no longer glitches out
- Anima textures now bloom when used
### Fixed
- Updated **Port Sarim** maps (fixes hdos/issues#2223)
- Some pillars in the **Hallowed Sepulchre** now glow (missing asset)

## [507] 2024-9-5
### Added
- New **immersive toggles** have been improved, and new ones have been added for the **Osmumten's Fang**, **Elder Maul** and **Ghrazi Rapier** to bring back some of the old **chaotic weaponry**! (fixes hdos/issues#1745 hdos/issues#1084)
  - Also fixed a bug with the **TokHaar-Kal** toggle where the state could get stuck
  - Item toggles no longer incur a loading screen
  - Fixed unknown material effect on backported meshes (allows compatibility leprechauns, chaotic longsword etc.)
  - Added up-to-date locations of immersive toggles to `All Settings > Gameplay > HDOS`
### Changed
- Updated the new **Osmumten's Fang** models
  - The ornamented version is now also textured
- Further improved **Araxyte** caves:
  - Remastered the secret area
  - The sky will no longer turn green in proximity of the Ectofungus
  - Improved MapFilter
  - Remastered some missing assets such as the Acid pools and magic projectiles
- Update several **plugins** to be compatible with Araxxor
- Remastered all genders and off-hand versions of the **Bruma Torch**
- Added a smoother transitioning atmosphere effect to **Al-Kharid gate**, and the area **west of Al-Kharid bank** no longer uses the swamp atmosphere
  - Slightly increased the terrain complexity in the area west of Al-Kharid bank
    - Also fixed some TileFlags in this area that caused the player character to teleport around
- Heavily improved collision clarity by the **Catacombs of Kourend's Fire giants** (fixes hdos/issues#2202)
  - Added crystals with new lights, improved brightness and excluded height blending
- Restored **Drezel**
### Fixed
- Further updated map compatibility for **Ardougne**, fixing missing walls north of the market, and updated the Flying Horse Inn once again
- Updated map compatibility for **Lumbridge**, fixing broken roofs (fixes hdos/issues#2222)
- Updated map compatibility for **Yanille**
- Updated map compatibility for **Varrock** (fixes hdos/issues#2180)
- The **sidebar** no longer forces itself to re-open upon re-initialization (world hopping etc.) when it was set to hide whilst a plugin tab was still selected in the background (fixes hdos/issues#1753)
- Fixed several object miss-maps
- Fixed one of the corners on Oziach's house in **Edgeville** (fixes hdos/issues#2151)

## [506] 2024-8-28
### Changed
- #### **HD Pack 20**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Packs targets **Araxxor** and introduces the following changes:
  - Remastered the **Araxyte caves** and its entrance area
    - Added an immersive **atmosphere**, added **lights**, diversified and improved complexity on **terrain**, improved **heights**
    - Remastered all **Objects**
      - Also remastered the POH Araxxor display
    - Added new web and acid terrain
  - Remastered all **Araxytes**
    - *Replaced **Weave** with a temporary upgrade*
  - Remastered all **items**
    - Fully remastered **Aranea boots**
    - Remastered the **Amulet of rancour** and improved model compatibility
    - Remastered the **Noxious halberd**
    - Remastered all related **ground items** (boss and create drops)
    - *With the exception of the **slayer helm**, where only textures and improved offsets were applied*
  - Remastered new **Graphical effects** used during the boss fight
    - Added a new particle effect
  - Added **ExtendedNPCs**
  - Added Login backgrounds, which are also favored upon login for this update
  - Fixed Z-Fighting on objects and the teleport scroll
### Fixed
- **MapFilter** has been fixed for the Araxyte caves
- Updated Map compatibility for the Ectofungus
- Updated cache


## [505] 2024-8-26
### Changed
- #### **HD Pack 19**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Packs primary focus is on the **Hallowed Sepulchre** (thanks to `J3` for texturing most assets)
  - Remastered the **Hallowed Sepulchre**
    - Remastered the lobby as well as each of the 5 levels of the minigame
    - Improved visibility of some of the obstacles that were harder to differentiate in HD by giving the gfx bloom
    - Textured 287 environment objects
    - Remastered **all NPCs** featured in the minigame
      - The Mysterious Stranger has been remastered for all 7 encounters in a variety of quests and circumstances
    - Remastered the following items; **Hallowed Focus**, **Hallowed Hammer**, **Hallowed Symbol**
    - Gave texture to both variants of the **Squirrel pet**
    - Fixed Z fighting on various objects within the sepulchre
  - Other changes:
    - Remastered all **deadman items**
      - Added a new **Korasi GFX** for the kitted variant
      - All weapons have been upgraded
      - Remastered the new imbued god cape kits and fixed their Z-Fighting (fixes hdos/issues#2205)
    - Fully remastered the **Serpentine Helm** (thanks to `@Woahscam` for providing the base mesh)
      - Both the **Magma and Tanzanite** use this same base mesh for the time being, to fix several rigging issues
    - Remastered the **Achievement diary cape** under the hood and fixed its regressions
      - The emote is now also textured
    - Remastered the **Thieving bag**
    - Remastered the **Trailblazer reloaded teleport GFX**
- Upgraded the Slayer helmet widget on the welcome screen
### Fixed
- Update the **Soulreaper axe** animations once again, fixing latest game update regressions
- **Mac** has had his pirate outfit taken away
- Updated map compatibility
  - The **Falador Park pond** has been fixed (fixes hdos/issues#2217)
    - *Note that the collision is broken server side. The water is supposed to be where it is*
  - Some fencing around the mote of **Falador Castle** has been corrected
  - Fixed **Ardougne West Pub**
    - Note that the collision is also broken here*
  - Fixed houses around **Ardougne Market** partly
    - *Oldschool introduced a lot of changes, causing large chunks of houses to break. Due to the complexity of this area, it will take time for us to update it fully*
- The **Hiscores plugin** now shows the Colosseum icon correctly
- The statue in front of the **Heroes Guild** has reappeared
- Corrected some inventory sprites for the imbued god max capes when broken
  - Some variants of the fire- and infernal max cape sprites have also been corrected

## [504] 2024-8-14
### Fixed
- Temporarily disabled the xp lamp interface restore that caused a client crash with the latest game update
- Fix known crashes

## [503] 2024-8-14
### Added
- Added plugins
  - Hunter Rumours
  - Max Hit calculator
  - Visual Metronome (font types not functional yet)
  - Friends and Clan Viewer
  - Current World overlay
### Changed
- Update protocol to 224
- Updated plugins
  - Chat command plugin: added up to date aliases such as aldt2, hunter rumour, lunar chest etc
  - Quest helper plugin: removed Kourend Favour favours requirement and quest
- Remastered **Ba-Ba's rolling boulders**, and drastically improved the contrast of the broken variant, which should eliminate the need for any plugin to mark it
- Restored several **robe bottoms** from Rometti's store (fixes hdos/issues#2160)
- The While Guthix sleeps backgrounds on the login screen are no longer prioritized and the music has turned back to normal
  - *The new login background is not fully removed, and can still randomly play*
### Fixed
- The special attack 'disrupt' performed with Korasi's sword (Voidwaker) has been improved
  - Bloom has been addded
  - Alpha rendering has been fixed
### Removed
- **Achievement diary cape** and **Serpentine helmet** have been temporarily reverted

## [502] 2024-8-7
### Changed
- Restored the **Morrigan's thrown graphics**
### Fixed
- The **Voidwaker** special attack once again uses the Korasi's
- The new **Osmumten's Fang** special attack graphic no longer renders the user invisible, and now glows
- Updated map compatibility for **Player-owned houses**
- Update game cache

## [501] 2024-8-1
### Removed
- The **balance elemental** restore has been temporarily removed
### Changed
- Remastered the **burn** sprite (used by the Burning claws, Scorching bow and Eclipse atlatl)
- Restored other versions of the **Black mask (i)**, **Dragon defender** (l) and (t), **Crystal shield** and **Crystal Bow**
### Fixed
- **While Guthix Sleeps** statues are now restored outside of Falador (fixes hdos/issues#2179)
- **Captain Kalt**'s idle has been temporarily changed to a working idle (fixes hdos/issues#2178)
- Updated NPC and Item data for plugins
- Fixed an issue with the **fairy ring** exiting animation affecting yourself instead of others
- An issue with the skeletal compatibility of magical explosion **graphics** has been fixed (fixes hdos/issues#2141)
  - *Some may be downgraded*
- Updated map compatibility for **Wintertodt** and several While Guthix Sleeps maps
- Update game cache (fixes chat-heads)

## [500] 2024-7-24
### Changed
- #### **HD Pack 18**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Packs primary focus is on the **Smoke Devil Dungeon**
  - Remastered the Smoke Devil slayer dungeon
    - Remastered the entrance of the dungeon
    - Drastically improved the terrain depth of the dungeon
    - Remastered the lava, and now uses real HD objects rather than the oldschool overlay
    - Added more complex terrain generation with modern pathing and floor decorations
    - Remastered the Nuclear variants of the Smoke Devils
    - Restored Smoke Devils
  - Remastered Breive
  - Added immersive toggle: The Arcane Stream Necklace can now be picked up at the skeleton by the entrance of the dungeon
  - Misc improvements:
    - The mesh compatibility and textures of **Jusiciar armour** have been heavily improved
    - The **Twisted Bow** now uses improved bloom
    - Remastered the Leagues Dwarf Multicannon
    - The smoke ejected by the devils no longer makes the entity beneath it appear invisible
- Some improvements have been made to the **While Guthix Sleeps** quest (fixes hdos/issues#2175)
  - Remastered **Duradel's Notes**
  - Login backgrounds will now randomise after playing the While Guthix Sleeps tracks
  - Some drawers in Falador castle have been upgraded
  - Statues in Falador park have been restored
  - Terrain around Ardougne is upgraded once again
  - Restored the quest completion icon
  - Fixed 'hide roofs' on all the Ancient Guthixian Temple maps (fixes hdos/issues#2173)
    - Also removed some random rocks floating in the void that OS unintentionally placed there
  - Fixed some broken cave walls in the Stone of Jas caves that broke after Jagex had changed the blocked map squares (fixes hdos/issues#2176)
  - Restored barricades in the Black Knight Catacombs (OS used unused/untextured HD assets) (fixes hdos/issues#2177)
  - Added global upgrades for several WGS areas, allowing objects to be upgraded if ever used in another area
  - A rock (with rope attached) in the Tears of Guthix caves have been upgraded
  - The balance elemental has been changed so it allows for recols like oldschool (experimental, we cannot easily verify this change)
  - Some vine flowers have been restored
  - Remastered some of the new Movario spells
### Fixed
- Some unintended changes to the **Phantom Muspah** floor decorations have been reverted
- Entering a **Fairy Ring** now forces the player to exit after a certain amount of time, if the server does not tell it to
  - Fixes players getting stuck in the Evil Chicken Lair where the HD animation policy relied on the server
- Abyssal Demons in the **Catacombs of Kourend** no longer float in the vicinity of an upper area
- ExtendedNPCs: Sitting ogres no longer walk around
- Update game cache and fix known regressions


## [499] 2024-7-17
### Changed
- The following improvements have been made to the **While Guthix Sleeps** quest:
  - Updated map compatibility for: Falador, Taverley, Feldip Hills, Warrior's Guild, Fight Arena, Khazard Battlefield
  - Restored Movario's Hideout, Black Knight's underground & King's ransom Prison, Black Knight Catacombs, Lucien's Camp, Wilderness Chaos Altar, Ancient Guthix Temple (Tormented Demons), Remastered Stone of Jas cutscene (with V)
    - Remastered new maps
  - Restored over 160 NPC's!
    - **Mahjarrat** are now fully restored
    - The **Balance Elemental** has been restored
    - **Tormented Demons** have been restored
    - Some **dragonkin** (excluding DS2) have been restored
    - New NPCs have been remastered, such as Lucien's dark form and several Elite Black Knights
    - Some NPCs have received fixes, such as one of the Tormented Demons missing bloom textures, which was most likely unintended
  - Remastered the **Dark Squall outfit**
  - Restored **Elite Black Armour**
  - Restored dozens of **GFXs** including Lucien's magic, Puffs of smoke, Eletrocution and more
  - Remastered the following items: Emberlight, Purging staff, Burning Claws, Scorching bow, Imbued heart, Smouldering heart, Smouldering pile of flesh, Venom gland, Smouldering gland, Tormented synapse
    - The **Purging staff** no longer interferes with other items, and its idle has been improved
    - The **Scorching bow** now uses a bowstring
    - Reworked the **Emberlight**
    - The new weapons now use billboards to glow
  - Remastered misc: Herb Patch (Falador Castle), Guthixian Brazier, Druid Assassin's dagger
  - Restored numerous animations
  - Improved MapFilter
  - Restored several interfaces
  - Certain NPC modifications sent by the server are now capable of being upgraded
  - Remastered several **Health Bars**
  - Updated the login screen theme for this update
- Restored **Frog Prin(ce)(ss)**
- Fishing overlays now only show on real NPCs (not extended ones)
- Changed some behaviour how the client handles server sent animations for objects
  - *We're not sure what implications this has, so please report any glitching objects if present*
### Fixed
- Children are restored once again
- Some **magic explosion GFXs** no longer become stiff on impact
- Fixed camera yaw and pitch interpolation in cutscenes
  - Disabled Lucien's cutscene temporarily, as the engine currently does not display the camera correctly
- **NPC overhead icons** should now appear correctly
  - **Tormented Demons** are affected by this fix
- The height on **NPC overhead icons** has been fixed
  - Both **Tormented Demons** and **Demonic Gorillas** are affected by this fix
- Bandaid multi-graphic (similar to the manticore) for **Tormented Demons**
- The dragon defender (l) is now restored (fixes hdos/issues#2168)
- Updated the latest NPC mapping changes (fixes hdos/issues#2164)
- Fix known crashes
- Update game cache
### Audio (thanks to `rsorchestra`)
- Updated music and jingles to HD Sounds, up-to-date with the While Guthix Sleeps update.


## [498] 2024-7-10
### Added
- Experimental `::osmode` command to toggle HD items
### Changed
- Restored the **King Black Dragon lair** (and the Nightmare Zone arena)
- Remastered the Yanille **Nightmare Zone** terrain
  - Remastered **Dominic Onion** (fixes hdos/issues#1792)
- ~~Restored the current **Mahjarrat**~~ (fixes hdos/issues#1762) (downgrade on release of WGS)
- Added **Run Energy** plugin
- Updated **Opponent Info** plugin
### Fixed
- Fixed latest game crash
- An issue with the Yanille wall has been fixed (fixes hdos/issues#2078)
- Imps are restored once again
- Fixed missing **Razor Kebbit Tracking** plugin ui
- Items and NPCs have been updated for plugins

## [497] 2024-7-4
### Changed
- Restored the **Asgarnian Ice Dungeon**
  - Remastered the upstairs skeletal wyvern area
  - Remastered the icy 'steps'
  - Remastered **Pieve** and **Steve** (this area only)
  - Restored a newer version of **Hobgoblins**, their animations, models and sounds, (all over Gielinor)
  - Improved a small issue that would surface itself on specular retextures of the cave wall gradients (the edges fade into the darkness). This should be fixed for all caves using those assets.
  - Added Steve to ExtendedNPCs
  - Added Login Background
- Updated some plugins: 
  - Boss Timer
  - Hiscore
  - World Map
  - Time Tracking (Varlamore farming patches)
- **Tile Indicators** now takes priority over **Ground Markers** (fixes hdos/issues#2132)
- **Amulet of Blood Fury** has been remastered, and no longer causes clipping (fixes hdos/issues#2147)
- Remastered the **Occult necklace**
- Restored the **Genie** and the **Pillory guard** random event NPCs (fixes hdos/issues#2061 fixes hdos/issues#2100)
  - A version of the **Mysterious old man** has also been restored
- Remastered the **Bonecrusher necklace**
- Remastered several versions of **Brundt the Chieftain** and restored **Agnar** (fixes hdos/issues#887)
- Remastered the **Flax Keeper** (fixes hdos/issues#1667)
### Fixed
- Improved **ToA's Warden** clickboxes (fixes hdos/issues#1868)
- Animations for the following slayer monsters have been corrected (fixes hdos/issues#2144):
  - Cave Crawlers
  - Rock Slugs
  - Cockatrices
  - Basilisks
  - Turoths
  - Bloodvelds
- The skeleton for **fishing spots** is once again compatible, upgrading a bunch of them providing they are restored (Varlamore Fishing Spots are now upgraded)
- Fishing spots no longer cast a shadow if they are restored, mimicking behaviour of higher revisions (fixes hdos/issues#2125 hdos/issues#1347)
- Fixed the minimap behaviour by the Varlamore Port (fixes hdos/issues#2138)
  - *Oldschools port unfortunately does not provide minimap icons, unlike areas like Port Sarim, so they rely on terrain instead. Some custom assets had to be created, and some terrain had to be undone in order to fix this*
  - Players can walk around on the boats once again
- Corrected the female **prayer cape** model (fixes hdos/issues#2121)
- Improved the alignment of the **Abyssal dagger**, fixing the stitching between it and the Imbued zamorak cape (fixes hdos/issues#2059)
- Fixed another Ardougne castle door (fixes hdos/issues#2105)
- Remastered the Witchhaven dock chair (fixes hdos/issues#1766)
- Grain sacks in **West Ardougne** now:
  - Transform into their burnt OS variants when into that stage of the quest (fixes hdos/issues#1699)
  - Fixed one of their variants being mapped to the same variant (there is more variation now)
- Restored a variant of **Sir Palomedes** (fixes hdos/issues#1669)
- Hellhounds in the **Catacombs of Kourend** no longer float when approaching the wall

## [496] 2024-6-26
### Added
- Added **Map Filtering** to several dungeons in Varlamore
### Changed
- Update protocol to 223
- The **Moons of Peril** Blood Moon room has had its lighting reduced
- Remastered **Maple Stumps** (fixes hdos/issues#2099)
- Remastered **Forestry** Ritual Circles
- Remastered the charged, uncharged, blood and holy- **Scythes of Vitur**
  - *Their idle animation angle has also been improved* (fixes hdos/issues#2140)
- Restored several NPCs in **Rimmington** (fixes hdos/issues#2117)
### Fixed
- Fixed **Echo Boots**'s item sprite (fixes hdos/issues#2131)
- **Mi-Gor** and **Sorebones** no longer use upgraded chat-head animations as their HD chat-heads are not upgraded (fixes hdos/issues#2084)
- All **crocodiles** in **Pollnivneach** are now restored (fixes hdos/issues#2137)
- Fixed **Pollnivneach river** until its full restore (fixes hdos/issues#2136)
  - The stepping stones have been upgraded and no longer cast a square shadow
- A faulty custom asset has been fixed, causing the wrong wall to be placed in **Varlamore**'s City (fixes hdos/issues#2139)
  - *A cornerpiece has also been added to the Thief hideout, filling the hole that is there in Oldschool*
- Restored a variant of barrows gloves
- Improved the rigging for the **Inquisitor armour set**
- **Air- and Earth Surge** graphics now work correctly (fixes hdos/issues#2141)
- The Crafting shop minimap icon in **Al-Kharid** now appears correctly (fixes hdos/issues#2123)

## [495] 2024-6-23
### Added
- Added custom login backgrounds
  - *This update will prioritize Varlamore background tracks first. This is temporary.*
### Changed
- #### **HD Pack 17**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  The first batch of this HD Pack targets **Varlamore**. Although Cam Torum, Moons of Peril and Ralos' Rise have received an initial graphical improvement, their full glory will be released in Batch 2 due to the size of the pack. All the following changes are introduced:
  - Over 1500 custom assets have been added
  - NPC upgrades:
    - Some NPCs have received full remasters, such as: **Guild Master Kiko**, (varlamore) **Estate Agent**, **Picaria**, **Osaga**, **Gloria** and more!
    - New **particles** have been added for Embertails and Pyrefoxes
    - 16 new HD children have been created
    - 24 new HD bankers have been created
  - Remastered **objects, NPCs, projectiles and items**
    - Upgraded many models to **true HD**
    - Restored numerous objects and NPCs
    - Doubled the amount of assets for various walls to **improve shadows**
    - Various custom assets have been added such as the **modern trees** and **Hunters Guild banners**
      - Some **trees** now also **animate**
    - Remastered the **Atlatl** based on the Sagaie
    - Remastered the **Eclipse Moon Armour** and its graphical special effects
    - Remastered **Dizana's Comp Cape** and added new particles
    - Remastered **Echo Boots**
    - Remastered **Hunters' Sunlight Crossbow**
  - Added immersive atmospheres and added extra lighting objects
  - Overhauled Terrain:
    - Created **new terrain** and improved detail all over the region
    - Improved **ground heights**
    - Fixed buggy paths
    - Added **shorelines** (underwater)
    - **Waterfalls** have been added
    - Added **oases** and water sparkles
    - Initial overhaul of **Ralos' Rise**
      - It has also started snowing lightly on the snowy mountain tops
      - Added jungle and shorelines in adjacent upcoming areas
    - Added new objects to compliment terrain better
  - Dungeon overhauls:
    - Various lighting has been added
    - Detailed decorations have been added or improved
  - Added extended NPCs
    - Added a new system to predict server NPC spawns
  - Misc changes:
    - Jumping over **hunter pitfalls** no longer causes the player to fall into one, youch!
    - **Broken shorelines** on the surface have been fixed
    - The **windmill** has been greased and will no longer cause the animation to jump
    - You can no longer see through the world at **the rift** in the Savannah
    - Upgraded all **mining rocks**
    - Several areas have had their assets altered to fix **Z-Fighting** and other alignment issues
    - **Braziers and torches** have had their flames upgraded to HD
    - Many terrain **miss-maps** have been fixed
    - Added sand particles to the Avium Savannah **Fairy Ring**
    - A **pick-up animation** used to grab logs from the roots in the Hunters Guild Dungeon has been restored
    - Moar **beach sand**!
    - Fixed some of the **animation labels** for mother citizens, and should no longer cause the head to detach from the baby
    - Improved several lackluster **existing HD assets** while still respecting the intended art style
    - Upgraded some bankers in the **clan hall**
    - The modern Herblore **Potion Mixing** animation has been restored
    - Restored the **Fairy Ring** visuals
    - Remastered **Leagues alchemy spells**
    - The **comp cape hood** has been restored
    - Remastered and increased polycount on the **Soulreaper axe**
    - **Alpha rendering** order for dynamic objects was fixed as part of this remaster (translucent objects that animate no longer appear in front of others when they are behind them)
      - *Note that this only applies to dynamic objects*
    - Restored the true **healing hitsplat sprite** (fixes hdos/issues#2124)
    - Remastered **Masori** including the reinforced versions, chat-heads and rigging improvements
- Added hill-adjust to **Hellhounds**, and are now able to stand on slopes more accurately
- Restored some untextured **dragon** parts (NPC, not armour)
- Adjusted the **agile cape** mesh (fixes hdos/issues#1333)
- Adjusted the labels and positioning of the **mythical cape** mesh
  - Improved the textures slightly
- Some children have had their idle animation smoothened
- Restored another variant of the **enchant jewellery** animation
- A warning about **memory allocation** has been clarified in the set-FPS section of the settings
### Fixed
- Added a band-aid fix for the **Manticore** multi-graphic that was not appearing on the NPC
  - *Note that this is a temporary solution until we have a formal engine implementation*
- **Iffie** no longer decides to get out of her chair!

## [494] 2024-6-12
### Fixed
- Fixed the following for **Harmony** island:
  - The farming patch is once again visible
  - Fixed an issue with a rock by the shipwreck
- Fixed client crash on login

## [493] 2024-6-12
### Changed
- Remastered 27 **Crown Shirt** models
  - *The rigging has also been modified. We're experimenting with this change and will be monitoring it in preperation for Varlamore, where more armour may be updated*
- Upgraded the animations for the **Felling axes**
- Upgraded the slashing animation for the **Zombie axe** and **Leaf-bladed battleaxe**
- Restored **Harmony**
- Restored **Dorgesh-Kaan** (market/city)
### Fixed
- Fixed **Kit Breaker** in certain states
- Fixed a coloration error with **Timallus**
- Fixed some **Ungael** underworld
- Improved the offsets and timings of high alchemy without a staff
- Improved the offsets of all **alchemy** graphics
- Low alchemy with a staff now plays the correct animation
- Fix login screen **world list** flag sprites (fixes hdos/issues#295)

## [492] 2024-6-5
### Fixed
- A breach in Thordur's blackhole has been fixed

## [491] 2024-6-5
### Removed
- An NPC has been removed from upgrading as it is no longer present in the cache
### Changed
- Remastered the **Pride event**
  - Various models for **Kit Breaker** have been upgraded
  - All new **capes** have been remastered, and some rigging has been improved
  - **Gilbert** has turned back to HD!
  - Upgraded all **objects**
    - Restored chests
  - **Note:** *The new shirts are not included in this update*
### Fixed
- Upgraded misc movement animations for players (minor, for certain cases)
- Update map compatibility for **Falador**
- Update cache

## [490] 2024-5-29
### Fixed
- Fix link browsing for linux/flatpack users (fixes hdos/issues#1950)
- Fix male heads when wearing hats
- Fix a missmap with the food barrel in Varlamore

## [489] 2024-5-24
### Fixed
- Fix **hairstyles**

## [488] 2024-5-23
### Changed
- Update **protocol** to 222
- The Wilderness North-East of Edgeville has been **restored**
  - Fixes shorelines, jagged heights, improves terrain detail and adds rock upgrades and a brighter atmosphere
- All **Mining Gloves** have been remastered
- The **Salve amulet (i)** has been restored, and the **Salve Amulet (ei)** has been remastered
- **Black masks (i)** have been restored
- **Pirate Jackie the Fruit** has been restored
- **Hairstyles** will now more aggressively make your hair shorter if the server deems it to be so (whilst wearing certain hats)
- Restored the following inventory sprites:
  - Holy elixir, Spectral-, Arcane- and Elysian sigils
### Fixed
- Parts of rivers that were frozen in **Edgeville** and **Falador** have now melted
- Hands not aligning when wearing the **female agile top** has been fixed
- A bug that was present in the original game where the 'bare arms' option on **armguards** would not actually hide them has been fixed
- A bug in the backport system that would prevent certain models from being ported has now been fixed. The following changes have been detected:
  - Spectral-, Arcane- and Elysian spirit shields
  - Queen of Snow (chat-head)
  - Third-age range top
  - A certain bucket of water
  - A certain type of shelves
- Some vents and other invisible NPCs at **Duke Sucellus** as well as some NPCs in the **Colosseum** no longer cast shadows that allowed you to spot them before they appeared

## [487] 2024-5-15
### Fixed
- **Draynor's tunnel shortcut** has been patched up, and the minimap icon has been moved
  - Improved Draynor's terrain heights
- The **Motherlode Mine** plugin now updates correctly
  - Also updated the new sack capacities
- Updated map compatibility for **Taverley** and remastered some of the terrain surrounding the shortcut
  - Also added a missing stone in the water around The Lady of the Lake
- Fixed 3 known crashes
- Update cache with the latest game update

## [486] 2024-5-9
### Added
- Added an option to change the **font size** to the ground items plugin with additional font choices
    <details>
    <summary>Click here for a full list of the added fonts</summary>

  - Regular
  - Bold
  - Small
  - Extra large
  - HD
  - Fancy
  - Max hit
  </details>

### Changed
- The animation for Tridents inside **The Gauntlet** have been upgraded
- Textures of a wood basket in **Hosidius** has been improved
- 229 different textures are now able to benefit from **Texture Upscaling**, and texture quality should be improved all around Gielinor!
- The clue casket chest in **Hosidius** has been upgraded
- Church benches in **Kourend Castle** have been upgraded
- **Dinh's Blazing Bulwark** has been remastered
- The lobby icon for the max cape has been remastered
### Fixed
- **Infernal eel fishing spots** no longer cast a shadow that would cause it to Z-Fight slightly, provided the display setting was enabled
- The following fixes have been made to the **Fishing Trawler** minigame (fixes hdos/issues#2094):
  - Broken parts of the ship now appear properly
  - Remastered broken railings
  - The Enormous Tentacle on the northern side now faces the correct direction
  - Some NPCs no longer appear to hover in the air
- **Moonclan- and progress hats** now have faces
- The **Chambers of Xeric** plugin should now:
  - Refresh on raid reload whilst inside
  - Refresh on party ID change (resetting with multiple parties in the same FC)
- Fixed some minor object layering on the amethyst ores in the **Dwarven Mine**
  - *The shadows have not yet been fixed, presumably an engine limitation*
- Updated map remaster compatibility for the **Motherlode Mine** and added new custom asset
- Updated map remaster compatibility for the **Waterbirth Dungeon** and added new custom asset
- Updated map restore compatibility for the **Taverley Wall** and added new custom asset
- Updated map restore compatibility for **Shilo Village** and remastered new terrain
- Remastered some new shortcut objects used for the **Asgarnia Ice Dungeon**, **Pollnivneach** and **Barrows**
- Updated minor map compatibility for: **Crandor**, **Barrows**, **Waterbirth Island**
- The fence shortcut east of **Burgh de Rott** now appears broken
### Removed
- The rest of the Easter related content been disabled

## [485] 2024-4-24
### Changed
- **Infernal eel** (TzHaar) fishing spots are now remastered
- The crushing animation using a hammer (used for crushing infernal eels) has been remastered
- Restored the **green skin** player appearance (fixes hdos/issues#2083)
- Certain objects are now allowed to upgrade and animate if the server is able to unset (temporarily remove) the object, when they previously weren't
  - *This is an experimental feature as it may have undesired effects*
  - The **corporeal beast portals** once again animate correctly (fixes hdos/issues#2063)
- Health bars for the **Moons of Peril** have been remastered
- Swapped the mullet and front-wedge **hairstyles** for males
- An alternative **smithing animation** has been restored (should be used for the Smith's Uniform) (fixes hdos/issues#2047)
- The **Loot Tracker Plugin** should now track Lunar Chests, Hunters' loot sacks and Colosseum Reward Chests
### Fixed
- **Skybox Plugin**: Custom Skybox themes will no longer reset when swapping from them on client boot
- Bushes, regular trees and Yew trees in **Shayzien** have been remastered (fixes hdos/issues#2090)
- The **Annoyance mute plugin** is now able to mute birds, Jad pets and teleport sounds it did not mute before (fixes hdos/issues#2068)
- Update cache with the latest game update
### Removed
- **Skybox Plugin**: `Extra brightness` no longer forces itself to reset on certain themes
  - *The Extra brightness option was never intended to make dark presets lighter, but the option resetting itself back caused some confusion and thus no longer resets*
- The **Easter** overhaul has been removed

## [484] 2024-4-17
### Fixed
- Invalidate Skybox plugin settings

## [483] 2024-4-17
### Added
- Razor Kebbit Tracking plugin (incorporated)
### Changed
- The **Menu Entry Swapper plugin** now allows for the 'Build' and 'Remove' options for the Construction skill to be swapped, following changes to Third-party Client Guidelines
- The **Skybox plugin** has been improved:
  - Skyboxes no longer turn off when restarting the client or relogging
  - The following options are now saved:
    - `Enable skybox`
    - `Skybox`
    - `Hide Scene`
    - `Sky Height Offset`
  - Changed the order and some of the labels in the sidepanel
  - Custom theme settings no longer get discarded when selecting another theme, as each theme now saves their settings individually
    - *Only the Skybox setting saves on custom themes, the rest under the Skybox section currently do not.*
  - Added an additional two custom theme presets that you can configure
  - Added new themes:
    - Modern
    - Sunny
    - Dusk
    - Mist
    - Moonlight
    - Draynor Gloomy
    - Incandescent
    - Ember
    - Flicker
### Fixed
- A **client crash** that would occur upon logging in with the Skybox enabled has been fixed
- **Varlamore music** should work once again
### Audio (thanks to `rsorchestra`)
- **Soundbank Patches**:
  - Remastered the Overdriven Guitar instrument (on Bank 5, Preset 29)
- **Music and Jingles**:
  - Updated music and jingles to HD Sounds, up-to-date with Easter & Varlamore Updates
  - Mor Ul Rek (HD Remix)

## [482] 2024-4-10
### Changed
- Restored the **Double- and Left eyepatches**
### Fixed
- A missing type of **Rockslug** has been restored
- The **Bracelet of Slaughter** remaster intended for v466 is now affecting the item properly
- Remove menu destination tile config due to conflict
- Update game cache with the latest update

## [481] 2024-4-3
### Fixed
- Update cache with the latest game update

## [480] 2024-4-2
### Removed
- Thok's memory has subsided (**april fools** has passed)

## [479] 2024-3-31
### Added
- You have become the mighty Thok
### Changed
- Upgraded the **Magic Butterfly Net** animations
- Excluded texture 607 (specular flesh) from upscaling to high as it makes it look visually worse
### Fixed
- A certain type of **Teak Tree stump** is now restored like the rest of them (fixes hdos/issues#2065)
- A change in model IDs has been fixed, and **Varlamore** sweetcorn no longer turns into part of Tempoross when picked
- **Turoth** models and sizes now match their respective combat levels (fixes hdos/issues#1022)
- **Basilisks** are restored once more

## [478] 2024-3-29
### Fixed
- Fix Orthogonal Projection when rendering models within interfaces (Varlamore travel interface)
- Fix a client crash caused by the Attack Styles plugin (fixes hdos/issues#2079)

## [477] 2024-3-27
### Changed
- Update **protocol** to 221
- The following improvements have been made regarding to the recent **Varlamore** game update
  - Remastered the following spellbook sprites:
    - Jewellery enchantments
    - Jewellery enchantments (enlarged)
    - Fortis Teleport
    - Fortis Teleport (unavailable)
    - Fortis Teleport (enlarged)
    - Fortis Teleport (enlarged, unavailable)
  - Improved the Kourend Teleport spellbook sprites to be more in line with the rest
- The default FPS has been lowered to 145 (was 500)
  - A warning has been added relating to client instability currently caused by skeletal animations
- Updated **map compatibility** for Varrock
  - Added the HDOS easter overhaul
  - Upgraded some OS easter assets
  - Restored maps are active once again
### Fixed
- Fixed JSwing illegalArgumentException **crash** that could occur on boot
- Fixed **transparent dialogs**
- Set **effect toggle widget** no longer overlaps special attack (fixes hdos/issues#2076)
  - *The widget while shown still has some issues we are aware of*
- Increased software model limits
- Public and clan chat options are no longer swapped
- The equipment bank interface widget has been fixed

## [476] 2024-3-20
### Fixed
- Some improvements have been made to the **Troll Stronghold**
  - The kitchen drawers no longer downgrade (fixes hdos/issues#2062)
  - Some collision with the northern and southern staircases has been fixed, and custom assets have been created to better align the models
    - *The remaining staircases remain to be fixed*
- Update map compatibility for the Misthalin region
  - Update Varrock (Varlamore)
  - Update Lumbridge (remove Leagues)
  - Update Alkharid (players can no longer get imprisoned in the Duel Arena after a server restart)
- Update cache
- Fix potential game crash

## [475] 2024-2-28
### Added
- More Varrock guard variants have been added for the **Children of the Sun** quest (fixes hdos/issues#2056)
  - The OS female Varrock guard missing a helmet has been changed to a remastered female Varrock guard (unarmed), as the model does not allow for helmet removal
  - The OS male Varrock guard wielding a mace instead of a sword has been given a mace
### Fixed
- Fixed some downgrading mining rocks in the **Shilo Village Mine** (fixes hdos/issues#2055)
- The fire of the **furnace in Burgh de Rott** now animates (fixes hdos/issues#1575 hdos/issues#2054)
  - The fire in Enakhra's Temple has received the same treatment and now also animates
- A HD bug where the icon of the female Crafting skillcape was untextured has been fixed (fixes hdos/issues#2053)
- Adjusted the item definitions for the partyhats, fixing the **green partyhat**'s face (fixes hdos/issues#2057)
- You can no longer walk into the void when passing through the **Haunted Mine**
- Removed large entities from **ExpendedNPCs** to prevent double bosses from showing up
- The animation of the whirlpool within the **Volcanic Mine** has been improved
- An incompatibility with **Rick** has been fixed. He now uses a Great Orb Project Wizard model instead.
- Update cache with the latest game update

## [474] 2024-2-22
### Changed
- #### **HD Pack 16**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets **The Inferno** / **Mor Ul Rek**, and introduces the following changes:
  - Textured all relevant **objects, NPCs, projectiles and items**
    - The **Obsidian cape (r)** and the **Tzhaar-ket-om (t)** have had their models remastered as well
  - Immersive **atmosphere and lighting** have been added
  - Several NPCs around the TzHaar area have been restored, including bankers and the Fight Caves warden
    - Remastered numerous **TzHaar creatures** to use new models, including the stronger TzHaar-Ket variants, Mol Ul Rek's shopkeepers, Terminal guides, the Inferno Warden and JalRek-Jad's healers
  - Several new objects have been added, such as the Inferno's entrance vortex and the custom Haar-Tok statues
  - 18 new **particle emitters** have been added
    - Also added a new (fiery) butterflies variant for the Fairy Ring
    - Added particles to various Inferno monsters and pets
    - The Inferno exit no longer stutters and now uses particles as well
  - Added custom support for ~70 different Tzhaar Chat-head dialogue expressions the server could send
  - Remastered **UI elements** of TzHaar-Ket-Rak's challenges scoreboard
  - Some outdated models have been improved
  - **Restored** the related areas:
    - **Musa Point**
    - **Karamja Volcano**
    - **Karamja Volcano Dungeon**
      - Remastered the cave entrance leading to the Dragon Slayer 2 quest room and added some lights in the quest room itself (to be remastered; part of DS2 when the time comes)
    - **Crandor**
    - **Brimhaven**
    - **TzHaar City**
      - Remastered Mor Ul Rek area
        - *Cave walls have been repaired, ground heights were adjusted, new objects have been added, and lava now glows fiercely, derived from the Tzhaar City lava in HD.*
    - Remastered **The Inferno**
    - **Infernal cape** (fixes hdos/issues#1115)
      - Its textures have been improved slightly
      - The ground mesh has been remastered
      - It is now formally a HD item (should not change anything)
      - *An item from a distant past has been spotted at the establishment of TzHaar-Hur-Zal's shop.. Perhaps you could investigate this claim?*
  - Other fixes:
    - TzKal-Zuk's healers no longer have a shadow floating above them
    - Fixed several **Z-Fighting** (flickering) issues, such as the Ancestral Glyph before engaging Zuk, and the TzHaar pollbooth variant
    - The **Venator bow**'s arrow positioning during its idle animation has been improved
      - *Due to limitations to the code rewrite, the uncharged version of the arrow has been removed during idle and move animations.*
    - The **JalTok-Jad** ranged attack slam is no longer able to disappear depending on your camera angle, and should now have better visibility
    - The **TzTok-Jad** ranged attack graphic no longer stutters
    - The **TzTok-Jad** ranged stomp no longer stutters as much (fixes hdos/issues#2035)
    - Other versions of **Elvarg** have been restored
    - Removed deprecated methods to override atmos for Mor-Ul-Rek that would prevent the **Skybox Plugin** from working correctly
    - Texture 335 (rocky) can no longer be upscaled to high due to issues that arose when over-upscaling it
- Made the following improvements to the **Defender of Varrock** quest:
  - Remastered the following NPCs:
    - Varrock Guard Captain (armed)
    - Added Varrock Female Guards
  - Restored the following NPCs:
    - Elias White (Ali the Wise)
    - Arrav
    - 52 Armoured zombies
  - Re-restored new variant NPCs:
    - Aeonisig Raispher
    - King Roald
    - Sir Prysin
    - Ambassador Ferrnook
    - Queen Ellamaria
    - Reldo
    - Captain Rovin
    - Treznor
    - Dimintheis
    - Varrock guards
  - Added basic atmos to the Tunnels during the quest (to be restored)
### Fixed
- A bug in **ExtendedNPCs** has been fixed where NPCs would be able to walk around when they shouldn't be
  - Fishing spots no longer walk around
- Fixed a buggy water tile near **Port Sarim**'s church
- An issue where an item sprite's ambient and contrast would not apply correctly has been fixed
- Updated map **compatibility** and fixed regressions for VarrockCity, Silverae, Weiss and Hosidius


## [473] 2024-2-15
### Fixed
- Fix clan chat

## [472] 2024-2-15
### Changed
- Update proto to 220
- Fix welcome screen banner
### Fixed
- The **Chat Commands** plugin will now work for the GIM channel (fixes hdos/issues#1964 hdos/issues#2051)

## [471] 2024-2-11
### Changed
- Restored/Remastered the following interface components:
  - The **Experience reward interface**
  - Several **GameFrame border kits** (48 sprites)
  - The **Activity Tracker orb**
  - Lots of icons including Minimap Orbs (Hitpoints, Prayer, Run), Account management, Wiki banner and more! (39 total)
  - Combat achievement progress bar
- Restored **Anvil Sparks**
### Fixed
- An issue where the custom fog effect during the **Phantom Muspah** encounter did not show has been fixed
- The (ornamented) **Mystic Steam Staff** is now using Oldschool's model once again, and the regular version has been upgraded (fixes hdos/issues#2040)
- Fixed an issue where a Third-age pickaxe would be equipped during the **Certers random event** (fixes hdos/issues#2017)
- Fixed an issue where objects under special circumstances would not be detected by plugins
  - This *may* fix an issue in the Hallowed Sepulchre, where certain things would not be highlighted
  - This *may* fix an issue in Chambers of Xeric where chests would not be highlighted
  - Fixed an issue where objects would not be highlighted by the **Agility plugin**
    - The Ardougne log balance show highlights correctly (fixes hdos/issues#2042)
- The **POH plugin** no longer confuses objects for underworld, and will no longer show minimap icons where they should not be
  - Portal nexus icons will no longer show in the Lumbridge Swamp
  - Carralanger teleport will no longer show near Baxtorian Falls (fixes hdos/issues#1694 hdos/issues#1929)

## [470] 2024-2-7
### Fixed
- Update cache
- Update NPC/lava dragon mappings (fix chat-heads)

## [469] 2024-2-6
### Changed
- Improved the **Annoyance mute** plug-in:
  - It now supports HD sounds (not all are 1:1, so keep this in mind when trying to add one manually)
    - *An ID can be flagged as HD by appending "h" or "hd" directly after the id*
    - Sounds that are mapped and sent by the server will already be patched automatically
  - Added support for several sounds that previously refused to be muted such as:
    - Cannon
    - Pets (27)
    - Woodcutting (6)
    - Mining (70)
    - Town Crier
    - Thralls
  - Sound IDs that require animations now check for HD correctly (e.g. teleports)
  - An option has been added for **Star Sprites**
  - ***If any options are still not working, let us know!***
    - Note: *Vorbis remains unsupported as it is not classified as a sound effect (used by newer revisions, such as the updated pickaxe special attack)*
### Fixed
- Invalidate `maps-cache` (fixes lingering custom assets bug)

## [468] 2024-2-5
### Fixed
- Fix custom assets (fixes hdos/issues#2034)
  - *If you are still experiencing issues, please delete `User\hdos\bin\maps-cache`*

## [467] 2024-2-5
### Fixed
- Attempt to fix custom assets
- Reverted the removal of an interface (fixes hdos/issues#2036)
  - *Unfortunately they seem to reuse Camdozaal's fake darkness for Zulrah's transition*

## [466] 2024-2-5
### Changed
- Remastered the **Prospector Helmet chat-heads** for all genders and versions
- Remastered **Breive** by the Smoke Devil dungeon entrance (fixes hdos/issues#2021)
- Upgraded OS-specific **Teak trees** (25 models in-total) (fixes hdos/issues#2025)
  - *Includes diseased, dead and trunk versions*
- Remastered **Runite Minors** in the Motherlode Mine (fixes hdos/issues#1786)
- Changed behaviour for the **Spiked Ponytail** hairstyle for female characters
  - *Before, certain hats would chop off the ponytail. Now it persists. Hiding the hairstyle is determined by the server, so this change is experimental.*
- Made the following improvements to the **Ruins of Camdozaal**:
  - Remastered much of the terrain (subject to change, still missing icy aspects)
  - Added many lights
  - Added underwater
  - Removed a fake overlay that caused the atmosphere to become darker than intended
  - **ExtendedNPCs**: Fishing schools and Rubble no longer walk around
- Remastered the **Cursed Goblin Staff** (fixes hdos/issues#1871)
- Restored the **Mystic Steam Staves**
- Created a remastered brown (ghost) **Bedsheet** version, and now appears when applicable instead of always showing the green variant (fixes hdos/issues#1804)
- Remastered the silver (enchanted) jewelry:
  <details>
  <summary>Click here for a full list</summary>
  
  - Jade Bracelet
  - Flamtaer Bracelet
  - Jade Necklace
  - Necklace of Passage (1-5)
  - Jade Amulet
  - Amulet of Chemistry
  - Opal Bracelet
  - Expeditious Bracelet
  - Opal Necklace
  - Dodgy Necklace
  - Opal Amulet
  - Amulet of Bounty
  - Topaz Bracelet
  - Bracelet of Slaughter
  - Topaz Necklace
  - Necklace of Faith
  - Topaz Amulet
  - Burning Amulet (1-5)
  </details>

### Fixed
- Some mismatched behaviour that caused faces to disappear from the Barbarian Assault **Runner- and Ranger hats** has been fixed (fixes hdos/issues#1617 hdos/issues#1638)
- The `Display Nex's shouts in chat` option now saves correctly
- Fixed a rendering issue that was caused by high polycount
  - The **Reborn Vet'ion Jr** Pet inventory icon now displays correctly (fixes hdos/issues#1523 hdos/issues#1901)
- **Cute- and Evil creatures** from the Path of Glouphry quest now animate correctly (fixes hdos/issues#1963)

## [465] 2024-1-31
### Fixed
- Fix chat state (overhead text not appearing)
- Fix plugin UI

## [464] 2024-1-31
### Changed
- Made the following changes to the **Children of the Sun quest**:
  - Remastered **Alina** and **Noah**
  - Restored **King Roald** and **Aeonisig Raispher**
  - Added several new **Varrock Guard** variants
  - Remastered the **Bandits**
  - Textured all **Knights of Varlamore**, **Prince Itzla Arkan**, **Servius, Teokan of Ralos**, **Furia Tullus**, **Ennius Tullus**
  - Remastered **Hosa**
  - Remastered all respective chat-heads
- Remastered several **Mahogany Trees**
### Fixed
- The Jail on top of Varrock's castle has been fixed
- The **Plugin settings** now correctly state that `Ctrl-S` can be used to toggle the settings panel (this was changed from double-tap shift)
- Fixed client components
- Update cache

## [463] 2024-1-27
### Changed
- #### **HD Pack 15**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets **Scurrius, the Rat King** boss encounter and introduces the following improvements on top of the initial area restore:
  - Textured all relevant **objects, NPCs, projectiles and items**
  - Remastered **Historian Aldo**, who now uses a HD character model
  - **Bloom** (HDR) has been added to both the lightning and blue magic attack
  - Upgraded the rigging of the **bone mace** to use proper HD labels (fixes defend animation with shield)
  - Remastered the **Bone Shortbow** and added a bowstring along with HD rigging to support the updated drawback animation
  - The lightning attack **animation** has been improved
  - Added **sewer waterfalls**
  - Added two new **particle emitters**: 'Stinky cheese cloud' and 'Dust cloud'
  - Some of the flames on torches no longer disappear when `hide roofs` is enabled
  - Some lingering **pointlights** inside the walls have been removed, and some missing ones have been added
  - The broken metal bars have been re-added and have been remastered
  - Some invisible objects causing shadows to be projected improperly have been removed, and some lights have been dimmed slightly to compensate
  - Fixed some minimap issues (mostly)
  - Improved several animation stutters, such as the one from the falling debris attack
  - Improved some heights around the area
  - The colour of some misc post-rework Shayzien planks have been changed to better match the HD style
- The random event version of **Count Check** has been upgraded
### Fixed
- Fixed some NPC definitions that caused them to have unintended displacements (Nex's minions should no longer be in the wall)
- The **Imbued Heart** no longer makes a Trick or Treat sound (fixes hdos/issues#2020)
- Some unclickable areas in **The Wardens** boss room in the Tombs of Amascut raid have been fixed (fixes hdos/issues#1958)
- The **Miscellania** map has been rebuilt to work with the latest game update, and no longer shows walls missing (fixes hdos/issues#2024)
- A faulty vertex on the female **Virtus robe legs** has been fixed (fixes hdos/issues#1928)
- The **Varrock** guards, stray dog and children have been ordered to pack up their Christmas outfits
- **Guthan's chainskirt** has had the same treatment as the Ahrims robeskirt, and certain polygons are no longer invisible in both HD- and SD-mode
- The **SOTE** area in the underground pass involving Lady Trahaearn has been pulled out of the void once again (permanently)

## [462] 2024-1-24
### Changed
- Initial restoration of the **Scurrius** boss area
  - Black tiles no longer cover the Scurrius area
### Fixed
- Update cache

## [461] 2024-1-21
### Fixed
- Update cache (fix random holes)

## [460] 2024-1-21
### Removed
- Snow overhauls have been removed

## [459] 2024-1-12
### Fixed
- Music changes scheduled for v458 are now in effect, and related music issues should now be fixed (see audio section)
### Audio (thanks to `rsorchestra`)
- **Soundbank Patches**:
  - Remastered the Picked Bass instrument (on Bank 0, Preset 34)
  - Remastered the Fretless Bass instrument (on Bank 0, Preset 35)
  - Replaced the Choir Aahs instruments with the original Choir Oohs from OSRS (on Bank 2, Preset 0)
  - Added new String Ensemble instruments (on Bank 5, Presets 48 and 49)
  - Added new Overdriven Guitar instrument (on Bank 5, Preset 29)
  - Added new Alto Saxophone instrument (on Bank 5, Preset 65)
  - Added new Choir Aahs instrument (on Bank 5, Preset 52)
- **Music and Jingles**:
  - The Vault (HD Remaster)
  - The Fallen Empire (HD Remix)
  - Pick & Shovel (HD Remix)
  - Sea Shanty 2 (HD Remix)
  - Darkly Altared (HD Remix)
  - All Music and Jingles pertaining to The Path of Glouphrie quest have been remastered to HD
  - Dot's Yuletide (HD Remaster)
  - Master of Puppets (HD Remaster of "The Price is Wight")
  - His Faithful Servants (HD Remix of "Dying Light")
  - The Adventurer (HD Restore) (only present in cache)
  - Elf Rest Musician (HD Restore) (only present in cache)
  - Skill: Level 99 Achieved (HD Restore) (only present in cache)
  - Reign of the Basilisk (HD Remix)
  - Eye See You (HD Remix)
  - All Music from the Children of the Sun quest have been remastered to HD

## [458] 2024-1-10
### Changed
- Update proto to 219
- Removed the Christmas event
  - It has stopped snowing
  - Snow has started to melt in Varrock
- The **forest south of Castle Wars** has been restored
  - Some minor terrain upgrades
  - Terrain and heights around the smoke dungeon cave entrance have been remastered
  - Added Fairy ring light
  - Upgraded smoke clouds
  - Fixed several issues regarding rivers and bridges (e.g. bridge supports now aligning properly)
- The eastern part of the **Dorgeshuun Mines** has now been restored
### Fixed
- The plough in the OS Christmas event has been removed
- Removed some weeds in **Morytania** for better collision manners (fixes hdos/issues#2014)
- The following improvements have been made to the **Underground Pass** chasm area (fixes hdos/issues#2018)
  - Restored the Staircase by the well
  - Restored the roof of the witch's house
  - Repaired one of the bridges in the northern section
  - Upgraded some minor terrain
  - The area whilst inside the well has been added to the MapFilter
- The ornamented version of the **Osmumten's Fang** is now wielded correctly

## [457] 2023-12-25
- Snow pile should now be visible too

## [456] 2023-12-25
### Fixed
- Crystals during the OS Christmas event are now showing correctly

## [455] 2023-12-23
### Added
- Beavers, Fox and Pheasant pets have been added to the **Summoning plug-in** (fixes hdos/issues#1984)
- Skyboxes are now able to be part of a **Skybox plug-in** theme preset
  - Winter theme has been added
  - A skybox has been added to the Underwater theme
### Changed
- Restored the **Candy cane**'s `spin` option (to access, right-click the item whilst wearing it) along with its idle animation (fixes hdos/issues#2012)
- Improved a Secret's hint
- Torches in the **Rogues Den** will now always be visible, even when `hide roofs` is enabled (fixes hdos/issues#1996)
  - *This was not a bug on our side, but it seems logical to make this change*
- Remastered the missing recoloured variants of the **Lost Bag** (fixes hdos/issues#1976)
- Remastered the **Barbarian Fishing** animation, which now performs the HD animation with the Barbarian rod in hand (fixes hdos/issues#1920 hdos/issues#1970)
  - *Before, it played an OS animation (sometimes with a buggy double rod in hand)*
  - *In HD, they removed the Barbarian rod and replaced it with a generic one*
- Restored the Banshee Outfit with the **Christmas Ghost Outfit** (fixes hdos/issues#1968)
- The **Make-over Mage** interface now uses HD chat-heads (fixes hdos/issues#1959)
- The **Blade of Saeldor** and **Vesta's Longsword** now use longsword idle animations (fixes hdos/issues#1949)
- Remastered the **Blessed Saradomin Sword** (fixes hdos/issues#1923)
- Restored the updated **two-handed sword** animation stance
  - *Currently only applied to godswords, to make them feel special and retain both styles, but do let us know what you think!*
### Fixed
- The fence gate is no longer closed by the **OS Christmas event**
- The area by the Suplhur Lizards in the **Karuulm Slayer Dungeon** is no longer missing with MapFilter enabled (fixes hdos/issues#1875 hdos/issues#2010)
- Some missing objects and misaligned doors in the **Ardougne Castle** have been moved or added, previously making parts inaccessible (fixes hdos/issues#2003)
- Some rocks have been moved in the **Silverae** area, so players may no longer move through them (fixes hdos/issues#1993)
- **Jad**'s ranged attack animation has been improved (fixes hdos/issues#1980)
- **Sir Tiffy** no longer sneakily slides around with his bench
- The **Giant Sea Snake** now appears correctly (fixes hdos/issues#2000)
- Fremennik sea boots no longer show as a head in the **Lost Property Shop** (fixes hdos/issues#1965)
- Digging with a spade no longer causes the sound to play twice (fixes hdos/issues#1943)

## [454] 2023-12-19
### Added
- Added secrets
- Christmas Presents have been re-added and Falador has had a frosty overhaul!
### Changed
- Several improvements have been made related to **Christmas**
  - Welcome screen icons have been updated (now show Christmas chat-heads)
  - NPCs with a Santa Hat equipped will now also show it in their chat-head models
  - Additional NPCs are now wearing Santa Hats
  - Several new custom terrain overlays have been added (icy/snowy variants) for roads
  - Some man/woman partygoer NPCs from a higher revision have been removed, and instead now wear Santa Hats
  - Christmas Music will no longer always start with the same track
### Fixed
- Players will no longer teleport when running past the snowy evergreen trees by Ice Mountain
- The fencing around the Christmas event area has been improved
- The ruins where **Nex**'s minions stand during the encounter have been altered to indicate and respect collision more clearly (fixes hdos/issues#1975)

## [453] 2023-12-13
### Fixed
- Snow has returned to several areas affected by last update's fixes

## [452] 2023-12-13
### Changed
- Improved **Oldschool Christmas event** compatibility
  - Updated cache
  - Maps have been improved
    - Snow has been upgraded
    - Objects have been improved
    - Map downgrades have been fixed
  - Some NPC santa hats have been re-added
  - **Jack Frost** and their server-side chat-head emotions have been restored
    - This applies to all NPCs using his emotions
- The **snow globe** has been remastered (fixes hdos/issues#413)

## [451] 2023-12-3
### Changed
- Christmas is closing in... (see v312 for detailed changes)
  - Mild snow particles have been added instead of the more intrusive particles from last year
  - More changes will follow as Christmas closes in

## [450] 2023-12-1
### Changed
- Update RLS to `1.10.17.1` (fixes a bunch of broken plugins)
### Fixed
- Fixed Settings and other windows that were not working on leagues

## [449] 2023-11-26
### Removed
- **Witches' Chronicles** has been removed
- Maggie has departed and all **Halloween** decorations have been cleaned up
### Changed
- The login screen music has returned to normal (fixes hdos/issues#1994)
### Fixed
- Al-Kharid is restored once again (fixes hdos/issues#1995)
- Updated cache (fix chat-heads)

## [448] 2023-11-17
### Added
- Added the Halloween skybox to the theme presets in the Skybox plugin
### Removed
- Maggie has ordered the removal of **Halloween** as they're getting ready to depart
  - **Witches' Chronicles** will be removed soon
  - Halloween overhauls of Edgeville, GE and Lumbridge have been removed
  - The darkness has been lifted for all areas
  - Items and NPCs have returned to normal
### Fixed
- Further improve human skeleton patch (fixes hdos/issues#1991)
- Updated the **Lumbridge** map with the latest League update

- ## [447] 2023-11-15
### Fixed
- Patch human skeletons

## [446] 2023-11-15
### Changed
- Remastered **Sanguine Torva**
### Fixed
- The Lady Trahaern cave in the **Underground pass** has been pulled out of the void
- Initial human skeleton patch (fixes todays crash)
- Potential fixes for crashes introduced with the proto update
- Update cache

## [445] 2023-11-8
- Update proto to 218

## [444] 2023-11-2
### Changed
- The following weapons have been reverted following last update's feedback:
  - *This is still subject to change*

  | Affected                | Changed to          |
  |-------------------------|---------------------|
  | Occult necklace         | Unchanged           |
  | Occult necklace (or)    | Reverted            |
  | Dragonbone necklace     | Split dragontooth   |
  | Toxic staff of the dead | Reverted            |
  | Osmumten's fang         | Reverted (fix anim) |
  | Osmumten's fang (or)    | Reverted (fix anim) |
  | Elder maul              | Reverted            |
  | Elder maul (or)         | Unchanged           |
  | Zaryte vambraces        | Remastered          |
### Fixed
- Fang's prior animation upgrade has been fixed

## [443] 2023-11-1
### Changed
- The following weapons have been upgraded:
  - *This is experimentation and subject to change, as anything* 

  | Affected                | Upgraded to            |
  |-------------------------|------------------------|
  | Occult necklace         | Arcane stream necklace |
  | Occult necklace (or)    | Arcane blast necklace  |
  | Dragonbone necklace     | Demon horn necklace    |
  | Toxic staff of the dead | Chaotic staff          |
  | Osmumten's fang         | Chaotic rapier         |
  | Osmumten's fang (or)    | Primal rapier          |
  | Elder maul              | Chaotic maul           |
  | Elder maul (or)         | Primal maul            |
  | Zaryte vambraces        | Mercenary's gloves     |
### Fixed
- Fixed the following in the hard-mode boss encounter after the Witches' Chronicles quest
  - health will now be calculated correctly on regular hard-mode after entering nightmare-mode
  - The boss should no longer freeze occasionally

## [442] 2023-11-1
### Changed
- The Halloween rats no longer squeeck as loud!
- An Escape rift in the **Witches' Chronicles** boss fight now despawns any existing rifts on the same spot
- Update cache (fix chat-heads)
### Removed
- The sound of the updated Fang animation has been removed

## [441] 2023-10-30
### Changed
- Some changes have been made to the fight of the **Witches' Chronicles** quest
  - You can no longer get hit by Unstable Anima whilst inside the darkness
  - The `COME CLOSER!` boss special should no longer have inconsistent timings
  - Rifts now show their time remaining
  - Added NIGHTMARE Mode!

## [440] 2023-10-29
### Changed
- #### **HD Pack 14**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the second batch of **Forestry**:
  - Remastered beehives, fox traps and other misc NPC objects
  - Remastered all **Felling Axes**
    - Also improved alignments and offsets
  - Remastered the **Pheasant outfit**
  - Remastered **Twitcher's gloves**
  - Remastered **Entlings**
  - Remastered the **Dryad**, and now uses fully HD mesh
  - Textured the **Poachers**
  - Restored **orange Foxes**
  - Remastered **Frightened Foxes**
  - Restored Pheasants and added remastered chat-heads
  - **Campfires** now use smoke particles
  - Other changes:
    - Remastered and updated the animation of the **Osmumten's Fang**
- Updated login music to Scape Theatre (Created by `RS Orchestra`!)
- Added H'Ween atmos to login scenes
- Restored **Spria**, the slayer master in Draynor

## [439] 2023-10-27
### Changed
- Restored coloured **fires** and campfires
- A secret in the Witches' Chronicles quest can now be completed on a free-to-play world
- Restored the **Web cloak**
### Fixed
- Fix the introduction paging on the Witches' Chronicles quest
- Custom quest messages now recolor on opaque chat

## [438] 2023-10-26
### Changed
- Improved the **hard-mode boss** encounter for Witches' Chronicles:
  - **Harmonic Anima** in the hard-mode boss encounter will now cause additional energy to be restored when your sanity has already been fully replenished
  - A mechanic in the final phase has been made clearer
  - The explosion radius of **Volatile Anima** has been reduced to 4 (from 5) in the final phase
  - The `COME CLOSER!` boss special has been improved
    - Damage has been reduced significantly, but now heals the boss
    - Visual elements have been improved, and a voice line was added
  - The `THERE IS NO ESCAPE!` boss special now always spawns an Harmonic Anima rift after the player manages to escape
  - An **extra dialogue** has been added upon victory
  - *Although hard-mode is meant to be difficult, some of the overlapping mechanics made it too punishing. Hopefully some of these changes will make the feel more rewarding without overly devalueing its difficulty*
- Other improvements to **Witches' Chronicles**:
  - An **unreachable dousing bucket** has been moved
  - Professor Oddenstein now gives **additional hints** about the Ingredient puzzle, if you talk to Lottie again

## [437] 2023-10-25
### Fixed
- Made the following improvements to the ingredient puzzle during the Witches' Chronicles quest
  - Simplified a combination that forced you to reset the puzzle
  - You can no longer get yourself stuck permanently if you really tried
    - *This hasn't actually happened to anyone, but it can theoretically happen*

## [436] 2023-10-25
### Fixed
- An unreachable fire in the Witches' Chronicles quest has been fixed

## [435] 2023-10-25
### Added
- Custom quest: **Witches' Chronicles**
### Changed
#### Halloween
- Halloween has returned!
  - Restored the 'Trick' emote with the original 'Trick or Treat'
  - Several NPCs have magically transformed into spooky variants
  - Music changes
    - Restored several additional tracks
  - Several assets have been upgraded
  - Some OS bat banners have been fixed
  - Pyromancer clothing has turned into Witch and Warlock clothing for females and males respectively
  - Halloween overhauls are now active in:
    - Lumbridge
    - Draynor
    - Port Sarim
    - Rimmington
    - Grand Exchange
    - Edgeville
- Quick Settings will now dock on close instead of hiding (users often got confused)

## [434] 2023-10-19
### Added
- **DOOM!**
### Changed
- Update RLS to `1.10.14.2`
### Fixed
- Unranked group ironman, quest speedrunning and other misc chat-icons now display correctly (fixes hdos/issues#1321)
- ExtendedNPCs: Corporeal Beast no longer shows, as two of them could appear

## [433] 2023-10-13
### Added
- `Hide scenery` for cutscenes
### Fixed
- Fixed a faulty chat-head animation

## [432] 2023-10-12
### Added
- Added the option to hide plugins while **cutscenes** are playing
  - This is enabled by default, the setting can be toggled under the `Gameplay` tab
### Changed
- Improved **chat-head expression accuracy**
- Quick Settings will now force the **sidebar** visible if it's hidden when using the hotkey
- Improved **ExtendedNPCs**: The Nightmare is now static
### Fixed
- Fix **default world** (via your first favorited world within the login screen)
- Remove the old `Shift + Shift` default **Quick Settings keybind** (only the user configured one will apply now)
- Fix a case where the window would not contain itself on screen when resizing
- Fixed a crash that could occur when performing a faulty oldschool chat-head animation
- Fix **collection log** searching
- Fix RS Account login problems through the **Jagex Launcher**

## [431] 2023-10-8
### Added
- Add plugin: `hub/clan-chat-webhook`
### Changed
- Restored 92 player identity kits (upgrade **new hairstyles**) (fixes hdos/issues#938 hdos/issues#1619 hdos/issues#1889)
  - **Note the following:** 
    - *These hairstyles are not 1:1 with oldschool and thus may differ*
    - *Male may contain a few duplicates*
    - *We have chosen the closest possible HD variants*
    - *Some hairstyles have been moved around to adjust for server-side behaviour, fixing clipping in the process*
- Update the Hair Dresser interface previews for the new HD hairstyles
- Increase the username and password field limit from 32 to 64
- Importing your RS Profiles data will now automatically restart plugins for you
### Fixed
- Fix a layout bug on initial startup with UI scaling enable (resizing/login would fix it before) 
- Fix a bug where importing your RS Profile data multiple times would not replace the existing data
- Fix misc. in-game settings that would not persist or respond while selecting them by search
  - ex: selecting unavailable color, transparent chat box
- Users can now change keybind settings from the search results (keys used to type into the search instead)



## [430] 2023-10-5
### Fixed
- Fix settings that were not persisting between restarts (but worked between profile swapping)
- **Npc Indicators** now marks non-interactable npcs (useful when using IDs)

## [429] 2023-10-5
### Added
- Add the following settings / features (currently under the `Interface` settings tab):
  - Window Settings:
    - Remember client position
    - Resize type (defaults to `Keep game size` now)
    - Always on top
    - Exit warning (defaults to `Logged in`)
    - Lock window size
    - Sidebar hotkey (defaults to `Ctrl + F11`)
    - Sidepanel hotkey (defaults to `Ctrl + F10`)
    - Quick Settings hotkey (mentioned above)
    - Full Screen hotkey (mentioned above)
    - Drag hotkey (continues to be `Alt`)
    - Block extra mouse buttons
    - Use wiki item prices
  - Notifications:
    - Game message notification
    - Notification timeout (Linux only)
    - Test Notification (to help debug notifications)
    - Set custom sound (helps change your custom sound, if enabled)
- **Note** Settings should be automatically migrated and merged into your profile.  
  The value of settings that were previously not exposed continue where they left off when the profile was imported (if it was).
### Changed
- Restored all **corrupt dragon** (fixes hdos/issues#1735 hdos/issues#1939)
  - Remastered the new pieces:
    - Corrupt dragon dagger (poisoned)
    - Corrupt dragon spear (poisoned)
    - Corrupt dragon sword
    - Corrupt dragon warhammer
    - Corrupt dragon claws
    - Corrupt dragon 2h
    - Corrupt dragon boots
- Users can now search for settings from the Quick Settings that exist outside of the `Plugins` tab
  - note: Only non Jagex settings will be searchable from Quick Settings (HDOS/Custom settings).
- The following keybinds have been changed and are now configurable:
  - Quick Settings: `Ctrl + S` (many users have had problems with `Shift + Shift`)
  - Full Screen: `Ctrl + Enter` (to avoid clashing with RLs default keybind for the side panel)
- **Npc Indicators** now supports highlighting NPCs by **ID** (instead of just name)
  - You may mix names with ids.
  - This should provide most of the requested features from `hub/better-npc-highlight`
- Transparent sidebar will be initially forced back on this update.   
  (Most users disabled it to the lacking the settings added this update)
### Fixed
- **Extended NPCs**: Dagannoth Kings no longer show to avoid confusion
- The `public` and `trade` chat states no longer swap between logins (fixes hdos/issues#1899)
- Camera key remapping now filters inputs while any modifiers are active (ctrl,alt,shift)
- Update cache (fix chat-heads)
- Increase the precision for the brightness setting between client restarts (for more fine tuned values)

## [428] 2023-9-28
### Added
- Added atmos to **Scar Essence Mine**
### Fixed
- Updated **The Scar** map with the latest game update
- Update cache

## [427] 2023-9-26
### Added
- Add plugin: `core/grandexchange`
- **Ground Items**: add the ability to de-prioritize hidden items within the menu

## [426] 2023-9-26
### Fixed
- The **Penguin suit** has been restored and now animates (fixes hdos/issues#1509)
- Fixed a HD bug where some of the textures on the **Ahrim's robeskirt** were misplaced (fixes hdos/issues#1551)
  - *Another bug caused some polys to disappear. The model should now look better in both HD and SD*
  - *The darkness on the robes is intended in this revision* (fixes hdos/issues#595)
- Some black tiles in the **Varrock Sewers** have been cleaned up, and the torches have been restored (fixes hdos/issues#968 hdos/issues#1897)
- Raids plugin: enable/fix friends chat information within the overlay 
- Fix known crashes

## [425] 2023-9-22
### Added
- Added more easter eggs
### Changed
- Increased the timer of **Star Sprites**
- Restored the **Meiyerditch Laboratories**
### Fixed
- The chest by the Ceremonial Brazier on **Lunar Isle** no longer displays as a bench in certain situations and has been restored
- The Saradomin godsword ornament kit **sprite** has been fixed (fixes hdos/issues#1911)
- Z-Fighting on one of the treasure trail **map interfaces** has been fixed
- A **missmap** on certain morytania-styled cave doors has been fixed (fixes hdos/issues#370)
- Fix "null" players from showing in the right click menu in crowsed areas
- Potential fix for some underground areas displaying as bright when they shouldn't be
- **Gilbert**'s remaster has been re-enacted


## [424] 2023-9-20
### Fixed
- Update proto to 217
- Updated cache
### Changed
- Upgraded current welcome screen icons

## [423] 2023-9-19
### Fixed
- Fix reported micro-stutters that occur from the `group-ironmen-tracker` plugin

## [422] 2023-9-19
### Removed
- Reverted **Tumeken's shadow** animation upgrades
### Added
- Add plugin: `hub/group-ironmen-tracker`
### Changed
- Improved the garden terrain for the Twisted Theme in **Player-owned-houses**
- Warn users about side effects when disabling `Ground Decorations` in the display settings
- Update `js5connect_outofdate` error screen
### Fixed
- Add support for favoriting worlds > 512 in the login screen (as a default world)
- Ground items: fix loot tracking for items dropped by the player while running (fixes hdos/issues#1909)
- Fix performance hitches when the World Map and the Hiscore plugin are both on screen

## [421] 2023-9-17
### Changed
- Added shorelines to **Corsair cove**
- Improved **Wilderness boss** areas
  - Replaced lightbeam in the Vet'ion room with real lights (fixes hdos/issues#1913 hdos/issues#1916)
  - Upgraded terrain and added atmospheres
- **Skybox plugin**:
  - Add a "Extra Brightness" setting
  - Overriding a skybox's colors will only apply when set to a non-default value
### Fixed
- The pot boiler used during the Rag and Bone man quests now animates properly (fixes hdos/issues#1546)
- Improved Extended NPCs (fixes hdos/issues#1912)
- Fix `quick-hop` in the **world hopper** plugin
- Fix **loot tracker** for misc. npcs (entity hider might not hide some npcs)
- Possible fix for a long standing bug where users are unable to click npcs/object after logging in or hopping worlds
- Fix known crashes

## [420] 2023-9-16
### Changed
- #### **HD Pack 13**
  HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the **Giants' Foundry** and introduces the following changes:
  - Improved the **Atmospheric effects** and added immersive lighting
  - Textured all objects
  - Remastered **Kovac**
  - Textured the **Smiths outfit**
  - Remastered **terrain**
    - Created new lava and water overlays that blend
    - Increased complexity in terrain pattern
    - Fixed some Z-Fighting and Alpha rendering issues 
  - Other changes:
    - Remastered the **Warped sceptre**
    - The glow on **Tumeken's shadow**'s attack graphic has been improved
    - Upgraded animations for Tumeken's shadow
### Fixed
- Fix entity hider to hide actions related to hidden things
- Fix plugin: `hub/wikisync`
- The terrain in morytania next to the Digsite barge has been fixed (fixes hdos/issues#1907)
- Vasa/crystals in the **Chambers of Xeric** raid should no longer go invisible under certain circumstances
- Fixed an oldschool bug where a green pixel would appear when performing the Warped sceptre's animation
- Dragon bolts no longer appear as Dragonstone-tipped bolts on certain stack sizes

## [419] 2023-9-13
### Changed
- **The Path of Glouphrie** quest has been restored (fixes hdos/issues#1887)
  - Fixed **Castle wars regressions**
    - Restored the **Poison Waste Canyon**
    - Restored the **Poison Waste Dungeon**
    - Restored the eastern Poison waste forest (connecting Castle wars to the Canyon)
  - Restored the following **NPCs**:
    - Elkoy is once again restored under certain conditions
    - Argenthorg
    - Advisor
    - Bolrie
    - Monoliths
    - Longramble
    - Spirit tree (diseased/cured)
    - **Children** and Hazelmere have been restored once again
    - Restored **Warped Terrorbirds**, **Terrorbird Guards** and **Warped Tortoises**
  - **Tree Gnome Village** and its dungeon have been restored once again
  - Remastered the Golrie's storeroom/**Monolith room**
    - Upgraded some gates in the restored dungeon too
    - Restored the lectern **cutscenes**
      - Remastered the lectern **interface**


## [418] 2023-9-13
### Added
- Add plugin: `hub/wikisync`
### Changed
- Added and improved the **Venator bow**'s animations and stance
  - A distinct GFX has been added for the uncharged variant
### Fixed
- Removed the excessively large clickbox on **The Wardens** in the ToA raid
- Misc. stability fixes
- Update cache (fix chat-heads)

## [417] 2023-9-12
### Fixed
- Add plugin: `hub/barrows-door-highlighter`
- Add plugin: `hub/c-engineer-completed`
### Changed
- Dirty water now appears when the waterwheel in the **Motherlode Mine** stops working
- Restored and fixed the POH **Lectern tablet icons** (fixes hdos/issues#1827)
### Fixed
- Fix known crashes
- Possible fix for Whisper/TOA frame stutters and client exits
- Lootbeam textures now really show correctly in **Standard Detail mode** (fixes hdos/issues#1854)
- Fix Quest Helper: Lunar Diplomacy
- Fix Vardovis blood CAPTCHA (fixes hdos/issues#1856)
- Fix TOA/ObeliskPuzzleSolver
- The barrel of staves in **Zaff's shop** is now able to appear empty when there are no staves left to buy
  - The model for the empty barrel has been remastered
- Some shorelines in the ocean above the **Piscatoris Fishing Colony** has been fixed (fixes hdos/issues#1846)
- **Dink:** Fix serialization for generic notification data

## [416] 2023-9-10
### Added
- Added a toggle to display **Nex's shouts** in the chat (revert to OS behaviour) under `Gameplay > HDOS`
### Changed
- The **Lava battle- and Mystic lava staves** no longer appear to be a regular battlestaff with an upgrade kit
  - Also remastered the new models
- The following **Treasure Trail items** have been upgraded in the HD style:
  - Monk's robes (t) and (g)
  - Dragon scimitar (or)
  - Dragon defender (t)
  - Dragon chainbody (g)
  - Dragon boots (g)
  - Frog slippers
  - Bronze-, Iron-, Steel-, and Mithril (g) and (t) sets
  - White-, Red- and Blue- cavaliers
  - The Pith helmet has been restored
  - Golden Apron and -Chef's Hat
  - Pink- and Gold elegant outfits
### Fixed
- Tiles in the last phase of **The Wardens** in the Tombs of Amascut raid will no longer disappear when floor decorations are disabled (fixes hdos/issues#1848)
- **Ahrims** is no longer see-through (again)
- **Godsword** sound effects no longer play twice
- **Dink:** Fix serialization for non-discord integrations
- **Quest helper:** fix Lunar Diplomacy
- Suppress GL errors for **macs**
- Fix known crashes

## [415] 2023-9-9
### Fixed
- Fix Custom menu swaps plugin

## [414] 2023-9-9
### Fixed
- Update the player/npc stacking algorithm to match OS behavior (fixes hdos/issues#98 hdos/issues#203)
    - Ex: Nightmare Husk and God-wars minions (smaller NPCs stacked under larger ones)
### Changed
- Update RLS to `1.10.11.3`
- Several graphical effects in the **Tombs of Amascut** raid now glow in the dark
  - The purple flames on the loot chest now also glow slightly
- You can now move whilst performing any variant of the **Dragon Pickaxe** special
- **Ahrims** is no longer see-through
- A bush in the **Ferox enclave** and other areas has been textured
- **Fog** is now less aggressive when view distance drops below 3
### Fixed
- Fix plugin: `hub/wikisync`


## [413] 2023-9-8
### Fixed
- The interaction of the pay-dirt sack in the **Motherlode Mine** has been fixed (fixes hdos/issues#1812)

## [412] 2023-9-7
### Changed
- Restored **Yulf Squecks** and Rellekka-Miscellania's **Sailor**
- The (filled) **Tithe farm** deposit sack has been remastered (fixes hdos/issues#1764)
  - The empty variant has also had its colors adjusted
### Fixed
- Fix known crashes
- Fixed false-positive npcs hiding when reaching 0 hp
- Fix dukes attack animation in his second phase
- Fix the sidebar from becoming visible on world hop
- The following improvements have been made to the **Motherlode Mine**
  - The railing of the waterway no longer disappears when hide-roofs is enabled
  - Some old pebbles that were floating in the air slightly have been removed from the pay-dirt sack
  - The upper part of the ladder is no longer untextured when going up the ladder
- **Myth's Guild** stairs will no longer disappear when ground decorations are hidden
- The **Whisperer** and **Vet'ion and Calvar'ion** should no longer disappear during the fight
  - *Entity hider 'hide dead NPCs' may still cause them to*
- **Dragon claws** will no longer play OS sounds when performing the special attack
- Other players' shield damage hitsplats will no longer appear as poison
- Fixed TOA Update panel from not closing
### Removed
- Removed warnings about previously unsupported Jagex accounts

## [411] 2023-9-6
### Changed
- Oldschool-specific axes have had their animations upgraded
- A series of projectiles used in the Arceuus Reanimation spells and other activities have been restored
- Magic potions now reflect their oldschool color
- Restored **King Vargas** and **Advisor Ghrim** (thanks Kris!)
- Improve setting UI for the following plugins
  - Dink
  - World Map
  - Barbarian Assault
  - Team
  - Friend Notes
  - XP Drop
  - Runecraft
  - Kitten Tracker
### Fixed
- Update OS cache (fix chat-heads)

## [410] 2023-9-6
### Changed
- Remove debug information for failed logins through the launcher.

## [409] 2023-9-5
### Added
- Add plugin: `core/world-hopper`
### Changed
- #### **HD Pack 12**
  This version marks our twelfth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the **Motherlode Mine** and introduces the following changes:
  - The mine has been overhauled in the style of Falador's Dwarven Mine
    - Custom assets have been created to reflect this
    - Light-sharing on the ore veins has been corrected
    - The waterwheel now uses a more modern HD animation and no longer gets stuck whilst spinning
    - New lights have been added
  - The **Prospector** outfits have been textured
    - *Some of the rigging has also been somewhat improved, but a full mesh rework has not been provided in this update*
  - **Prospector Percy** has been fully upgraded
  - The following pickaxes have been upgraded: (fixes hdos/issues#1761 hdos/issues#1763)
    - Black pickaxe
    - Gilded pickaxe
    - Dragon pickaxe (or) --> Restored with the Gilded dragon pickaxe
    - Dragon pickaxe (upgraded)
    - Crystal pickaxe
    - Crystal pickaxe (inactive)
    - Corrupted crystal pickaxe
    - Infernal pickaxe
    - Infernal pickaxe (uncharged)
  - Over 60 new **mining animations** have been created to reflect their individual HD style (fixes hdos/issues#1601)
    - Also upgraded special attack and Motherlode Mine animations to more modern equivalents
    - **Note:** *The trailblazer and third-age animations have been upgraded, but their models will not be affected in this update*
  - Added **particle effects** to the Infernal pickaxe
    - Only its special attack has more pronounced particles
  - Extended NPCs: **Mercy** in the Motherlode Mine no longer moves around
  - The mining pet has been remastered
- Refine `Dink` and `BA Minigame` plugin settings UI
### Fixed
- Fix lootbeam textures while in SD mode
- Fix `greenscreen` when in fixed mode with a opaque side panel
- The ranged arrow for the **level-up** icon is now removed correctly
- **Player-owned-houses** no longer have grass where they shouldn't with hide roofs disabled
- Fix world list refresh for the in-game world-hopper


## [408] 2023-9-1
### Changed
- Improve `hiscores` UI
- Change `timestamps` to use system-time vs engine-time
- Change `Util/Draw FPS` keybind from `control D` to `alt F` (to avoid accidentally conflicting with the WASD keys)
- Change `Util/Draw Debug` keybind from `alt D` to `alt V` (to match the above change)
### Fixed
- Update Loot tracker for DT2 bosses
- Fix sound effects for skeletal (maya) animations (ex: fixes usages in TOA and DT2)
- Fix false-positive logout notification in the `idle notification` plugin
- Fixed HD **Tzhaar** death animations (fixes hdos/issues#705)

## [407] 2023-8-30
### Fixed
- Expose hiscore plugin (:jebaited:)

## [406] 2023-8-30
### Added
- Add plugin: `core/timestamp`
- Add plugin: `core/hiscores`
- Add convenient licensing information for external plugins
### Changed
- Time tracking: Add more clear left-click options to the notification toggle button
- Improve camera zoom smoothing
### Fixed
- Improve `Time Tracking` while the user is logged out 
- A miss-map causing a part of the Tzhaar walls to become invisible has been fixed
- Fix rendering skeleton animations in widgets (login screen)

## [405] 2023-8-25
### Fixed
- Fix keyboard camera smoothing

## [404] 2023-8-25
### Added
- Add plugin: `core/time-tracking`
- Add plugin: `hub/time-tracking-reminder`
### Changed
- Overlays should no longer overlap with the side panel
  - Overlays should also push to the side when opening the side panel
- Improve camera smoothing
- Swap dropdown buttons in the graphics presets with normal buttons to make it more clear to users
### Fixed
- Fix the direction of the side panel arrow button
- Fix tooltips from piling up while dragging (for a single frame)
- A tree by the **Taverley wall** has been fixed (fixes hdos/issues#1774)
- Fix key bind info in the ground item plugin

## [403] 2023-8-24
### Added
- Camera plugin: add the ability to adjust the mouse and keyboard sensitivity
- Add plugin: `hub/plank-sack`
- Add plugin: `hub/gnome-restaurant`
- Add plugin: `hub/crab-stun-timers`
- Add plugin: `hub/cox-thieving`
- Add plugin: `hub/cox-timers`
- Add plugin: `hub/cox-vanguards`
### Changed
- Cleanup and unify side panel UIs to better match the native look-and-feel
- A set of barrels by the **Fight Arena** area has been restored
- The graphic for winemaking once again shows correctly, and has had its model remastered (fixes hdos/issues#1683)
- **Sammy Servil** has been restored (fixes hdos/issues#1689)
- Khazard Door Guards have been restored
- All versions of **Bob the cat** and **Neite** have been restored (fixes hdos/issues#1685)
  - '**Not bob**' has been remastered
- **MapFilter** has been improved for the **Karamja Volcano Dungeon**
- The **Leviathan**'s boulder attack shadows have been made more visible
  - *This is to compensate for The Leviathan's dark atmosphere*
- Volumes of Graardor, Skotizo, Kril and Kree'arra pets has been reduced significantly
  - *This currently applies to the bosses as well, for their regular walks*
- Notification: set default `Client notification focus` to `Request` (from Off) and `Client notification sound` to `Native` (from Off)
- The **True Blood Altar** area is now using blood water, same as the Arceuus Blood Altar
### Fixed
- Improve middle mouse camera smoothing to better to match OSRS sensitivities and behaviors
- Fix ground item examining
- Improve an issue where overlays hide behind scenery (when UI batching is disabled)
- Enable `team` + `friends-notes` plugins
- Fix prayer reordering in the `prayer` plugin
- The **Lumbridge castle** ladder no longer makes you teleport when walking past it (fixes hdos/issues#1760)
- An elite clue stash by the Fight Arena bar no longer looks like a bench (fixes hdos/issues#1690)
- Fixed the positioning of the **Crandor** 'hole' leading into the Karamja Volcano
- The stepping stones present in **The Rift** no longer cast a square shadow due to their clickbox
- Fixed several miss-maps in the **Shades of Mort'ton** catacombs (fixes hdos/issues#1294)
  - *Note: The caves themselves are to be remastered still*
- The **Raiments of the Eye** set is now using the correct inventory sprite angles on the recolored variants
- Updated cache and maps with the latest game update
- Fix `Green Screen` where the player would turn white
- `Idle notifier` should no longer notify you that your idle when it is started
- Fix a case where the client would switch back to HD graphics (from SD) when moving from fixed to resizable modes
### Removed
- Reverted a potential fix for the Fletching level-up icon
  - *We are aware it is currently missing*

## [402] 2023-8-16
### Fixed
- Deploy missing assets from 401

## [401] 2023-8-16
### Added
- Add plugin: `core/xpdrop`
- Add plugin: `core/runecrafting`
- Add plugin: `core/team`
- Add plugin: `core/friends-notes`
- Add plugin: `hub/ba-minigame`
- Add plugin: `hub/rogues-den`
- Add plugin: `hub/kitten-tracker`
### Changed
- Update RLS to `1.10.10.1`
### Fixed
- Fixed agility outlining in Canifis (and probably other object outlining things too)

## [400] 2023-8-16
### Fixed
- Update proto to 216

## [399] 2023-8-14
### Fixed
- Fix remember username

## [398] 2023-8-14
### Added
- Added a basic ::commands/::help command
### Changed
- Update RLS to 1.10.9.4
- SD Mode: Ground decorations no longer disable automatically, and emissive texture effects are now enabled
### Fixed
- Re-Enable the "Standard Detail" button in the login screen
- Fix login screen music
- Update cache
- **Leviathan**'s projectile color improvements are now active
- Fixed **Desert Treasure's II** desert temple regressions
- All variants of the **Ancient Sceptre**'s inventory sprite has been corrected
- Fix Tempoross and Tears of Guthix **plugin timers** (along with misc. other plugins effected by the bug) (fixes hdos/issues#1698)
- **Star sprites** no longer have a Talk-to option
- Added a missing Fletching sprite to **HD levelups**
  - *This was difficult to test, so let us know if it works or not!*
- **Barrows** head models are now upgraded whilst inside the crypt


## [397] 2023-8-7
### Fixed
- Plz Fix login black screen

## [396] 2023-8-7
### Fixed
- Fix login black screen

## [395] 2023-8-7
### Added
- *Standard Detail* has been added back!
  - You can enable this under the in-game "Display" settings, and selecting the "Standard Detail" preset.
  - This version still runs on the GPU, and is more similar to the look-and-feel of 2011 SD.
  - This is apart of the restore, as we used to have SD, and it always remained an option to users back then.
- This includes new Graphical Settings to align things to the SD style:
  - *Textures*: Enable/Disable (most) textures
  - *Ground blending*: Enable/Disables the ground blending
### Changed
**Hespori** now has an atmosphere
- The **Abyssal Whip**'s special attack graphic has been restored
### Fixed
- Fix graceful icons for POH wardrobes
- **Edgeville Monastery** is no longer downgraded
- **Hosidius' deposit box** is no longer a cave exit
- Watered plants during the **Tithe farm** minigame will no longer Z-Fight
- A chunk of Tolna's rift is no longer present during the **Nex** encounter instance
- **Gargoyle** flapping sounds are no longer crazy loud
### Audio (thanks to `@RS Orchestra#9880`)
- Country Jig (HD Remix)
- Added new/recycled instruments in the soundbank.
- Realistic clockwork added to bank 1, preset 2.
- Upgraded Desert Treasure II music to HD soundset
- Reverted Guardians of the Rift music to original versions per request

## [394] 2023-8-5
### Changed
- The boss atmosphere of **The Leviathan** has been altered, to improve the contrast between the melee and ranged projectile
- Remastered the **Kalphite Slayer Cave**
  - Also fixed the feature where high cave walls can be toggled with the 'hide roofs' toggle

## [393] 2023-8-3
### Fixed
- Fixed broken animations (caused by the DT2 player mirroring fix)
  - The Leviathan should no longer be stiff

## [392] 2023-8-3
### Fixed
- Fix ID shift in latest update (wrong gear showing)

## [391] 2023-8-3
### Added
- Added the option to show your character name in the title bar (under Interfaces)
### Changed
- Restored the **Suprise Exam** random event area
- **Desert treasure II**
  - Improved terrain and shorelines around the Stranglewood area
  - All areas should once again be upgraded
  - Upgraded the Leviathan boss area
  - The Leviathan now uses bloomed projectiles
- Tempoross' wave effect is now darker
  - *This was how it was intended to be, but since then we fixed the transition, which caused the area to be blindingly bright for a short period*
### Fixed
- Improve cutscenes where the camera plugin conflicts with the aiming.
- Fix DT2 boss showcase cutscenes
- Fix DT2 player mirroring
- Update cache
  - Torva should also be upgraded once again
- Improve Tile indicator rendering for rougher terrains
- Fix a quest helper related crash

## [390] 2023-8-2
### Fixed
- Fix login

## [389] 2023-8-2
### Changed
- Update RLS to `1.10.9.1`
- Improved **Slayer tower basement**'s atmosphere, and added more lights
### Fixed
- Nex's room will no longer change its objects after an update
  - Fixed the dirt in Nex's room
- DT2: Fix recoloring, overhead icons, and player mirroring
- **Crashed stars** should now animate properly

## [388] 2023-7-31
### Fixed
- Upgrades for Ghorrock (Desert Treasure II) are now active

## [387] 2023-7-30
### Changed
- Improved the terrain around Camzodaal's entrance (Ice mountain)
### Fixed
- Fixed the following related to **Desert Treasure II**:
  - Added base atmospheric effects for Ghorrock
  - Upgraded some terrain for Ghorrock
  - A certain altar object now uses particles outside the welcome screen
  - The desert temple  and the Ruins of Camzodaal have been given base atmospheric effects
  - Lassar Undercity and the Stranglewood atmospheres and terrain have been upgraded
- **Ice mountain** is no longer downgraded, and now respects the new shortcut that was added in the latest game update

## [386] 2023-7-26
### Fixed
- Fixed the following related to **Desert Treasure II**:
  - Brightened the atmos slightly in the new Digsite cave
  - The crevice in the new digsite cave is no longer filled with dirt
  - Re-restored the Digsite
  - Remastered a specific wall-kit that was missing a corner piece
  - Fixed Morytania regressions
  - The doors in the Stranglewood temple have been fixed and added some lights
  - Added basic atmospheric effects to The Scar
  - Fixed flipped chests in The Scar
  - Added basic lights to misc areas
  - The Axion puzzle can now be completed (fixed options)

## [385] 2023-7-26
### Fixed
- Re-enabled dark atmospheres for suspected caves. This will cause some areas to be dark once again when they shouldn't be - we rely on reports to fix them
- Update cache
  - Fixed chat-heads
  - A certain broken animation skeleton that may have caused Nardok some trouble has been fixed with the latest update

## [384] 2023-7-25
### Changed
- Upgraded the 'puff of smoke' graphic once again, now using a modern variant with particles
  - Another, larger, puff of smoke has had the same treatment for its larger equivalent
### Fixed
- Unique loot in ToA will no longer show the shadow and silhouette of all other uniques
- Fix custom models misbehaving (fixes hdos/issues#1727)

## [383] 2023-7-24
### Added
- ToA plugin
### Changed
- **Tombs of Amascut - Batch 1**: Encounter fixes and base overhaul: (fixes hdos/issues#1452 hdos/issues#1519 hdos/issues#1549 hdos/issues#1550 hdos/issues#1608 hdos/issues#1721 hdos/issues#1724)
  - Changed
    - Much of the terrain has been fixed and remastered, now using custom overlays and underlays
    - Atmospheric effects have been added
  - Fixed
    - Boulders during the Zebak fight should no longer occasionally disappear when they shouldn't
    - Projectiles and objects can now animate using the new skeletal animations
    - Certain animations that play for specific players now play as intended
    - The puzzle should now properly animate and display
    - Red skulls in the Apmeken puzzle now show up
    - Akkha's Shadows' visibility has been improved
    - The visibility of runic symbols has been improved
    - The visibility on the Warden's pyramid attack has been improved
    - The Warden's floor attack now animates properly
    - Increased performance in the Warden's final area
- **Dragon Slayer I**'s cutscene (sailing to Crandor) has been restored (fixes hdos/issues#1722)
- Restored the underground pass during the **Song of the Elves** quest
- Restored a scenery object of the **Dwarf Multicannon**
### Fixed
- Dungeon areas will no longer be dark by default (fixes dark cutscenes/instances outside)
  - *This may also affect areas that are supposed to be dark. Please make an issue on git if you find this is the case!*
- The crate cutscene during **Merlin's Crystal** is no longer absent (fixes hdos/issues#1705)


## [382] 2023-7-21
### Changed
- Restored **Ancient Warrior's Equipment** (Vesta/Zuriel/Statius/Morrigan) (fixes hdos/issues#1711)
- Remastered the ancient talisman (welcome screen)
- **Potion sprites** have adapted a more modern look, and now looks more clearly full at 4-doses
### Fixed
- Fix music player
- The Ice Demon encounter in the **Chambers of Xeric** raid may now be interactable when it wasn't before (fixes hdos/issues#1709)
  - *Please re-open the issue if this isn't the case!*
- **Kourend Castle** houses no longer have black inner walls (fixes hdos/issues#1720)
- The Jagex account banner once again warns about HDOS not being supported

## [381] 2023-7-20
### Changed
- Restored the **Dig site**
  - Restored the **Temple at Senntisten**
  - Remastered the **Fossil Island barge**
    - Textured 30 models
    - Improved terrain and heights
    - Improved shorelines at the canal area
  - Restored the **Paterdomus Beacon Network**
- Restored **Shooting Stars**
  - Star sprites have returned! (absent in OS)
### Fixed
- Potentially fixed a disappearing issue during the Kephri and Akkha encounters
- **ExtendedNPCs:** As part of the Dig Site restore, several extended NPCs should no longer move when they shouldn't
  - Panning point
  - Nick
  - Nisha
  - Barge foreman
  - Barge workman
  - Barge guard

## [380] 2023-7-19
### Fixed
- This is why deploying tired is a bad idea

## [379] 2023-7-19
### Fixed
- Fix crashing due to projectiles

## [378] 2023-7-19
### Fixed
- Fixed a crash when using roman bank tabs
- Fixed switching between window modes
### Changed
- Update RLS to `1.10.8.2` (from `1.10.7`)
- Updated the inventory sprite for **runes**

## [377] 2023-7-19
### Changed
- Updated the **Helm of Neitiznot** inventory sprite to a later revision
### Fixed
- Update **proto** to 215
- Update **cache**
- Some improvements have been made to the **Ruins of Ullek**:
  - several object miss-maps causing holes in the walls have been fixed
  - The blocked staircase has been dug out
  - A fire has been restored
  - The mining rocks west of Ullek have been restored
- **ExtendedNPCs**: Pyramid blocks on the agility pyramid no longer walk around
- **Miss-maps**: A golden sink or bird snare will no longer look like a Nex prison door (fixes hdos/issues#1493)

## [376] 2023-7-18
### Added
- Added the `::particles` command for those that desperately need to tone down the particle rate
  - This currently does not save as a setting and is an experimental feature
### Changed
- Remastered the bond model in the bond pouch
- Improved the **Blood Altar**
  - A new atmosphere has been added, and is now more accurate based on your vicinity to the altar
  - Added numerous bones to the blood water
  - Custom blood water has been added that looks more saturated
- **Tolna's rift** (Souls Bane quest) has been restored
  - Some bits have been remastered as an addition to the restore, notably the boss room
- Restored **player-owned-house** terrain and atmospheres (fixes hdos/issues#1052)
  - Also upgraded the terrain for the Halloween and Twisted themes
### Fixed
- The amulet space in **player-owned-houses** will no longer appear completely invisible (fixes hdos/issues#1714)
- **Nylocas Matemenos** (Red Crabs in ToB) should no longer go invisible when walking over a tile where a pillar used to be
- Minions during the **Abyssal Sire** encounter should no longer go invisible
- **Extended NPCs**: Abyssal Sire's tentacles should no longer walk around
- Fixed the zooming of the bond model in the bond pouch

## [375] 2023-7-16
### Fixed
- The Kings' ladder should no longer have an incorrect size, causing it to clip into the ground

## [374] 2023-7-16
### Changed
- The **Waterbirth dungeon** and **Dagannoth Kings** has been remastered
  - Upgraded the majority of the dungeon's features with existing HD assets, and restored where possible
  - Added immersive atmosphere
  - *Be sure to read our new [blog](https://hdos.dev/dev-blog/blog-dagannoth-kings) if you want some extra insight!*
- The animation of the **Rune Thrownaxe**'s special attack has been restored
  - In the process also patched a bug that would occur until it was fixed in 2012
- Several stationary NPCs have been restored; they should also no longer move when rendered as **Extended NPC**
  - Rock lobster
  - Rock crab
  - Dagannoth egg
  - Door support
- **MapFilter** has been improved for several of the Waterbirth dungeon areas
  - You will no longer get stuck in a certain part of the dungeon
- **Context cursors** will now display unique icons for "Peek", "Step-over", "Standard" and "Slayer" (enter boss room)
### Fixed
- Fixed a shift in the NPC IDs caused by Jagex
  - **Friendly Foresters** will no longer be GOTR Wizards
  - **Apprentice Cordelia** should now behave properly
- The **level-up icon** for combat is no longer pixelated
- Some other extended NPC's should no longer move:
  - Guardian mummy
  - Plough
- Fixed a bug where extended NPCs would not spawn their proper location with MapFilter enabled

## [373] 2023-7-12
### Added
- Added a warning to the **Jagex account** pop-up in the bank interface
### Changed
- Upgraded the beaver in the **login screen**

## [372] 2023-7-9
### Added
- Add plugin: `hub/PvP Performance Tracker` (just the overlay for now)
### Fixed
- Fix known crashes
### Changes
- RLS performance improvements


## [371] 2023-7-8
### Changed
- Update DLL Block List
### Fixed
- Fixed a long standing memory leak when at the login screen for a long time (causing a "OutOfMemory" crash)
- **God wars dungeon** instance areas should no longer have spiky heights
- Fix Activity advisor interface
- Quest Helper: fix hint map lines (fixes hdos/issues#1687)
- Quest Helper: add support for cheerers and the "Herb run" quest
- Quest Helper: refine side panel UI to fix text from getting cutoff

## [370] 2023-7-6
### Changed
- Update RLS to `1.10.7` (from version `1.10.6.1`)
  - Fixes bank clipping bug (fixes hdos/issues#1680 hdos/issues#1681)
- fix loot tracking for zulrah and kraken
- virtual levels: fix total level calculation

## [369] 2023-7-5
### Changed
- Remastered the depleted dense runestone in **Arceuus** (fixes hdos/issues#1678)
- Update cache
### Fixed
- The hunter level-up icon will no longer appear when it shouldn't
- The **crystal arrow** drawback on recoloured BofA's will no longer bug out
- Backspace will no longer clear your chat (but will still lock it when empty)

## [368] 2023-7-4
### Fixed
- Disable duplicate npc marker plugin
### Changed
- Restored **level-up icons** (fixes hdos/issues#751 hdos/issues#812)
  - Remastered the combat level-up icon
- Restored several **pets**, and remastered their chat-heads:
  - Prince Black Dragon
  - Chaos Elemental Jr
  - Kree Arra Jr
  - Zilyana Jr
  - Penance Pet
- **Key Remapping** now unlocks the chat on forward-slash, and locks the chat on backspace

## [367] 2023-7-2
### Fixed
- Fix quick settings regressions from v365

## [366] 2023-7-2
### Changed
- Update RLS to `1.10.6.1` (from version `1.10.5`)
  - This includes the new forestry update to woodcutting plugin
  - All core and hub plugins are updated as well (if they had any updates)
- #### **HD Pack 11**
  This version marks our eleventh **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the recent **Forestry** update

  The following changes are introduced:
  - Remastered all objects and items
  - Added a new **particle effect** to the anima-infused root that spawns during the roots event
  - Remastered the **Woodcutting Leprechaun**
  - Upgraded the **Friendly Forester**
  - **Animation tweening** has been improved for two new Forestry-related graphics
  - Restored the 'puff of smoke' **graphic** (Imp teleport, random event spawns etc.)
    - An animation tweening fix has also been applied for its animation
  - Fixed odd shadows cast by the struggling sapling
  - Rigging has been improved for the following assets, reducing clipping issues and allowing for more detailed animations:
    ```
    - Forestry outfit
    - Log basket
    - Forestry kit
    - Forestry basket
    ```
  - Restored the **Beaver** pet
    - Remastered the recolors
    - Upgraded the welcome screen beaver
- Restored the **Seers village** court guards
### Fixed
- Fixed known plugin crashes

## [365] 2023-7-1
### Added
- The RLS (RuneLite Subsystem) is now in public beta!
- Extended click distance
### Changed
- Restored the Circlet of water with the **Enchanted water tiara**
  - Added and remastered an uncharged variant
- Restored **Sergeant Damien**
### Fixed
- The **Strange plant** is no longer as strange (fixed hdos/issues#500 hdos/issues#718)
- Improved orthogonal projection (a rare type of projection used for interfaces)
  - Fixed the **Forestry**'s sapling interface
- A certain magic cast animation now animates the legs whilst moving
  - **Malignius Mortifier** no longer slides when casting his spell (fixes hdos/issues#1660)

## [364] 2023-6-28
### Added
- **Town criers** will now warn players that Jagex accounts are **not** supported for HDOS, as the process is irreversible
- Added support for multi headbars (fixes health bar for sapling)
### Removed
- The secret **pride** quest has been disabled, till next year!
### Changed
- The following changes have been made to the **Desert**:
  - Remastered **Jarr** in Shantay pass
  - The deposit box in Shantay pass has been remastered (added a desert variety)
  - Improved tile heights and smoothened jagged hills
  - Restored **cacti** and **rockslides** by the Shantay pass area
  - Remastered cacti and rockslides in the north-east of the Kharidian desert
  - Restored the **Kalphite caves**
  - Restored the **desert** area south-west of the Shantay pass (Kalphite cave entrance)
    - Also upgraded the objects here
    - The fairy ring here now uses a new variety of butterfly **particles**, sand! (fixes hdos/issues#1527)
- The following changes have been made to **Feldip hills**:
  - Remastered the **Red chinchompa hunting grounds**
  - Restored the **Feldip eagle network**
  - Restored the **Feldip jungle** area surrounding the eagle cave's entrance
  - Upgraded the **Hunting expert** (red chinchompa hunting ground)
- Remastered the **Cabbage cape**
- The particle emitter colors for **Skotizo**'s dark bolt have been changed from purple-black to black, as **Cerberus** also uses it
- Remastered the central kourend statue blocks prior to the quest's completion (fixes hdos/issues#1651)
- **Bob** in Varrock has been upgraded (fixes hdos/issues#1637)
- Restored **Leo**
- Restored the **Quiz Master**
- Restored all versions of the **Mysterious old man**
- Restored **Postie Pete**
- The **Beekeeper** can now walk, and has been restored once again
- The **Bow of Faerdhinen**'s crystal arrow graphic has had its tween exclusion removed, and now performs a true HD animation
  - *Prior to the tween exclusion, the drawback had been misbehaving*
- The **Amethyst arrow** has been remastered (fixes hdos/issues#1621)
- Restored the **Teletab creation** animation
- Restored **Nickolaus**
- The following changes have been made related to **Forestry**:
  - Added a light to the fires by the forestry shops
  - Updated maps
- The locked version of **Void knight equipment** has been restored
### Fixed
- **Korasi's sword** should no longer display the Voidwaker lightning effect as well when used in PvM situations
- The floor of the **Wintertodt** boss area no longer vanishes when floor decorations are hidden
- Fixed the way the 'hide roofs' toggle behaves in the **God Wars Dungeon**
  - Hiding roofs now hides the floors above you and lowers walls around you
  - *Increased render still shows some objects in the background on a certain floor, these cannot be changed*
- Fixed some minimap tiles in the **Arceuus** and **Kourend castle** areas
- **Knights of Saradomin** in the God Wars Dungeon now animate correctly when standing still
- **Marks of Grace** no longer spawn under the floorboards on the Varrock rooftop (fixes hdos/issues#1655)
- **Camels** now animate properly (fixes hdos/issues#1648)
- **Plague cows** should no longer slide around (fixes hdos/issues#1645)
  - *This one was quite hard to verify, so please let us know if this has not been fixed!*
- A property that makes objects act as tables is no longer inherited from the HD counterpart
  - **God wars dungeon** pillars no longer make items float in the air when dropped
- The **young vine** that is used to grow the tall vine to access the Feldip eagle network is now visible (fixes hdos/issues#437)
- **Captain Dalbur** should no longer be unrestored under certain conditions
- Updated cache
- Potion making no longer makes you slide (fixes hdos/issues#1659)
- Another variant of Cormorants (seabirds) will no longer slide across the ground when moving, different from the fix in v316 (fixes hdos/issues#1658)
- Fixed a missmap in the Kalphite cave that may have caused trouble somewhere else

## [363] 2023-6-23
### Fixed
- Fix wintertod crashes

## [362] 2023-6-23
### Changed
- #### **HD Pack 10**
  This version marks our tenth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This milestone is celebrated with a larger amount of changes, and targets the Zeah: **Arceuus**, **Kourend**, **Catacombs of Kourend**, and **Wintertodt** areas.

  The following changes are introduced:
  - Added immersive **atmospheres** to all areas
    - An effect has also been added by the dark altar in the Arceuus and Catacombs areas, that cause crystal light effects to pulsate outwards
  - Remastered numerous **objects and NPCs**
    - Created new assets of the Kourend castle to fix issues regarding the shadows on the higher floors
    - Fires in Wintertodt and Kourend have been upgraded
    - Added several new chat-heads
  - **Terrain** has been remastered
    - Fixed oldschool's ground heights in Arceuus that would cause several buildings to sink
    - Snow now looks more realistic
    - Added more detailed terrain to the Blood altar, Arceuus mine and Soul altar areas
    - Added more detail to the Catacombs and Skotizo areas
    - Brushed (jagged) ground heights in several places
  - Several **new particle effects** have been added
    - Tome of Fire's embers and smoke
    - Bruma torch's flame
    - Wintertodt boss (active and inactive)
    - Wintertodt's snowstorm (damage mechanic)
    - Choke devil's smoke
    - Green, blue, purple and white Phoenix pet flames
    - Shadow veil
    - Resurrect crops' water
  - It now **snows** in the snowy areas of (Wintertodt)
  - Added **shorelines** to the northern shores of Wintertodt
  - Remastered all 88 **Arceuus spellbook** sprites
    - *Enlarged icons for both the Arceuus and Lunar spellbooks is on the to-do list!*
  - Remastered the Kourend favour house icons
  - Improved item alignment on **Kharedst's memoirs**
  - Restored the **Darklight** and **Silverlight**
  - Restored **Death spawns** (Nechryael minion)
  - Improved rigging and alignment issues with the **pyromancer outfit** for females
  - Restored a variety of monsters in the Catacombs that weren't already
    - Also upgraded brutal dragons globally
  - Other notable remasters:
    ```
    - Arclight
    - Pyromancer outfit
    - Kharedst's memoirs / Arceuus teleports
    - Amulet of torture
    - Phoenix pet recolours
    - Twisted banshee, Screaming banshee, Twisted screaming banshee
    - Deviant spectre, Abhorrent spectre, Repugnant spectre,
    - Greater nechryaels, Chaotic death spawns, Nechryarch
    - Insatiable bloodveld, Insatiable mutated bloodveld
    - (Catacombs) lesser demons
    - Warped jelly, Vitreous jelly, Virtreous warped jelly
    - Choke devil
    - All reanimated monsters
    ```
- Remastered the **quest interface** sprite
- Remastered the **ironman head** in the welcome screen
- The **jagex accounts** banner in the welcome screen now warns it is unsupported
  - Also upgraded the border to an HD sprite
### Fixed
- Animation tweening: The firemaking animation has been improved
- The **Orb of Oculus** now only moves whilst the chat is locked, if the option for it is enabled
- A concurrency issue where the wrong **particles** would be emitted has been fixed (fixes hdos/issues#1540)
- Fixed a bug where lighting on certain camera angles would make walls pitch black (fixes hdos/issues#1567)
- Fixed an issue where particles could cause the client to crash when rendered inside a window
- Safeguarded against a rare potential crash that could occur when loading NPCs

## [361] 2023-6-15
### Fixed
- Added welcome screen assets

## [360] 2023-6-15
### Changed
- Remastered the **magic shortbow (i)**
- Remastered the welcome/lobby screen
  - *Note that this is not the finalized version, but it may be a while before the original is restored*
- The forced **view-distance** for Runecrafting altars has been tweaked (fixes hdos/issues#1595):
  - Upped the minimum view-distance cap equal to the vanilla client
  - The cosmic and chaos altars are now uncapped
  - *The view-distance for the colossal wave during the Tempoross encounter is intended*
  - *We strongly believe the artists meant to make the areas more immersive with strong fog. However, we recognize the forced view-distance was a little too aggressive*
- Restored the **White Wolf Mountain passage**
  - Also fixed an issue with MapFilter in that area
- Several chat-heads and/or NPCs have been upgraded (fixes hdos/issues#1631):
  - Restored the **security guard** chat-head (fixes hdos/issues#1625)
  - Restored **Skippy** (fixes hdos/issues#1626)
  - Remastered **Count Check** (fixes hdos/issues#1627)
  - Remastered **Two-pints** (fixes hdos/issues#1628)
  - Restored **Rohak** (fixes hdos/issues#1629)
  - Restored **Harrallak Menarous**, and **Lorelai** has been given a chathead (fixes hdos/issues#1630)
  - Upgraded **James**'s chat-head
  - Upgraded **Clerk**'s chat-head
  - The size of **lobby chat-heads** has been tweaked slightly
  - Upgraded **Saradomin-** and **Zamorak recruiter** chat-heads
  - Upgraded **Meredith**'s chat-head
  - Restored the **Master smithing tutor** in Varrock
  - Restored **Aubury**
  - Upgraded **Jeff**'s and **Sarah**'s chat-heads
  - Upgraded **Thessalia** and **Iffie**
  - Upgraded **Jess**'s chat-head
  - Remastered **Probita**
### Fixed
- The **login screen background** now randomises once again
  - *Before it would always start with the god wars dungeon area*
- Fixed a bug that was present until 2011, where **Cyreg Paddlehorn** would become stiff when standing still (fixes hdos/issues#1624)
- Fixed a bug where ground items would vanish when changing your view distance (fixes hdos/issues#1632)
- **Construction contract** objects in Ardougne should now become transparent during a contract (fixes hdos/issues#1615)
- **Varrock's Hairdresser** is now remastered once again
  - *Oldschool has re-added it with the latest game-update*
  - Also fixed any other potential Varrock restore regressions

## [359] 2023-6-9
### Changed
- Login screen chat-heads can now be restored
  - Upgraded login screen chat-head with 'Love'
- **Lumbridge**'s wooden castle wall pillars have been re-added
### Fixed
- Mining rocks in the **Shilo mine** no longer downgrade when they deplete
- Several plant parts in **Shilo village** no longer disappear when roofs are hidden (fixes hdos/issues#1597)
- Some rocks near **Yanille** are now upgraded
- Fixed **Varrock** regressions due to oldschool's hairdresser bug
- Fixed some typos in the **secret quest**

## [358] 2023-6-8
### Added
- A **secret quest** has been added
### Fixed
- The **Venator bow** should no longer be floating under certain conditions
- Fixed **missmaps** with NPCs that were removed from the cache
  - The tool leprechaun by Draynor manor is no longer a goblin

## [357] 2023-6-7
### Changed
- #### **HD Pack 9.5**
  This special version is an addition to our ninth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This additional HD pack version has a set of smaller improvements, and targets the **Oldschool Pride event**.

  The following changes are introduced:
  - Upgraded all NPCs and chat-heads
  - Remastered the objects around the Blue Moon Inn
  - Remastered the following:
    ```
    - Love crossbow/arrow/hearts
    - Flower crown (all variants) and its chathead
    - Poetry jacket
    - Colourful jumper (all variants)
    - Rainbow scarf (all variants)
    ```
- **Phoenix** particle count has been decreased slightly
- An animation tweening fix will now also apply for oldschool variants of crossbow animations (such as the love crossbow)
### Fixed
- The **chat-head for cats** has been restored
- Updated cache (fix chat-head regressions)

## [356] 2023-6-3
### Fixed
- Fix NPC chat heads
- Fix crashing due to the new chat effects

## [355] 2023-5-31
- Deploy unmerged assets (fixes giant nezzy helm in bounty hunter)

## [354] 2023-5-24
### Fixed
- Update proto to 214
- ***Grace*** has been fixed and now wears the agile armour
- Somewhat improved the fire animation glitches of fires once again
- Improved the terrain for **The Nightmare** encounter area

## [353] 2023-5-24
### Changed
- Altered the Gargoyle clickbox
### Fixed
- MapFilter has been improved for the ancient cavern (Dragon Slayer II area)
- The **Ferox Enclave** map has been made compatible with the Bounty Hunter update, and is remastered once again (fixes hdos/issues#1613)
  - Also added some new lights to compensate for the torches removed
  - Remastered the suits of armour
  - Restored the yellow carpet
  - Remastered the red-black carpet
- Fix the bounty hunter rewards interface (fixes hdos/issues#1612)

## [352] 2023-5-22
### Changed
- Restored the throwing weapon animation for **knives** and **thrownaxes**
- Restored the **dart** throwing animation
- Made improvements to **environment**
  - Fixed a bug where atmospheres would make the ambience go dark during a transition
  - **LMS** fog has been re-enabled and remastered. It now uses an immersive atmosphere instead of an interface
- Remastered the **Zaryte crossbow** inventory/ground model
- The texture projection for the **Dragon crossbow** has been improved slightly
  - Also remastered the female model that was added later on
- Remastered the clan banner on the **Grand Exchange**
- Restored **Oldak**
- Restored Goblin, Cave Goblin and Demon Butler **chatheads** (fixes hdos/issues#147)
- Remastered the **Ardougne Cloak's colors** using the HD mesh as a base (fixes hdos/issues#1599)
### Fixed
- **Hazelmere**'s chat-head has been fixed
- Fixed a bug where some sound effects from other players would play from afar
  - The regular sound effect should now also be muted for other players when using the Ardougne Cloak
- Fixed **Lumbridge discrepancies**:
  - The bar's floor has been changed to the one used in houses from the old HD maps
  - Some walls on the upstairs of the bar have been improved
  - On some tiles the furnace roof no longer hides when it shouldn't
  - Upgraded the clock and corrected some objects in Seth's house
  - Created some custom assets to improve walls around Fred's house
  - Upgraded chicken coop
  - Fix some inconsistent rocks
  - Some endings on fences have been improved (even if they weren't in HD)
- Fixed **Varrock discrepancies**: (fixes hdos/issues#1604)
  - Removed an old stone wall by the tea stall
  - Removed an old railing in the museum
  - Fixed some misplaced pillars around the GE wall
- The **pestle and mortar** animation no longer makes you slide while moving (fixes hdos/issues#1605)
- Fixed a tile missmap in the **Maiden of Sugadinti** boss fight area
- Made some improvements to the **Guardians of the Rift** terrain
  - Fixed an issue that regressed fixed tileheights and gaps in the mountain (this is normally present in oldschool)
  - Fixed an overlay missmap that displayed abyssal flesh as red carpet
- Fixed the missmapped terrain by the Tombs of Amascut raid entrance

## [351] 2023-5-11
### Fixed
- The **Easter bunny** has been banished
- The **Lletya statue** should now be remastered (fixes hdos/issues#1589)
- A bench in front of the **Blue Moon Inn** has been removed due to collision
- **Tyras guard** chat-heads (and potentially other things) should now be working correctly (fixes hdos/issues#1590)
- Fixed a bug where some (comp) **max capes** would not always apply their HD definitions, fixing issues such as floating (fixes hdos/issues#1588)

## [350] 2023-5-10
### Changed
- Restored **Isafdar**, **Lletya**, **Port Tyras** and the **Poison Waste** (fixes hdos/issues#1562)
  - Remastered the telescope
  - Restored **badgers**
  - Moved some gasses erupting from the poison waste, so they don't collide with stepping stones
  - Restored **Broken Handz**
  - Restored Iowerth Elves
  - Restored the Poison Waste **Canyon** (currently inaccessible)
  - Remastered the northern Isafdar trees
  - Restored 16 Lletya elves
  - Improved and restored Zul-Andra npcs
    - The fisherman remains oldschool for now (it is actually an object!)
- Restored the **Prison Pete** random event
  - The attack context cursor now also shows on 'pop'
  - Remastered the atmosphere
  - Improved MapFilter
- The player will now perform an animation whilst invigorating the **imbued heart**
- The lightning animation for the imbued heart and Saradomin's sword has been improved
### Fixed
- Fix the settings UI
- Fix potential client crash when entering a fairy ring
- Children-, gnome-, dwarf-, ogre- and Tzhaar **chat-heads** should now animate properly and are no longer small (fixes hdos/issues#147 hdos/issues#1359)
  - *This assumes the NPC is restored. The facial expressions cannot be restored easily, so it is something we will have to come back to at a later stage*
- Some issues with the upstairs floor of the **Hairdresser house** in Varrock have been fixed
- Updated cache (fixes hdos/issues#1587)
- Fixed an issue where the wrong balloon animals would show

## [349] 2023-5-7
### Fixed
- Fix client cache problems

## [348] 2023-5-7
### Changed
- The **Varrock hairdresser** has been remastered
- **Draynor village**'s darkness has been lifted
- Restored the **Lighthouse** during the quest
### Fixed
- Some improvements have been made to **Guardians of the rift** and the Temple of the Eye quest:
  - Some terrain no longer shows up as planks instead of flesh
  - Some untextured terrain has been upgraded
  - The Stone of Jas cutscene has been remastered
  - The Guardians of the Rift tutorial/cutscenes should now be remastered
- Upgraded the atmosphere and fixed some minor terrain issues for the **Theatre of Blood**
- Upgraded the atmosphere for the **Hallowed Sepulchre**
- The Varrock hairdresser map issues have been resolved

## [347] 2023-5-6
### Changed
- The Easter bunny has packed up their things, and the spirit of easter has faded in several areas
- Restored the **eating** animation
- #### **HD Pack 9**
  This version marks our ninth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack primarily targets the **Secrets of the North** and **Making friends with My Arm** quests

  The following changes are introduced:
  - The **Phantom Muspah boss** has been remastered
    - Several particle effects have also been added
    - Related bugs have also been fixed
  - Related areas have been remastered (including the Cave Horror dungeon)
    - **Weiss**, **Weiss Mine**, **Weiss quest caves**
    - **Ghorrock dungeon**
  - Several **interfaces** have been upgraded
  - Immersive **atmospheres** have been added
    - The 'fake' darkness effect during the Phantom Muspah encounter now utilizes real atmosphere, rather than an interface
  - Atmospheric **snow particles** with different intensities have been added to Weiss, the Ice Path, Trollweiss, and the God Wars entrance
    - The snow on **White wolf mountain** has also been fitted with the newer particle emitters
  - All **salt fire pits** now have lights and switch on or off depending on if you have built the pit or not
  - The animation of drawback arrows on **bows** has been improved
  - The **Venator bow** variants have been remastered based on the original Zaryte bow
  - The lightning effects of **Imbued hearts** have been remastered
  - Trollheim and Trollweiss have been restored

  - Other notable remasters:
    ```
    - All Phantom Muspah variants, and the Muphin pets
    - Salt teleports
    - Ancient sceptre
    - Charged ice
    - Ice chest
    - Ancient essence
    - Venator shard
    - Several quest items
    - Phantom Muspah's Scoreboard
    - Blue torches
    ```
### Fixed
- Some animation regressions should be fixed (fixes hdos/issues#1580)
- **Soulsplit**, **Wrath** and **Leech** effects during the **Nex** And **Muspah** encounters should now be working once again
- NPCs no longer lag behind when force-teleporting to another tile, resolving problems during several encounters. These include: **Panthom Muspah**, **The Nightmare**, **The Inferno** (Jal-ImKot), **Fishing Spots**, **Abyssal Demons** (fixes hdos/issues#902 hdos/issues#98 hdos/issues#674)
- The **Tome of water** and **Fairy ring** particles are now slightly smaller (as they were intended)
- Fixed a bug where interfaces would still be visible a tiny amount when set to invisible
- The **Warrior's Guild** and **Mac's island** are restored once again

## [346] 2023-5-1
### Changed
- Largely restored the **Underground pass** (fixes hdos/issues#737 hdos/issues#963 hdos/issues#1360 hdos/issues#1565)
  - *One section in particular where the disciples of Iban can be found, was not updated until 2014*
- Restored the **Grand Tree Tunnels** (fixes hdos/issues#1501)
- Restored the **Gnome Hangar**
- Remastered the hairdresser's **Changing room**
- Remastered the **infernal cape** (fixes hdos/issues#1558)
  - Also fixed several issues with it such as the rigging, stitching and alignments
- Remastered the **Neitiznot faceguard** (thanks to `Woahscam#0001` for upgrading the base mesh)
- Remastered the **Ghrazi rapier**, now using an increased polycount model
- Restored the **Crystal bow**
- **Thralls** have been upgraded
  - Restored the 3 monsters with 9 different memorable HD ones depending on their tier
  - Remastered the GFX to have bloom
  - Projectiles have been upgraded and extra ones will now spawn for their casting animation, giving a more detailed presentation
  - A series of more projectiles and animations unlock at the tier 3 (greater) variants, randomizing their attack visuals
- Two changes have been made to the **Voidwaker** (Korasi's sword)
  - Now plays the Korasi's sword sound effects upon performing its special attack
  - The special attack lightning is now positioned slightly lower
- Upgraded the **enchanting** animation
### Fixed
- **Unsafe mode** will no longer cause a Nullpointer crash under certain conditions
- **Chathead** regressions have been fixed
- Fixed several **ironman-related interfaces** (fixes hdos/issues#1392 hdos/issues#1453)
- The cupboard in **Radimus Erkle's office** is no longer absent, allowing you to progress the quest once again (fixes hdos/issues#1295)
- The spinning pole outside the **Falador barber shop** has had its tweening improved slightly (fixes hdos/issues#1563)
  - *We recognize it is still not perfect, but since the animation only has 4 frames there is only so much you can work with*
- The doors of the **Falador partyroom** have been fixed
- The staircases of the Falador partyroom now have black squares below them to indicate they're going down
  - *With the 15th anniversary it was decided to keep the museum underneath. Since a staircase going underground does not exist, this is a temporary solution for now*
- The **slash attack animation** no longer snaps back
- You once again perform the correct animation whilst casting with a **trident** (fixes hdos/issues#1574)
- **Tempoross' colossal wave** now changes the atmosphere even when the Skybox plugin is trying to override the area (fixes hdos/issues#1571 hdos/issues#1573)
- The **Imbued slayer helmet**'s chathead model for females is no longer using the male variant
- The **Avernic defender** inventory icon has been changed slightly, so it fits inside the slot

## [345] 2023-4-18
### Fixed
- Custom NPCs are working once again
  - This also fixes several issues related to NPCs and clickboxes

## [344] 2023-4-18
### Fixed
- Thralls no longer crash the client

## [343] 2023-4-18
### Changed
- Update proto to 213
- **Context cursors** will now show upon hovering over the **Exchange**, **Collect** and **Deposit** options as well
- **Zombies** have been restored. (tip: `::mog <id>` command to check them out!)
  <details>
  <summary>Click here for the full list</summary>
  
  - ID: 26, Name: Zombie
  - ID: 27, Name: Zombie
  - ID: 28, Name: Zombie
  - ID: 29, Name: Zombie
  - ID: 30, Name: Zombie
  - ID: 31, Name: Zombie
  - ID: 32, Name: Zombie
  - ID: 33, Name: Zombie
  - ID: 34, Name: Zombie
  - ID: 35, Name: Zombie
  - ID: 36, Name: Zombie
  - ID: 37, Name: Zombie
  - ID: 38, Name: Zombie
  - ID: 39, Name: Zombie
  - ID: 40, Name: Zombie
  - ID: 41, Name: Zombie
  - ID: 42, Name: Zombie
  - ID: 43, Name: Zombie
  - ID: 44, Name: Zombie
  - ID: 45, Name: Zombie
  - ID: 46, Name: Zombie
  - ID: 47, Name: Zombie
  - ID: 48, Name: Zombie
  - ID: 49, Name: Zombie
  - ID: 50, Name: Zombie
  - ID: 51, Name: Zombie
  - ID: 52, Name: Zombie
  - ID: 53, Name: Zombie
  - ID: 54, Name: Zombie
  - ID: 55, Name: Zombie
  - ID: 56, Name: Zombie
  - ID: 57, Name: Zombie
  - ID: 58, Name: Zombie
  - ID: 59, Name: Zombie
  - ID: 60, Name: Zombie
  - ID: 61, Name: Zombie
  - ID: 62, Name: Zombie
  - ID: 63, Name: Zombie
  - ID: 64, Name: Zombie
  - ID: 65, Name: Zombie
  - ID: 66, Name: Zombie
  - ID: 67, Name: Zombie
  - ID: 68, Name: Zombie
  - ID: 69, Name: Summoned Zombie
  - ID: 607, Name: Zombie protester
  - ID: 608, Name: Zombie protester
  - ID: 609, Name: Zombie protester
  - ID: 610, Name: Zombie protester
  - ID: 611, Name: Zombie protester
  - ID: 612, Name: Zombie protester
  - ID: 1784, Name: Zombie
  - ID: 2145, Name: Undead Druid
  - ID: 2501, Name: Zombie
  - ID: 2502, Name: Zombie
  - ID: 2503, Name: Zombie
  - ID: 2504, Name: Zombie
  - ID: 2505, Name: Zombie
  - ID: 2506, Name: Zombie
  - ID: 2507, Name: Zombie
  - ID: 2508, Name: Zombie
  - ID: 2509, Name: Zombie
  - ID: 3359, Name: Zombies Champion
  - ID: 3980, Name: Zombie
  - ID: 3981, Name: Zombie
  - ID: 5346, Name: Undead one
  - ID: 5347, Name: Undead one
  - ID: 5348, Name: Undead one
  - ID: 5349, Name: Undead one
  - ID: 5350, Name: Undead one
  - ID: 5351, Name: Undead one
  - ID: 5353, Name: Nazastarool
  - ID: 5647, Name: Zombie
  - ID: 6337, Name: Nazastarool (hard)
  - ID: 6398, Name: Nazastarool
  - ID: 6449, Name: Zombie
  - ID: 6450, Name: Zombie
  - ID: 6451, Name: Zombie
  - ID: 6452, Name: Zombie
  - ID: 6453, Name: Zombie
  - ID: 6454, Name: Zombie
  - ID: 6455, Name: Zombie
  - ID: 6456, Name: Zombie
  - ID: 6457, Name: Zombie
  - ID: 6458, Name: Zombie
  - ID: 6459, Name: Zombie
  - ID: 6460, Name: Zombie
  - ID: 6461, Name: Zombie
  - ID: 6462, Name: Zombie
  - ID: 6463, Name: Zombie
  - ID: 6464, Name: Zombie
  - ID: 6465, Name: Zombie
  - ID: 6466, Name: Zombie
  - ID: 6596, Name: Zombie
  - ID: 6597, Name: Zombie
  - ID: 6598, Name: Zombie
  - ID: 6741, Name: Zombie
  - ID: 7485, Name: Zombie
  - ID: 7486, Name: Zombie
  - ID: 7487, Name: Zombie
  - ID: 7488, Name: Zombie
  - ID: 8067, Name: Zombie
  - ID: 8068, Name: Zombie
  - ID: 8069, Name: Zombie
  - ID: 8139, Name: Skeleton
  - ID: 8140, Name: Skeleton
  - ID: 8514, Name: Trapped Soul
  - ID: 8528, Name: Trapped Soul
  - ID: 8529, Name: Trapped Soul (hard)
  - ID: 10591, Name: Undead Zealot
  - ID: 10592, Name: Undead Zealot
  - ID: 10884, Name: null
  - ID: 10885, Name: null
  - ID: 10886, Name: null
</details>

### Fixed
- Reverted regular prayer sprites
- Ice Sceptre in beta worlds no longer shows as Korasi's sword
- Fixed prayer icon tab regression
- Several treasure trail items no longer show cursed faces
  - Also fixed a slight oversight that would cause this bug to reappear periodically

## [342] 2023-4-9
### Changed
- **Context cursors** will now show upon hovering over the 'trade' and 'shear' options
- A variety of deadman (and group storage) chests have been remastered
  - The chest in the Lumbridge castle bank has been replaced with a more suitable variant
- Added more easter puns
- Improved something EGGcellent
- Restored the **teleport scroll** animation
- Restored **Rocktails** (Anglerfish)
### Fixed
- **Easter implings** no longer have a green box on them when observing them from underneath using the camera plugin (fixes hdos/issues#1557)
- Fixed a chat visibility issue regarding the easter event when chat is opaque
- The **Staff of Light** block animation now allows movement and no longer makes you twitch and slide
- The **Brown apron** now correctly recolors, and no longer appears as white instead (fixes hdos/issues#602)
- The **Ardougne sewers** are no longer downgraded
- **Bats** have had their flapping sounds lowered

## [341] 2023-4-7
### Changed
- The **Neitiznot Faceguard** has been replaced with the Helm of Neitiznot (e) to fix issues regarding the player's neck
  - *When the faceguard is remodeled to have proper rigging, we will most likely use it again*
- Restored the **Staff of Light** including its animations and effects
- Additional **easter** features have been added:
  - Temporarily restored the **Chocatrice cape** as an override for the cabbage cape (emote also works!)
  - Parts of **Lumbridge** and **Varrock** have been overhauled for Easter!
  - More 'easter eggs'
- Restored the barehanded (impling) catch animation
- Restored the **Ardougne cloak teleports**
  - Monastery teleport
  - Ardougne farm teleport
- The seals that were present in Lumbridge have been moved to the iceberg. Until next Christmas! (fixes hdos/issues#1498)
- A sword barrel in Lumbridge has been upgraded

## [340] 2023-4-5
### Changed
- #### **HD Pack 8.5**
  This special version is an addition to our eighth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This additional HD pack version has a set of smaller improvements, and targets the **Oldschool Easter event**.

  The following changes are introduced:
  - Remastered all items and objects related to this years Easter Event
  - Map issues related to missing objects and ground heights in Varrock have been fixed
  - Improved the terrain around the easter event area
  - Some problematic polygons on the Bird nest hats have been removed
  - Login screen icons have been remastered
  - Restored the 2009 Easter Bunny
  - Added an 'easter egg' or two
  - Fake NPCs can now be examined
  - Several chat-heads have been fixed
  - **Note:** *The new downgraded children were not able to be upgraded due to oldschool naughtiness*
  
- The **Mourner Tunnels** have been restored (fixes hdos/issues#504 hdos/issues#505)
  - The mines and dark beasts restored
  - Mourner headquarters restored
  - Temple of Light restored
  - Temple of Light (Song of the Elves) remastered
    - A lightcontroller has also been added for Seren's light
  - The boss fight during the Song of the Elves boss fight should now have a dark skybox
  - Remastered missing cave kits
- The animation for **tridents** has been improved
  - No longer twitches
  - Now only performs when you're wearing a trident and casting the built-in spell
  - The animation has been tweaked so that is allows you to move away from dangerous mechanics whilst performing the animation (no longer stalls) (fixes hdos/issues#1537)
- The **barrage** spell cast animation has been further improved
- A new variant of the **Korasi's sword** has been upgraded
### Fixed
- Your character will no longer occasionally try to path around a tile at the **Grand Exchange** entrance
- The construction **contract furniture** in the house south in Varrock will now be removed properly whilst on a contract


## [339] 2023-4-2
### Changed
- Thok's memory has subsided (**april fools** has passed)
### Fixed
- **Middle mouse camera rotation** now scales properly when in fixed mode
- The **crystal helm** (male) now respects skin color
- Another chest (rotten variant) has had its texture changed to match the open counterpart

## [338] 2023-3-31
### Changed
- Crystal armour has been remastered (including all of its other variants and chatheads)
- **Phoenixes** now use particles
- Remastered the **Zaryte crossbow**
- Remastered the **Ancient Godsword**
- Remastered **Primordial-**, **Eternal-** and **Pegasian boots**
- Several **animations** have received improvements:
  - *These changes are meant to fix animation smoothing issues and better reflect the intended style*
  - Judgement (Armadyl godsword): Heavier swing
  - Barrage and Blitz spells: Increased power
  - Smelting (furnace): Fixed arm twitch
  - Fire (firemaking): Should no longer flicker as much
  - Portal Nexus: Fixed twitch; once again tweens
  - Blood Sacrifice (Ancient godsword): Gfx no longer twitches
- Improved ground heights for Lumbridge basement (fixes hdos/issues#1533)
### Fixed
- **Fire-** and **Infernal max capes** now show their texture in inventory icons
- The **trailerblazer teleport** should now play the correct sound effects (fixes hdos/issues#1072)
- A patch in the water by the Waterbirth island dungeon entrance has been fixed (fixes hdos/issues#1532)

## [337] 2023-3-25
### Fixed
- Some improvements have been made to **Tempoross**
  - **Context cursors** now work for dousing fires and filling crates with fish
  - The atmosphere when the **colossal wave** hits has been made less dark
  - **Fishing spots** now also have high render priority, making them interactable whilst underneath a lightning cloud
  - The water splashing of a double harpoon fishing spot should now be more visible

## [336] 2023-3-24
### Changed
- The **crossbow** animation fix should now work correctly

## [335] 2023-3-24
### Changed
- **Zul-andra**'s terrain has been remastered and shorelines have been added
  - *Since HD Pack 1 was one of our first attempts at remastering an area, a lot has improved since then.
    The terrain (and atmosphere, part of the fog update) should now reflect a true remaster. Note that the adjacent zones may have buggy underwater. This will be fixed in time!*
- The **crossbow animation** has been improved
  - Modified to allow movement, and remove its stall when performing the animation and trying to move (fixes hdos/issues#1515)
  - Animation smoothing has been altered for the animation, re-introducing the iconic 'snap-back' or recoil when the crossbow fires, whilst retaining the smoothness
  - *The Hunters' crossbow retains its stall in PvP scenario's, so that its behaviour matches oldschools'*
- Remastered some missing assets during the **Tempoross** encounter:
  - Debris that spawns in the ocean after a colossal wave
  - The damaged variant of the mast on the boat
- Barbarian deposit boxes should now look like normal deposit boxes where they are not intended
  - The deposit box in **Port Sarim** has been remastered
  - The bank chest and deposit box in the **Barbarian Outpost** have been fixed (fixes hdos/issues#590 hdos/issues#591)
- The **Lumbridge basement** rocks have been cleaned up to look more like the HD version
- A certain variant of a wooden chest (one of which is used in Ardougne) has had its textures altered to match the open version
  - *This was presumably an HD oversight since the textures are similar, but it has been corrected to what it was intended to be*
- **White wolf mountain** now uses snow particles
- **Waterbirth island** has been restored
  - Now also has its snow interface removed, and the snow has been replaced with more uniform mild snow
- **Captain Bleemadge** has been restored
- All variants of **Captain Barnaby** are now fully restored
- **Fishing Trawler** has been improved:
  - Altered the Atmosphere to reflect Fog changes
  - Now uses a skybox
  - MapFilter now works during the encounter
  - Restored **Murphy**
### Fixed
- **Fog** has been fixed (fixes hdos/issues#1524)
  - *Fog has not been working correctly for years, ever since 6 view was added (let alone 12!).  
    It was configured and designed for 3 view back in 2008.  
    We finally sat down and figured out how to scale it to behave as it did
    back then at 3 view, and also scale well at higher view distances!  
    So everyone can expect many areas to suddenly have a more detailed atmosphere to them!*
  - <details>
    <summary>Click here for a list of affected areas</summary>
    
    - Morytania
    - Al-kharid
    - Kharidian desert
    - Runecrafting altars
    - Lumbridge swamp
    - Draynor village
    - West-Ardougne
    - Dwarven mine
    - Mudskipper Point
    - Fairy Ring DKS
    - Fremennik isles
    - Kraken's Cove
    - Stronghold slayer dungeon
    - Lighthouse
    - Guardians of the Rift
    - The Abyss
    - Waterbirth Island
    - Various other caves
    - Unlisted areas with inherited HD atmos
    </details>
- Regressions from the anniversary update have fixed in Lumbridge, Varrock and Falador
- Fix the wilderness lever options
- The following **interactions** have been fixed:
  - Door of the tannery shop in Al-Kharid
  - Front door of Jerico's house in Ardougne
  - Front (thieving) door of Ross' house in Ardougne
  - Side doors in the Ardougne bar
    - Roofs also hide more accurately here now
    - Some lights have also been added to the bar
  - The southern furnace in East-Ardougne has some more fences to clearly indicate where you can walk
  - A missing booth and a Z-Fighting bank table have been fixed in the northern Ardougne bank
  - A door in the Port Sarim jail
  - A chest in Rellekka
    - Some minor corrections have also been made to the terrain close-by
  - A fence stuck in the gate by Fred the Farmer's house
  - The fountain in Al-Kharid
- **Gnomes** and **Ents** should now animate correctly
- The **Heroes Guild** dungeon is once again restored
- Fixed a Z-Fighting wall by the entrance of Falador's crypt
- A HD bug with **Radimus Erkle** has been fixed, and has been restored as a result (fixes hdos/issues#1518)
- The ocean south of Neitiznot has returned to normal
- An underwater issue has been fixed by the strange altar south of Rellekka (fixes hdos/issues#1517)
- Restored **Askeladden** under certain conditions (fixes hdos/issues#1516)

## [334] 2023-3-19
### Fixed
- Fix particles on the infernal max cape...

## [333] 2023-3-19
### Fixed
- Fix particles on the fire max cape

## [332] 2023-3-19
## Changed
- #### HD Pack 8
  This version marks our eighth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack primarily targets **Tempoross** and the **Ruins of Unkah**, and introduces the following changes (fixes hdos/issues#776):
  - The Ruins of Unkah terrain has been drastically changed to more accurately reflect HD deserts
  - All **objects** in the affected areas have been upgraded or changed
  - All humanoid **NPCs** (and their chatheads!) have been upgraded with custom HD NPCs
    - **Vultures** and **Crocodiles** have been restored
    - Other NPCs have been textured
  - Several **projectiles** have been upgraded
  - Custom **underwater objects** have been created for objects located in the sea
  - **Shorelines** have been added to all affected areas, and the Tempoross encounter zone has been patched up
  - The **Tome of Water** has been remastered and now drips water
  - All other max capes have been remastered with completionist cape variants
    - The **Mythical-, Infernal-, Fire-, Imbued God-, Assembler- and Masori Assembler** capes now use custom particle emitters
    - The **Champion's cape** has also been remastered in the style of a master cape
  - **Fairy ring** butterflies now use Guthixian particles
  - The **Kharidian Desert** east of Unkah, and the **Quarry** have been restored
  - The **Colossal wave** mechanic during the Tempoross encounter now changes the Atmosphere around you

  - Other remasters:
      ```
      - Tackle box interface
      - Heron pet
      - Great Blue Heron pet
      - Tiny Tempor pet
      ```
  
  - **Note**: The angler outfit has not yet been remastered and will be revisited later, due to lack of tools
- **Captain Klemfoodle** has been restored
- A raging snowstorm by the fairy ring **DKS** has been added
- Some **sleepwalkers** by Canifis should now be remastered
- Improve fog when zooming out
### Fixed
- The assets concerning **Obor** and HD Pack 7 should now be working as intended
- **Gem rocks** in Shilo Village, the Shilo mine, and the Lunar mine have been upgraded once again after their latest regressions (fixes hdos/issues#1520)
- Lightning clouds during the **Tempoross** encounter have been fixed (fixes hdos/issues#1095 hdos/issues#832):
  - An issue where clouds could prioritize their clickbox over the dousable fires no longer occurs
  - The lightning bolt shooting out of the cloud now displays correctly
- **Captain Errdo** and **Brimstail** are no longer downgrading and T-posing under certain conditions (fixes hdos/issues#1500)
- All fire- and infernal cape variants should no longer lose their texture when entering a fairy ring

## [331] 2023-3-9
### Changed
- Improved MapFilter for underground pass
### Fixed
- Update **protocol** to 212
- The **Lady Trahaearn dungeon** used in the Song of the Elves quest has been pulled out of the void

## [330] 2023-3-5
### Changed
- Restored the following areas:
  - **Ape Atoll**
  - **RFD: Freeing King Awowogei dungeons**
  - **Ape Atoll dungeon** (Greegree creation cave)
  - **Temple of Marimbo dungeon** (Zombie monkeys)
  - **Banana Plantation Arena** (Monkey Madness I boss fight area)
- Restored 82 **monkeys** (primarily on Ape Atoll)
- Enlarged icons for the **Ancient Spellbook** have now also been upgraded to HD equivalents
- Restored **Explorer Jack** (replacing Hatius Consaintus)
- **Completionist cape particles** can now have bloom providing the option is enabled
- Restored the **dance floor emote** (replacing the new celebration emote)
- Remastered the **Jad plush**
- Textured the **10th anniversary balloons and cape**
- The priest in East-Ardougne that could not be talked to prior to the Song of the Elves quest, now has a chathead when talked to (fixes hdos/issues#1504)
- The **Imbued Slayer helmet chathead** has been restored (fixes hdos/issues#1306 hdos/issues#1382)
- **Lanthus**' chathead has also been restored
- Some clue scroll maps have been remastered, and the Z-Fighting has been fixed (fixes hdos/issues#1293)
- Restored the waterfiend attack animation (oldschool has none since the old model can only punch)
- **Hans** and the **Ranged Combat Tutor** have been restored (fixes hdos/issues#1511)
### Fixed
- A downgraded kitchen tile in **Lumbridge** that can act as a holiday event minimap icon has been fixed
- Repaired the cat skeleton after the 10th anniversary update. **Cats** should now be restored again (fixes hdos/issues#1502)
- A specific quest variant of **Gertrude's kitten** has been restored
- Several sound effects in the **Theater of Blood** raid should now be working correctly
- **Wilderness Ent** models should now be using the new models
- Resolved an issue where **waterfiends** and other fiends like the **icefiend** would have conflicting skeletons. All fiends should now perform their animations correctly
- (custom) NPCs can now correctly retexture whilst using the same models
- **Capt' Arnav** is now content with just his rum instead of trying to kill you with his rum-axe when the random event triggers (fixes hdos/issues#399)
- The Gauntlet scoreboard now displays correctly (fixes hdos/issues#1508)
- The cave entrance in **Taverley Dungeon** leading to the Cerberus junction area, is no longer invisible when 'hide roofs' is toggled on (fixes hdos/issues#1510)

## [329] 2023-2-22
### Changed
- Remastered some of the new anniversary objects
- Enlarged icons for the **Normal Spellbook** have been upgraded to HD equivalents (fixes hdos/issues#1496)
  - *It took quite a bit of time to upgrade 140 sprites, so we'll be releasing them in batches*
- Restored **Crash Island** (including the snake pit)
  - Also restored **Lumdo** and **Waydar** (Monkey Madness II is able to transform them)
- The **Dramen staff** appearance (including its inventory icon) has been changed to its OS equivalent (fixes hdos/issues#1492)
  - *After an OS update in 2018, the staff had its appearance changed to make it distinct from the regular battlestaff*
  - The oldschool equivalent has also been textured to reflect the HD style
- The colour of **Vorkath's dragonfire** projectiles has been made more accurate (fixes hdos/issues#1473)
- The white **Holy book** has been changed to blue to reflect the polled change from 2017, making it easier to differentiate it from the book of law
- Restored one of the Grand Exchange clerks (hdos/issues#1390)
### Fixed
- Fixed an issue related to lights casting on diagonal objects
- Improved terrain when a new update overrides a restored area
- Fixed the Lumbridge Anniversary terrain
- Goblins should no longer T-pose
- The **Keris Partisan** now shows the correct attack style info for 'Pound' (fixes hdos/issues#1284)
- Chairs in the **Ardougne banks** will now properly transform into group storages for irons and deadman chests on PvP worlds (fixes hdos/issues#1495)
- Bushes in the **Lumbridge courtyard** will now properly transform into PvP related objects on PvP worlds
- The **West-Ardougne wall** that is used in the Biohazard quest should no longer be rotated (fixes hdos/issues#1494)
- **Vorkath's sound effects** have been fixed (fixes hdos/issues#825)
- Fixed an oversight where some chests would be facing the wrong way around
- **Mammoths** have been downgraded to fix their animations (fixes hdos/issues#470)
  - *These will be remastered later on*
- The **Mysterious Watchers** in the Champion's Guild arena are now facing the correct direction (fixes hdos/issues#515)

## [328] 2023-2-12
### Changed
- Goreu, Arvel, Mawrth, Eoin and Iona have all been restored
  - Goreu no longer appears as another elf
### Fixed
- The following improvements have been made to **HD Pack 7**:
  - Interactable objects on the water should no longer be absent, such as the stepping stone shortcut. Underworld has also been added for the shortcut
  - The blood has been re-added in the Forthos dungeon
  - A cursed altar in the Forthos dungeon has been textured
  - The regular altar in the Forthos dungeon has had its textures improved slightly
- The male Ava's assembler now more accurately aligns like the accumulator variant
- The following has been fixed by the **Ectofuntus**:
  - Removed some old summoning obelisk lights
  - Re-added a wall by the stairs

## [327] 2023-2-11
### Changed
- The Bow of Faerdhinen's size has been slightly reduced
- #### HD Pack 7
  This version marks our seventh **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack primarily targets **Zeah: Hosidius**, and introduces the following changes:
    - 289 OS models have been textured, of which several have been remodeled
    - Numerous more objects have been upgraded with custom assets
    - All monsters in the Forthos Dungeon have been remastered
    - Terrain has been upgraded
    - Several objects have been adjusted to fix clipping issues such as the Kourend pathway and interior objects
    - Immersive lighting and atmospheres have been added to the affected areas
    - Shorelines and underwater objects have been added
    - Known 'dirt staircases' have been fixed
    - Large holes in the Hosidius' roofs have been fixed
    - The walls of houses in the Woodcutting Guild have been moved to be aligned with the HD configuration of housing. Underlays will no longer go outside the house, and roofs should no longer have gaps or be afloat
    - **Targeted areas:**
      - **Hosidius**
      - **Crabclaw caves**
      - **Barbarian Hideout** (A Kingdom Divided)
      - **Forthos Dungeon**
      - **Cellars** (A Kingdom Divided)
      - **Ent Dungeon** (Woodcutting Guild)
      - **Obor's Lair** (Edgeville Dungeon - Bonus!)
    - **Other notable remasters:**
      - Sarachnis Cudgel
      - Hill giant club
      - Xeric's talisman
      - Xeric's teleport
      - Sand crabs
      - Sarachnis, Sarachnis' minions and pets
      - Bloodhound Pet
      - Ents have been restored. The wilderness variant now has a custom HD model
      - House advertisement boards (next to house portals)
      - Book of the dead
      - Kharedst's memoirs
### Fixed
- In the **Vanstrom Klause** boss fight, the shadow that indicates an incoming lightning attack has had its visibility drastically improved
- The Fairy Hideout has had an initial restore to fix the bad edges
- South of Rellekka, a fairy ring has been removed that shouldn't be there (fixes hdos/issues#1490)


## [326] 2023-2-8
### Fixed
- Fixed a bug where projectiles could cause the client to crash

## [325] 2023-2-8
### Fixed
- Update **protocol** to 211
- Update the new Fairy beam model to respect new rigging
  - Should not affect aesthetics
- A chunk from **Lunar Isle's ocean** will once again appear (Ungael map collision issue has been fixed)
- A collision issue upstairs in **Falador's castle** has been fixed


## [324] 2023-2-3
### Changed
- Snow interfaces have been discontinued
  - *We plan to add snow particles instead, starting with the areas part of the Fremennik province restore (Trollheim comes later)*
- The following areas in the **Fremennik province** have been restored:
  - **Fremennik Isles**
  - **Fremennik isles boss dungeon**
  - **Jatizo**
  - **Neitiznot**
  - **Pirate's Cove**
    - **Captain Bentley** has also been restored (fixes hdos/issues#900)
  - **Lunar isle**
    - Includes the **Lunar mine** and **Baba Yaga's house**
  - **Fremennik-to-Keldagrim tunnel**
  - **Etceteria**
  - **Miscellania**
  - **Kingdom dungeon**
  - **Road to Lighthouse**
  - **Fremennik slayer dungeon**
    - Remastered the upstairs Kurask area
    - *We are aware the current style is quite outdated - we will return to this in the future*
  - **Mountain camp**
    - Remastered **Mac's island** (fixes hdos/issues#555)
  - **Mountain daughter dungeon**
  - **Rellekka**
    - *The agility course heights have been improved, and several objects in the area have been remastered*
    - **Fremmenik trials dungeon**
    - **Polar hunter area**
    - **Polar eagle cave**
  - **BrineRatCavern**
- Upgraded the **Shilo Village mine**
  - Restored atmosphere
  - Remastered cave-walls (custom gradients to reflect the HD art style)
- **The Kraken**'s (boss) whirlpools' visibility has been increased similar to the other cave kraken whirlpools
### Fixed
- Fix button settings
- **Shilo Village**'s darkness has been lifted (fixes hdos/issues#1458)
- A debug (rapier) animation that would play whilst wearing a **rune pickaxe** has been disabled, and should now be back to normal
- Furniture will once again indicate that something can be built when on a construction contract in Falador (fixes hdos/issues#1481)
- The algorithm to generate caves has been adjusted to no longer place pebbles in the void (where there is nothing)
  - Extra void has also been placed south of the mining guild, ensuring nothing can be seen from odd camera angles
- MapFilter for the Freaky Forester has been fixed (fixes hdos/issues#1486)

## [323] 2023-1-25
### Changed
- Restored the **Voidwaker** as Korasi's sword
- The snow has melted in the affected Christmas event areas
### Fixed
- **Ket-Zek**'s on wave 62 of the Fight Caves once again has one red and one orange variant to indicate where TzTok-Jad spawns (fixes hdos/issues#660)
- Construction contract furniture in Varrock should now become transparent whilst on a contract

## [322] 2023-1-23
### Changed
- The **Ourania Hunter and -ZMI caves** have been restored
  - Many NPCs have also been restored inside the ZMI caves
    - Cave salamanders have also received slightly improved animations
  - Several objects and structures have been upgraded from the original HD map to more accurately fit the new style used in the Khazard Battlefield area
  - Custom mining rocks have been added to the area to more accurately reflect other rocks in the vicinity
- The **Khazard Battlefield** has been restored (fixes hdos/issues#969)
- The **Observatory** and the **Observatory dungeon** have been restored (fixes hdos/issues#1002)
  - The grappling hook agility shortcut has also been remastered
- **Rick** in the Wizard's tower has been restored with Wizard Korvak from the RuneCrafting Guild (fixes hdos/issues#1463)
- Added the new shortcut in Weiss to the agility plugin
- The Witch's shed in **Taverley** has been upgraded
- **Taverley druids** have been restored (fixes hdos/issues#551 hdos/issues#643)
- ~131 **cave goblins** have been restored and should no longer go stiff (fixes hdos/issues#1378)
- **Locust**'s have been restored and should now properly animate (fixes hdos/issues#1088)
  - *This one was a little tricky to test thoroughly. Please create an issue if you still encounter any problems with them*
- The Pharaoh's sceptre has been restored once again
- Some newer versions of **Elena** have been restored
### Fixed
- Fixed scaled NPC clickboxes (fixes hdos/issues#1444 fixes hdos/issues#1472)
- Significantly improved the click-boxes of the caves in the **Asgarnian Ice Dungeon**, making it harder to mis-click them whilst trying to move to the wyvern safespot on the floor above them (fixes hdos/issues#1424)
- The following improvements have been made to **Castle Wars**:
  - Several staircases have been remodeled to accommodate for their OS clickbox (fixes hdos/issues#756)
  - Battlements can once again be interacted with (fixes hdos/issues#528)
  - Several props near the lobby bank chest have had their models upgraded
  - Certain doors should no longer downgrade
- Several custom assets have been created for **Ardougne** allowing us to fix the following:
  - The West-Ardougne wall by the Dark Mage's castle is now fully restored (fixes hdos/issues#1387)
  - Staircases in ardougne now align properly (fixes hdos/issues#1383)
    - Also corrected an issue in the model that caused it to Z-Fight under certain circumstances
  - A custom model has been created for the diagonal Ardougne Door, and as such has now been restored
    - This also allowed regular (non-diagonal) doors to be upgraded to its original counterpart
  - A cabinet in the Ardougne Mansion has been counterfeited with a smaller variant to prevent it from clipping into the wall
  - Some generic drawers have had their model rotation adjusted which should fix some interaction issues in Ardougne and Falador
- The rotation of the drawers in **Taverley** has been corrected (fixes hdos/issues#1437)
- The **Druids Circle** has been improved with the following fixes:
  - The sources of the point lights have been corrected, so they come from the torches instead of the stones
  - Torches have been upgraded to druid's circle torches
  - The Altar of Guthix model has been adjusted slightly to accommodate for Oldschool's collision (fixes hdos/issues#583)
- Broken shorelines by **Corsair Cove** have been removed until its remaster (fixes hdos/issues#83)
- Some collision with the cow fencing by the waterwheel in **Lumbridge** has been fixed (fixes hdos/issues#731)
- The barn north-west of **Lumbridge** has had its walls corrected (fixes hdos/issues#513)
- Some drawers in **Catherby** have been shrunk slightly to prevent them from Z-Fighting (fixes hdos/issues#556)
- The staircase going down to the **dwarven tunnel** underneath White Wolf Mountain has been corrected
- **Yt-HurKots**' (Jad's healers) animations have been fixed (fixes hdos/issues#659)
- **Grubfoot**'s animations have been fixed (fixes hdos/issues#1027)
- **Fluff's kitten** should no longer appear as a book of law (fixes hdos/issues#774)
- Snowy yew trees no longer turn into snowy willow trees after being chopped down
- Several objects should no longer downgrade upon interaction (fixes hdos/issues#1474)
  - A door in West-Ardougne
  - Several doors in East-Ardougne
  - Drawers in Falador
  - Drawer south of the Ranging Guild, by the windmill
  - A chest in Hemenster
  - Wooden gate in Taverley Dungeon
  - Coffins in Taverley Dungeon
- Some objects in the upstairs Black Dragon area in Taverley Dungeon will no longer be shown when downstairs
  - *Some objects still will be, this is intended*
### Audio (thanks to `@RS Orchestra#9880`)
#### Changed
- Upgraded Secrets of the North music to HD soundset
  - More Than Meets the Eye (Orchestrated Remix)

## [321] 2023-1-14
### Changed
- Improved the **Phantom Muspah** boss room atmosphere
- Several hitsplats have been upgraded to fit the 2009 style including max hits and other players' hits
  - Phantom Muspah's prayer shield
  - The Nightmare's totem charging and healing
  - The Nightmare's totem uncharging and damage
  - Palm of Resourcefulness crocodile's damage
  - Palm of Resourcefulness's growth and Kephri's scarab shield
- Corruption/Prayer shield (health)bars have been upgraded, such as the Phantom Muspah's shield
- **Scape Theme** once again plays in the Login Screen and in-game, as Christmas has passed (fixes hdos/issues#1470)
- Remastered the **Adventure Paths** banner that appears in front of the Lumbridge Pub when active
- Improved MapFilter for the **Lunar island mine** and **Baba Yaga's house**
- After some feedback, we have improved **Amy's house** and added the following details:
  - Put back and remastered some old paintings
  - Upgraded the carpet
  - The door has been put back
### Fixed
- A presumed Oldschool RuneScape bug that causes the shield hitsplats on the Phantom Muspah to appear as hitsplats from other players has been fixed until it gets resolved server-side
- The objects that spawn during the Secrets of the North quest in the **Carnillean's estate** should now be upgrading as intended
  - *This only applies to the already restored Ardougne objects, as well as the broken glass* 
- The Arceuus agile (graceful) top no longer appears as an amethyst object
- Oldschool's fairy ring beams no longer cast a shadow
- The odd wall in **Taverley Dungeon** used for the Scorpion Catcher quest can once again be interacted with


## [320] 2023-1-12
### Fixed
- Fix snow regressions in Lumbridge and Varrock

## [319] 2023-1-12
### Changed
- Restored **Falador** and several areas related to it: (fixes hdos/issues#232 hdos/issues#532 hdos/issues#802)
  - Restored the Dwarven Mine
    - Remastered the Mining Guild and Amethyst Mine
  - Restored Taverley Dungeon (fixes hdos/issues#1258 hdos/issues#1421)
    - Remastered Black demons, Blue dragons and upstairs areas
  - Restored the Giant Mole Lair
  - Agility course height blending and animations have been upgraded
- The new fairy ring beam by Oldschool received an initial remaster
- Particles are once again visible through transparency such as window objects
  - *We recognize there is a deeper bug present we haven't been able to fix properly yet. With the Christmas event we decided to revert the logic on a partial fix so that the snow particles weren't disappearing when in front of trees. It is now back to what it was before the event*
### Fixed
- The following improvements have been made to the **Fight Arena** area:
  - Certain grass by will no longer be invisible and cause your character to fly through the map
  - A missing light has been added
- The Mythical cape will no longer appear as a Mythical Completionist's cape

## [318] 2023-1-11
### Changed
- Santa has departed and snow has been swept off the roads
- Several interaction issues related to the new **Secrets of the North** have been fixed
  - Ardougne areas related to the quest have been remastered
### Fixed
- Resolved an issue where custom assets would occasionally be missing models when upgraded from an OS model
- Some gradients on the cave walls in Evil Dave's basement have been fixed
- **MapFilter** has been improved
  - The Amethyst mine no longer sinks into the ground
  - Walking through the Falador dwarven mine no longer causes you to get stuck when a map is reloaded at the end of the cave
### Audio (thanks to `@RS Orchestra#9880`)
#### Added
  - Added Upright Bass to Patch 672 (Bank 5, Preset 32)
  - Added Beneath Cursed Sands to Patch 1281 (Bank 10, Preset 1)
#### Changed
  - The Guardians Prepare (HD Remix)
  - Temple of the Eye (HD Remix)
  - Guardians of the Rift (HD Remix)
  - Beneath Cursed Sands (Orchestrated Remix)
  - The Militia (HD Remix)

## [317] 2022-12-30
### Fixed
- The following changes have been made to **Brimhaven Dungeon**:
  - The shortcut to the Bronze Dragons has been restored and is once again accessible
  - Some set of skeletons no longer cause your character to teleport around when running over them
- The doors in the combat training camp north of Ardougne are now fully upgraded 
- The following changes have been made to **Keep Le Faye**:
  - Some furniture has been upgraded
  - Some collision issues have been fixed, and the respective lights have been removed
  - The docks will no longer show pillars floating midair
  - Chests and doors will no longer downgrade upon interaction
- The Edgeville Monastery's incompatible drawers have been downgraded to prevent downgrading upon interaction
- Doors in the Edgeville Monastery no longer change model upon interaction
- Furniture and doors have been upgraded in the Fight Arena area and should no longer downgrade upon interaction
  - **Note**: The doors being on the wrong tile is something to return for in the future
- Merlin's door in Camelot castle no longer downgrades upon interaction
- Wardrobes in the witch's house no longer Z-Fight with the wall
- Upgraded the trapdoor in Paterdomus
- The metal gate in the Paterdomus church has been upgraded when opened
- The terrain around Hespori from the Night at the Theatre quest in Morytania has been remastered, and no longer causes your character to stand on water
- The boat to Fossil Island is no longer blocked by mountainous terrain (fixes hdos/issues#616)
- The Oak tree fix intended for v301 now works correctly
- Improve alignment for animating objects within the scene
- An issue with the atmosphere in the Stronghold Slayer Dungeon has been fixed
- The upstairs blue dragon area in Taverley Dungeon is once again accessible whilst MapFilter is enabled

## [316] 2022-12-24
### Changed
- The following improvements have been made to the **Corporeal Beast's cave**:
  - The terrain has been restored
  - The exit into the wilderness has been restored
  - The private instance portal has been restored with a spirit portal from the Summer's End quest
  - The Ancient Brazier and wilderness Cave Entrance have been remastered
  - The ironman instance counterpart of the cave is now restored as well
### Fixed
- Cormorants (sea birds) will no longer slide across the ground when moving
- The Al-Kharid gates should no longer downgrade under certain conditions (fixes hdos/issues#1456)
- Fix transforming objects (Farming crops, barrows doors, ect.) (fixes hdos/issues#1449 hdos/issues#1451 hdos/issues#1457)
- Certain skyboxes are no longer black when they shouldn't be
- Improve camera shaking smoothing at higher fps targets

## [315] 2022-12-23
### Fixed
- Fix last updates missing map files

## [314] 2022-12-23
### Changed
- The second floor on the Lumbridge bar has been added (fixes hdos/issues#1408)
- #### HD Pack 6
  This version marks our sixth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets various **Slayer** and **Combat** activities, and introduces the following changes:

    - Restored the following areas in proximity to the remasters (fixes hdos/issues#224 hdos/issues#469 hdos/issues#1407):
        ```
        - Tree Gnome Stronghold
        - Glarial's Tomb
        - Brimstail's Cave (partial remaster)
        - Piscatoris hunter area (partial remaster)
        ```

    - Remastered the following areas (fixes hdos/issues#858):
        ```
        - Tree Gnome Stronghold's slayer dungeon
        - Monkey Madness II crash site (excluding the gorilla caverns)
        - Kraken's Cove
        ```
        - Lighting, and environment such as lava and underwater have been added to the areas to create a more immersive feeling
        - Added more realistic heights and depths to the slayer stronghold, similar to some other HD caves

    - Affected items:
        ```
        - Kraken's Tentacle
        - Saradomin-, Guthix- and Zamorak Imbued God Cloaks
        - Ancestral Robes
        - Justiciar's Armour
        - Inquisitor's Armour
        - Obsidian Armour
        - Blade of Saeldor (including all recolours)
        - Bow of Faerdhinen (including all recolours)
        - Abyssal Dagger (including poisoned)
        - Eldritch-, Harmonised-, Volatile- and regular Nightmare staves
        - Dinh's Bulwark
        - Dragon Harpoon
        - Dragon Hasta
        - Dragon Hunter Crossbow
        - Elder Maul
        - Kodai Wand and -Insignia
        - Twisted Buckler
        - Avernic Defender and -Hilt
        - Scythe of Vitur (charged and uncharged)
        - Sanguinesti Staff
        - Leaf-bladed Battleaxe
        - Slayer's Staff (e)
        - Eternal, Pegasian and Primordial boots
        - Guardian Boots
        ```

    - Additional changes:
        - Tridents are now using a HD animation
        - The Tridents' GFX's have been restored
        - The Bow of Faerdhinen has been remodeled and now has a string attached
        - The BoFa's crystal arrow GFX has been remastered
        - Some ground items of the respective remastered items have been remastered
        - The Avernic Defender has been remodeled (after the 2010's Flameburst defender)
        - Cave Krakens has been remastered
        - Cave Kraken Whirlpools are no longer completely transluscent
        - King Narnode Shareen, Daero, Errdo, Prissy Scilla and Bolongo have been restored


  - **Note**: *Some models have not yet been remodeled, so clipping- or chathead-related issues remain unchanged for those. Some of you have been longing for these for a while now, so we decided to include some non-remodeled versions to make the game seem less jarring. We are also aware some of you might long for HD overrides, such as chaotic weaponry. These are not included as of yet, but we are taking the ideas into consideration for the future.*

## [313] 2022-12-16
### Changed
- Ice has accumulated in the hay roofs in Lumbridge and the cooking's guild in Varrock
- Snowy Yew and Willow trees should no longer downgrade after being chopped down
- The pond in Lumbridge's forest has become frozen
### Fixed
- The fences by the potato field north-east of Lumbridge now respect collision correctly (fixes hdos/issues#1441)
- Tempoross and other misc. npcs no longer show when extended npcs is enabled
- Possible fix for a random bug where npcs would be swapped out with another.

## [312] 2022-12-15
### Changed
- #### HD Pack 5
  This version marks our fifth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets **Christmas**, and introduces the following changes:

  - Santa has arrived and instructed snowmachines to be placed in Varrock, Lumbridge, Edgeville and Barbarian Village
  - Restored Music (seasonal): Scape Santa, Smorgasbord, Haunting Christmas, Ghost of christmas presents
  - Many old NPCs have returned, such as **Jack Frost** and the **Snow Imps**!
  - Improved the performance of particles and reintroduced HD snowfall
  - Added a custom snowy atmosphere to all areas affected by the event
  - Added lights and improved several objects related to the oldschool event
  - Players now leave footprints in the snow
  - Over 125 objects have been remastered
  - A couple of NPCs have been remastered
  - Fixed the Z-Fighting issue on Santa's List
  - Restored the Candy cane
  - Restored the Anti-santa outfit as the Christmas Ghost outfit
  - Restored the newer Party hat inventory icons

  - Other major affected items:
      ```
      - Sweet nutcracker staff
      - Festive nutcracker staff
      - Sweet nutcracker outfit
      - Festive nutcracker outfit
      - Coal bag
      - Christmas jumper
      - Festive gamescrown
      - Santa's list
      ```

  Note: *We wanted to get this to you as soon as possible, and we don't want to keep you waiting any longer, so as before, chat-heads and armour will not be the full remodeled versions we would like them to be. We will be returning to all of those respective models in the future.*
### Fixed
- Several improvements have been made to areas within the Christmas event:
  - Improved several roof heights
  - The wilderness statistics board window has been removed, and the model has been upgraded to make it stand out more from the non-interactable variant
  - A quest icon in the Lumbridge Kitchen is no longer missing (it was fixed, but got unintentionally removed whilst fixing a bug)
  - A light has been added to the fireplace in the building by the north-eastern chicken pen
  - The 2022 Pride event area by Barbarian Village has been restored to more accurately reflect oldschool's terrain
  - A missing pillar around the Grand Exchange wall has been added, so it respects oldschool's collision
  - Altered the size of one of the cabinets in Duke's room to compensate for the wall
  - Fixed the collision issues by the Lumbridge cow pen (fixes hdos/issues#1441)
  - Fixed the location of the fire light in Barbarian Village
- Fix nex centering bug (along with her minions) (fixes hdos/issues#1110)
- fix extended npcs on floors > 0

## [311] 2022-12-8
### Fixed
- The Spooky Cauldron behind Zaff's shop in Varrock has been remastered
- The Rift Guardian pet remaster should now be working as intended
- Fix item examining

## [310] 2022-12-8
### Changed
- Drastically improved the Abyssal Nexus area
  - *We will be returning to fix the rest of the area as a remaster at a later stage*
- Improved MapFilter for the slayer tower basement (fixes hdos/issues#1434)
### Fixed
- Update proto to 210
- Other players pets are now hidden when trying to target them (fixes hdos/issues#1429)
- When targeting, the default option now behaves similar to OSRS, and doesn't render a blank "Use ->" option.
- The great blue heron pet will no longer appear as Donie in player-owned-houses
- Some Uranium-235 has been removed from west of Canifis, no longer making your character glow green when walking past it (fixes hdos/issues#1431)
- Some tile heights in Lumbridge have been adjusted to fix a sunken roof and a broken gate (fixes hdos/issues#1423)
- The following improvements have been made to Edgeville Dungeon:
  - Some collision issues that caused you to fly through the map have been fixed (fixes hdos/issues#1433)
  - Additional columns have been removed and moved to more accurately reflect collision
  - Some stalagmites have been cleaned up
  - Gates and chests will no longer downgrade upon interaction
  - A Z-Fighting issue has been resolved
- Fixed a collision issue east of the Grand Exchange
- MapFilter has been fixed for the Abyssal plane
- South of the Falador wall is no longer downgraded

## [309] 2022-12-1
### Fixed
- Fix Al-Kharid regression from last update
- Release all the audio updates intended for the last update
#### Audio (thanks to `@RS Orchestra#9880`)
- Tombs of Amascut OST upgraded to HD soundset
- Added Ballad of the Basilisk (Sung parts from RS3)
- Fight of the Basilisk (Frost Fight - HD Restore)
- Ballad of the Basilisk (HD Remix with RS3 sung parts)
- Roots and Flutes (HD Restore)
- Elven Seed (HD Restore)
- The Terrible Caverns (HD Restore)
- The Terrible Tunnels (HD Restore)

## [308] 2022-11-30
### Changed
- Improved terrain around the shortcut behind the Zamorak Temple to more clearly indicate where you can walk
- Improved the atmosphere in the Giants' Foundry
- Improved MapFilter in the Stronghold slayer dungeon
### Fixed
- Improve clickboxes to better match standard OSRS behavior (fixes hdos/issues#1171 hdos/issues#1182 hdos/issues#1267)
- Fix Loot Tracker performance issues when rendering a lot of loot (fixes hdos/issues#1174)
- Fix the Giants Foundry interface
- Fix overlay dragging (no longer randomly changes positions after you drop them)
- Improve the clickbox for ladder in the Motherload Mine
- Fix context cursors from breaking on formatted menu entries
- MapFilter improvements intended for v307 are now in effect
- The roof in the Zamorak temple west of Goblin Village now hides when climbing up inside the tower (fixes hdos/issues#1417)
- Red tiles no longer float above Taverley (fixes hdos/issues#1416 hdos/issues#1414)
- Using the agility shortcut behind the Zamorak temple will no longer cause your character to fly across the map
- Several interaction problems have been fixed (fixes hdos/issues#1413):
  - Doors in the basement of the Wizard's Tower no longer downgrade upon interaction
  - Port Sarim's interaction fixes should now be in effect properly
  - Doors in Edgeville and the Monastery (Oziach's shop) will no longer downgrade upon interaction
  - Wardrobes in Rimmington will no longer rotate upon interaction
  - The bank objects in the Rogue's Den have been re-added (fixes hdos/issues#1415)
- The carpet in the Ardougne's Estate Agent building has been repaired by Sarah's trusty loom
- Fixed an issue where another fix regarding the 'hide roofs' option in the Lighthouse areas was too aggressive (fixes hdos/issues#1426)

## [307] 2022-11-28
### Added
- Added support for full screen fixed mode
### Removed
- Removed the ability to change window modes outside of the game (replaced with a `Full Screen` toggle)
### Changed
- The heroes guild and the heroes guild's dungeon have been restored (fixes hdos/issues#605)
  - MapFilter has also been improved for the dungeon
- Witch's House dungeon has been restored
- Burthorpe has been restored
  - Also restored the Zamorakian temple and fixed the Falador wall (fixes hdos/issues#807)
  - Burthorpe's games room has also been restored, but will see a rework if it becomes relevant content
- Rogues' Den has been restored
  - MapFilter has also been improved
- Uzer, the Uzer ruins and the Shadow realm have been restored (fixes hdos/issues#374)
- The poll booth flag has been textured
- The following improvements have been made to Taverley (fixes hdos/issues#550):
  - Stools are no longer clipping inside tables (fixes hdos/issues#633)
  - Stools, staircases, bookcases and sword racks have been upgraded
  - A missing roof has been added
- Quick Settings now automatically clears any active search when the side panel is collapsed
### Fixed
- Fix a bug where plugins and other UI elements break on client reset (fixes hdos/issues#1300)
- Quick settings no longer requires pressing double-shift twice to close
- Fix a bug where the minimum size of the client was wrong after opening/closing the sidebar (fixes hdos/issues#1349)
- Fix a bug where plugins would disappear within the quick settings if the user changes window modes after starting the client
- Fix a bug where orbs would disappear when changing window modes when in full screen
- Improve transparency rendering for landscape scenery
- The following improvements have been made to the Edgeville Dungeon:
  - The sign on the door is now visible on both sides (fixes hdos/issues#424)
  - The columns now more accurately reflect collision
  - Several excessive stalactites have been removed
  - All mining rocks are now stay upgraded when mined
- The "strange hole" from the "A Porcine of Interest" quest is now also fixed prior to completing the quest
- A fence in Lumbridge has been replaced by the correct type
- The fireplace in the Lumbridge general store no longer shines through the wall
- The mining rocks in the western part of the Lumbridge swamp should now also stay upgraded when mined
- Sir Rebral now properly upgrades (fixes hdos/issues#766)
- The upper walls in the lighthouse dungeon no longer disappear while 'hide roofs' is enabled

## [306] 2022-11-25
### Fixed
- Agility plugin: Fix broken shortcut regression from v305

## [305] 2022-11-25
### Changed
- Restored Al-Kharid and Shantay Pass (fixes hdos/issues#1395)
  - Captain Dalbur has also been restored
- 86 different skeletons have been restored (including the Skeletal Mystics from CoX!) (fixes hdos/issues#1403)
- Gargoyles are now upgraded when they shatter
- Numerous improvements have been made to Lumbridge:
  - The downgraded tiles in the Lumbridge kitchen and -basement have been upgraded
  - Several walls that are adjacent to doors have been replaced by the compatible variants
  - The two stairs on the upmost floor of the Lumbridge castle have been fixed
  - Restored the interior inside the Lumbridge castle bank
  - The cannonballs displayed as a ladder by the Lumbridge castle bank have been put back
    - The ladder has been moved to the other side
  - Restored the back entrance of the Lumbridge courtyard (and re-added the tower)
  - Dozens of models have been restored or upgraded
  - The king's statue has been remodeled to be compatible for the restore (fixes hdos/issues#1404)
  - The general store has been overhauled
  - Windows of all the houses have been restored
  - Pathing has been improved to accommodate for oldschool layout
  - The oldschool bushes that are seen for non-ironman accounts (instead of the portal) have been remastered
  - The rusted anvil has been remastered
  - In several places the types and colours of fences and fence gates has been improved (fixes hdos/issues#494)
  - Several parts of the church have been restored
  - The "strange hole" from the "A Porcine of Interest" quest has been remastered and no longer has dirt inside it (fixes hdos/issues#512)
  - The mining rocks in the Lumbridge Swamp have been upgraded permanently
  - Improved the accuracy of roof removal in several places
  - The bar has been restored, and the bar sign has been remastered (fixes hdos/issues#456)
  - Restored NPCs:
    - Woodcutting tutor (Woodsman tutor)
    - Smithing tutor (Smithing apprentice)
    - Bartender
    - Account security tutor (Henry, Professor of Stronghold of Player Safety)
    - Bank tutor
### Fixed
- The following improvements have been made to the Grand Exchange agility tunnel shortcut:
  - The upper walls are no longer slightly sunken and the tile heights have been improved
  - The surrounding pebbles of one of the holes have been re-added
  - Some upper walls will no longer disappear whilst roofs are hidden
  - An issue where the minimap would display two agility shortcut icons has been fixed
- Fixed an issue where tile heights would conflict and raise a building's corner in the air after growing back the hedges by Ardougne's mansion
- The player is no longer drunk whilst trying to perform the spinning wheel animation
  - *We are looking into solutions to fix the spinning wheel itself*
- Fix item outlines in misc. interfaces (fixes hdos/issues#1180)
- Improve terrain blending
- Fix a bug where NPCs would disappear when the client recovers from a disconnect

## [304] 2022-11-22
### Changed
- Change default world to 301
- The Kourend (white) graceful outfit has been cleaned by Sarahs trusty loom
### Fixed
- Fix known crashes

## [303] 2022-11-21
### Fixed
- Fix OpenGL NullPointerException crash
- Fix underworld crack in Tutorial Island

## [302] 2022-11-21
### Changed
- Restored Tutorial Island, and remastered The Node (fixes hdos/issues#219 hdos/issues#332 hdos/issues#580)
  - _The Ironman dialog (issue 1392) will be fixed soon_
- Restored the Undead Trees around Draynor Manor (fixes hdos/issues#1402)
- Ground Items: reformat the game message notification
### Fixed
- MES: Fix "Teleport to POH" priority over "Teleport" (fixes hdos/issues#1399)
- Additional Npcs downgrade fixes intended for v297 have been included

## [301] 2022-11-20
### Changed
- Restored Draynor Manor (fixes hdos/issues#213)
- Restored the Yanille Agility Dungeon (fixes hdos/issues#364)
- Restored North-East of the Kharidian Desert (fixes hdos/issues#375)
- Made the following improvements to Yanille:
  - Restored the Yanille area whilst you are in the upstairs section of the Watchtower
  - Upgraded the fountain by the Watchtower
  - Improved the staircase leading into the Yanille Agility Dungeon (fixes hdos/issues#658)
  - Two small corners in the entrance wall of Yanille have been fixed as part of an underlying issue
- Captain Barnaby has been restored (fixes hdos/issues#387)
- Catacombs of Kourend's atmosphere has been brightened slightly
### Fixed
- Fix agile inventory sprites
- Fixed the following in the Spirit of Elid and Kalphite (task-only) Slayer dungeons:
  - Random oldschool walls have been cleaned up (fixes hdos/issues#368)
  - Improved MapFilter so both dungeons are no longer visible for each other
    - Tears of Guthix is also no longer visible from either of these dungeons whilst MapFilter is enabled
  - A missing wall piece in the task-only dungeon has been added until its remaster
  - Added a cave atmosphere to the task-only dungeon
- Fixed the lava in Brimhaven Dungeon (fixes hdos/issues#1393)
- Fully grown and diseased Oak trees have had their bushes trimmed (fixes hdos/issues#301)
- Bill Teach has been restored, and is no longer missing his torso (fixes hdos/issues#366)
- Ross' arms are no longer burnt (fixes hdos/issues#397)
- Fixed an issue where Draynor Manor's atmosphere would get stale after enabling the Halloween event

## [300] 2022-11-16
### Changed
- *AGILE WHEN? AGILE NOW!*  
    (_192 models later, all agile sets have been remastered_)
### Fixed
- Fix sit-on objects (fixes hdos/issues#303)
- The following improvements have been made to Port Sarim:
  - Some furniture has been upgraded
  - Some (interactable) furniture has been downgraded, to prevent transformation upon interaction (fixes hdos/issues#1311)
- Menu Entry Swappers: Fix priorities (fixes hdos/issues#1239 hdos/issues#1265 hdos/issues#1104) 


## [299] 2022-11-16
### Fixed
- Fix downgraded assets and other regressions from today's game update

## [298] 2022-11-16
### Changed
- All the areas affected by Oldschool's Halloween event have returned to normal
- Juna has been restored
- Agility Plugin: add Meiyerditch and Revenant cave shortcuts
- Remastered some motherlode mine models to make it less jarring (fixes hdos/issues#385)
- Improved Zalcano's atmosphere (fixes hdos/issues#200)
  - Additionally, some lighting was added
### Fixed
- Players can now properly leave the Pest Control boat without ending up floating over the boat (fixes hdos/issues#1369)
- Fixed L-shape movements (fixes hdos/issues#1123)
- The Inferno cape inventory icon no longer becomes invisible when using Low texture detail
- Improve various chathead animations
- Fix known crashes
- A remaining underwater issue along the shore of Hazelmere's island has now also been fixed (fixes hdos/issues#1365)
- The mining rocks in Barbarian Village will no longer return to the default colour variant whenever they are mined (fixes hdos/issues#1237)
- Mayor Hobb has taken off his ice skates and no longer slides across the floor when walking around (fixes hdos/issues#1362)
- Dense Essence Fragments have had their inventory icon corrected (fixes hdos/issues#1334)
- The Falador Teleport tablet icon no longer appears larger than the rest (fixes hdos/issues#1273)
- The cannons during the Tempoross fight now shoot fish instead of light spheres (fixes hdos/issues#1268)
- Fixed several missmaps where the Barbarian Assault icons would replace other models (such as in the Hallowed Sepulchre) (fixes hdos/issues#421 hdos/issues#1230)
- Improve animation smoothing (fixes hdos/issues#1196 hdos/issues#1380) 
- Patched up the river west of the Gnome Ball Field (fixes hdos/issues#52)
- The Jormungand's Prison Dungeon (Island of Stone) has been cleaned up (fixes hdos/issues#183)
- Made the following improvements to the "Getting Ahead" quest (fixes hdos/issues#379):
  - Atmosphere of the cave has been improved
  - The interaction with the cave exit has been corrected
  - Flour should now appear correctly
  - Skeletons are no longer mismapped
- The Fisherman Realm has been restored, and:
  - Restored the Freaky Forester random event (they secretly hide there..)
  - Heavily improved background terrain and MapFilter
  - Fixed all the torches burning through the roof and improved lighting (fixes hdos/issues#361)
- Fixed an underwater issue at the Wizard's Tower

## [297] 2022-11-14
### Added
- Added background fps: ***when unfocused fps will be capped to the desired value***
- Camera Plugin: Add the ability to unlock the minimum pitch
### Changed
- Restored the Lumbridge Swamp Caves
- Restored Tears of Guthix
- Restored Edgeville Dungeon's earth warrior area (fixes hdos/issues#1374)
- Restored Pest Control (fixes hdos/issues#345 hdos/issues#696 hdos/issues#1371)
- Make the default outline thickness more sleeker
### Fixed
- Fix random cases where the client would hang for a few seconds before recovering
- Misc. npcs have been restored back to their original forms prior to this weeks game update
- Removed an unnecessary wall at the entrance in Brimhaven Dungeon (fixes hdos/issues#1364)
- Port Sarim chests, drawers and doors should finally work as expected (fixes hdos/issues#1311)
- The z-fighting in the Port Sarim jail has been resolved (fixes hdos/issues#1311)
- Paterdomus Temple walls have reappeared
- Slightly moved a wall in the Paterdomus Temple to where you should not be able to walk
- The Lumbridge Swamps-Lumbridge Cellar shortcut is once again accessible whilst MapFilter is enabled (fixes hdos/issues#1379)
- Tears of Guthix is once again accessible from the Lumbridge Caves whilst MapFilter is enabled
- An underwater gap along the shore of Hazelmere's island has been fixed (fixes hdos/issues#1365)
- The tool leprechaun restore intended for v296 should now work properly (fixes hdos/issues#1363)

## [296] 2022-11-9
### Changed
- Restored and remastered Brimhaven Dungeon and its new areas (fixes hdos/issues#893 hdos/issues#435 hdos/issues#428 hdos/issues#138)
- The eeriness of Halloween has subsided, and the crew has packed up their things. Until next year!
- The bushes at Lord Handelmorts home in Ardougne have been now properly watered by Horacio
- Restored baby green dragons
- Restore Tool Leprechaun
- Added a missing rock in the Port Sarim waters
### Fixed
- Improve update performance (the client should no longer get stuck at "Fetching Config")
- The sidebar button should no longer get lost randomly in the middle of the screen (fixes hdos/issues#1069)
- Improve Opponent Information plugin (fixes hdos/issues#1358)
- Fixed window symmetry of the Edgeville Monastery (fixes hdos/issues#1328)
- You can no longer walk through the wall of the Edgeville Monastery (fixes hdos/issues#1328)
- The web in the Temple of Ikov can once again be interacted with
- (Desert) goats will now perform their animations correctly
- A type of wooden gate in Nardah will no longer be rotated the wrong way around (also fixes future cases of this model)
- The door in Gerrant's fishing shop in Port Sarim will no longer duplicate (fixes hdos/issues#1311)
- Fix misspelled agility courses within the Agility plugin
### Removed
- Remove Quick Settings from the toolbar (since it's enabled by default now)

## [295] 2022-11-2
### Changed
- Restore Troll Stronghold (fixes hdos/issues#434 hdos/issues#888 hdos/issues#890)
### Added
- Add plugin: Object Markers
### Fixed
- Screenshot: fix a bug where events are captured before they are rendered
- Ground Items: fix lootbeams from despawning when changing regions
- Fix overheads from clipping through the minimap
- Fixed a Z-Fighting issue regarding the player's shadow on the Seer's Village rooftop agility course
- The furnace in Burgh de Rott should once again be interactable during the In Aid of the Myreque quest (We are aware that the fire inside the furnace may not animate. We will have to revisit this later due to a deeper engine problem)
- Removed an excessive light in the Temple of Ikov
- The Ice Queen Lair is once again fully accessible whilst MapFilter is enabled
- A certain type of steel door no longer vanishes whilst it's being opened 

## [294] 2022-10-31
### Changed
- Restored the following areas in the Kandarin region:
  - Seers village
  - Temple of Ikov
  - Fishing Guild and part of Baxtorian
  - Coal trucks and the dwarven outpost
  - Ranging Guild
  - McGrubor's Wood
  - SinclairMansion
  - Elemental Workshop
- Screenshot plugin: add "Open Folder" button to open screenshot directory
- Screenshot plugin: enable "Valuable Drops" by default
- Restore Fire cape (i) (fixes hdos/issues#1290)
### Fixed
- Screenshot plugin: fix the year-month formatting to not be a month behind
- Fix crashing when zooming at a low target FPS (fixes hdos/issues#1352)
- Added some missing rocks and fixed an underwater issue in the Baxtorian falls
- Fixed a mapfilter issue where the metal dragon task-only area in the Brimhaven Dungeon would be inaccessible
- You can once again exit the Nature's Grotto whilst doing the quest (fixes hdos/issues#1351)
- Pyrefiends now perform the correct defend animation (fixes hdos/issues#1324)
- Several doors in Port Sarim no longer revert to their old models after opening and closing them (fixes hdos/issues#1311)
- Entrana fences are no longer sunken into the ground (fixes hdos/issues#1309)

## [293] 2022-10-29
### Added
- Add Plugin: Screenshot
### Fixed
- Fix known crashes
- Fix many incorrect chatheads
- Transmog Plugin: locking the look no longer breaks the client after re-logging (fixes hdos/issues#1341)

## [292] 2022-10-26
### Changed
- #### **HD Pack 4.5**
  This special version is an addition to our fourth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This additional HD pack version has a set of smaller improvements, and targets the **Oldschool Halloween event**. Although the event is limited, we may re-introduce some of the assets in a future event.

  The following changes are introduced:
  - Additional fixes were made after the initial fixes in v291
  - Remastered all objects
  - Added additional (slightly lighter) Halloween atmospheres to several zones
    - Also added lights to all of the new Jack-o'-Lanterns
  - The HDOS crew has provided the children in Varrock with improved H'ween outfits
  - Textured the Witch outfit

  *We may subsequently remaster more things or revisit armour in the future, but our tools are limited, so that'll be something for the remastering stage*
### Fixed
- Improve Pin Bar layout behavior
- Opponent Information: Fix player lookup, refine UI settings
- Fix Amulet of the Eye recoloring (fixes hdos/issues#1345)
- Fix the world list not listing specific worlds

## [291] 2022-10-26
### Changed
- Camera zooming is now smoothened out
### Added
- Add plugin: Opponent Information
### Fixed
- Quick Settings no longer closes on world hop
- Fixed a crevice in the Varrock river
- Fixed the OS Halloween 2022 event
- Improved MapFilter for the slayer-only kalphite dungeon and the Desert Treasure pyramid

## [290] 2022-10-21
### Changed
- Improve Halloween audio behavior so users can manually select songs
### Fixed
- Ground items should no longer appear under objects when multiple items are stacked
- Fix downgraded assets from the last game update
- A small gap in the floor in the Legend's guild dungeon has been fixed

## [289] 2022-10-20
### Changed
- Quick Settings is now enabled and pinned to the sidebar by default
### Fixed
- Random event restores intended for v287 have been fixed
- A sunken floor tile that was ripping the carpet apart in the Ferox Enclave has been fixed
- Fix the "The art of hocus-pocus" intended for v287
- Fix known crashes

## [288] 2022-10-19
### Fixed
- Deploy missing assets for v287 (Ferox Enclave)

## [287] 2022-10-19
### Changed
- #### **HD Pack 4**
  This version marks our fourth **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the **Death's Domain** and **Ferox Enclave** areas as part of the spooky halloween update, and introduces the following changes:

  - Remastered all objects
  - Improved pathing and ground textures
  - Upgraded NPCs (work-in-progress engine work)
  - Immersive lighting and atmospheres have been added
  - The falador crypt entrance roof will no longer be hidden as roof
  - Death's domain entrances are no longer filled with dirt (fixes hdos/issues#1240)
  - Added more torches to the Ferox Enclave dungeon
  - Added underworld (underwater) to pools and rivers
  - Several wrongly rotated chests have been fixed, and replaced with the remastered variant
  - Other notably affected models:
      ```
      - Staff of balance
      - Death's domain entrances (Lumbridge, Falador, Seer's Village, Ferox, Kourend and Prifdinnas)
      - Craw's bow
      - Thammaron's sceptre
      - Viggora's chainmace
      - Dragon pickaxe (or)
      - Dragon pickaxe (upgraded)
      ```
- #### **It's halloween!**
  - Remastered the paintings from the 2006 H'ween event, can you find them all?
  - Restored the 2006, 2007 and 2008 H'ween music tracks
  - Restored the Small- and Angel of Death gravestones (permanently)
  - Restored Gravedigger (and Beekeeper, Pillory and Pinball random events) as part of the halloween themed upgrades (permanently) (fixes hdos/issues#516 hdos/issues#845)
    - Also restored the mysterious old man (permanently)
  - Restored the 2008 ***Trick or Treat!*** Halloween event, by overhauling several zones:
    - Lumbridge
    - Draynor
    - Port Sarim
    - Rimmington
    - Grand Exchange
    - Edgeville
  - Restored Grim reaper hood (permanently)
  - Restored Zombie head (permanently)
  - Restored the Warlock- and Witch costume outfits with the Pyromancer outfit
    - The bruma torch has turned into a broomstick!
  - Some NPCs have decided to change into their halloween costumes
  - Purple sweets have turned into H'ween candy

  <br>

  *Note: Some of these changes will be reverted once Halloween comes to an end*

- Restored Port Sarim and Rimmington (fixes hdos/issues#1311 hdos/issues#1035)
  - Cleaned up the draynor agiliy shortcut tunnel (fixes hdos/issues#226)
  - Remastered the cabbage-port graphic (fixes hdos/issues#991)
- Remastered the Achievement cape, Achievement cape (t) and Quest Point Cape (t)
- The clan portal at the Grand Exchange has been improved
- The clue stash in the Edgeville Monastery is no longer invisible
### Fixed
- Fix item offsets for female players


## [286] 2022-10-18
### Fixed
- Transparent dialogs now correctly disable when transparent chat is disabled
- Fix npcs downgraded from last update (fixes hdos/issues#1338)
- Fix player skin colors (fixes hdos/issues#1024 hdos/issues#71)
- Summoning plugin now works for pets in POH (fixes hdos/issues#1337)

## [285] 2022-10-18
### Added
- Add summoning plugin
### Changed
- Restore fonts
- Restore transparent dialogs
- Restore the fixed mode game frame (finish the restore)
- Improve aliasing on the xp-orb sprites
- UI: Searching now automatically cancels when selecting a plugin from the search results
### Fixed
- Fix world map bugs (fixes hdos/issues#1261 hdos/issues#1192)
- Fix holes in the minimap and compass when in resizable mode (fixes hdos/issues#1137)
- Fixed an issue where minimap icons in Varrock were no longer shown after the restore
- Mowed the grass on the road from Varrock west-bank to the Grand Exchange
- Fixed a path issue by the Lumbridge windmill
- The strange doors in the Lighthouse dungeon will no longer decide to have fun with the transmog plugin
- Panning down the camera in the Lighthouse dungeon no longer makes the upper walls disappear
- Various torches should no longer burn as rigorously and burn through roofs
- Fixed a collision issue regarding the queue ropes in the Ardougne south-bank

## [284] 2022-10-13
### Fixed
- Fixed the landslide on the ice mountain
- Ardougne: Fixed the thieving door in Ardougne Castle
- Ardougne : Added some missing lights and fix bleeding lights (lights should no longer shine through walls)
- Icefiends now perform the correct animations

## [283] 2022-10-11
### Fixed
- Fix world map crashing

## [282] 2022-10-11
### Fixed
- Update protocol to 209
### Changed
- The settings window of a plugin can now be quickly closed using the `Tab` key (vs clicking the arrow)
- The plugin settings UI now remembers your last scroll position when navigating the UI

## [281] 2022-10-09
### Changed
- **Restored Ardougne**
  - A map with the exact restored locations can be found [here](https://cdn.discordapp.com/attachments/1000839218681102366/1026451614418092062/ardymap.png)
  - Other Ardougne improvements:
    - Areas where the Adougne Restore would otherwise create sharp bumps in tileheights have been smoothened out and modernized whilst maintaining the intended style
      - The old-school agility course heights have also been smoothened and improved (the plank on the course no longer makes you wobble up and down)
    - Fixed an issue where attempting to note items at the Leprechaun by the Ardougne Farm would result into interacting with the rock instead. (fixes hdos/issues#1247)
    - The Golden Gnome in the Legend's Guild has been remastered
    - A dozen objects in Ardougne have been altered to fit the restore
    - Several lighting effects that were overlooked in the original HD Ardougne have been added
    - Some areas now hide roofs more accurately
    - Remastered the Witchaven Dungeon (hellhound area) and jail cell underneath Ardougne Castle used in the Song of the Elves quest
- The agility sign on rooftop courses has been remastered
- Improve the Entity Hider plugin
### Fixed
- Add brightness fixes intended for v280
- Fixed the black area upstairs by the wyverns in the Asgarnia Ice Dungeon
- The skybox plugin no longer overrides Nex during her darkness phase
- Fix a bug in the texture projection (a good example is GE floors)
- Several improvements have been made to the Edgeville Restore:
  - Restored the furnace building
  - Improved the river and shorelines
  - Fixed a missing wall by Oziach
  - Created a path around the tree in the middle of the pathway
  - The Hell-rat behemoth rooms in Evil Dave's basement have been remastered (fixes hdos/issues#758)
  - The Wilderness Statistics board is once again visible (fixes hdos/issues#612)
  - The coffins in the Edgeville cemetery south of the bank have been restored and the interaction has been fixed (fixes hdos/issues#1312) 
  - Some missing lights were added
  - Removed a pillar that was clipping inside the Soul Wars portal
- Fixed an issue where the upper parts of the walls surrounding the GE would always be hidden

## [280] 2022-9-22
### Fixed
- Fixed chatheads (as per usual!)
- Fixed an issue where a dock was missing by the Ectofungus
- The True Blood Altar to the Myreque hideout cave entrance is once again accessible
- Fixed an issue where map filtering would block you from going further into the True Blood Altar tunnels
- Increased the brightness on some runecrafting altars, Giant's foundry, Catacombs and Motherlode mine (fixes hdos/issues#1107)
- Reverted an unintended Wildstalker hat model from overriding models
- Brightened the lights in the Warrior's Guild Basement
- Patched up the path leading to the Warrior's Guild Basement ladder
- The Skull slope on the agility course is once again visible, and is now highlighted by the agility plugin (fixes hdos/issues#1266)
### Changes
- Remaster the Lost Bag
- Restored Treasure trail items 
  <details>
  <summary>Click for a list of items</summary>

  - 3rd age range top (HD: Third-age range top)
  - 3rd age range legs (HD: Third-age range legs)
  - 3rd age range coif (HD: Third-age range coif)
  - 3rd age vambraces (HD: Third-age vambraces)
  - 3rd age robe top (HD: Third-age robe top)
  - 3rd age robe (HD: Third-age robe)
  - 3rd age mage hat (HD: Third-age mage hat)
  - 3rd age amulet (HD: Third-age amulet)
  - 3rd age platelegs (HD: Third-age platelegs)
  - 3rd age platebody (HD: Third-age platebody)
  - 3rd age full helmet (HD: Third-age full helmet)
  - 3rd age kiteshield (HD: Third-age kiteshield)
  - Ancient robe top
  - Ancient robe legs
  - Ancient cloak
  - Ancient crozier
  - Ancient stole
  - Ancient mitre
  - Armadyl robe top
  - Armadyl robe legs
  - Armadyl stole
  - Armadyl mitre
  - Armadyl cloak
  - Armadyl crozier
  - Bandos robe top
  - Bandos robe legs
  - Bandos stole
  - Bandos mitre
  - Bandos cloak
  - Bandos crozier
  - Bronze dragon mask
  - Iron dragon mask
  - Steel dragon mask
  - Mithril dragon mask
  - Nardah teleport
  - Tai bwo wannai teleport
  - Dragon platelegs (g) (HD: Dragon platelegs (or))
  - Dragon plateskirt (g) (HD: Dragon plateskirt (or))
  - Dragon full helm (g) (HD: Dragon full helm (or))
  - Amulet of fury (or)
  - Ancient platebody
  - Ancient platelegs
  - Ancient plateskirt
  - Ancient full helm
  - Ancient kiteshield
  - Armadyl platebody
  - Armadyl platelegs
  - Armadyl plateskirt
  - Armadyl full helm
  - Armadyl kiteshield
  - Bandos platebody
  - Bandos platelegs
  - Bandos plateskirt
  - Bandos full helm
  - Bandos kiteshield
  - Ancient bracers (HD: Ancient vambraces)
  - Ancient d'hide body (HD: Ancient body)
  - Ancient chaps
  - Ancient coif
  - Bandos bracers (HD: Bandos vambraces)
  - Bandos d'hide body (HD: Bandos body)
  - Bandos chaps
  - Bandos coif
  - Armadyl bracers (HD: Armadyl vambraces)
  - Armadyl d'hide body (HD: Armadyl body)
  - Armadyl chaps
  - Armadyl coif
  - Green dragon mask
  - Blue dragon mask
  - Red dragon mask
  - Black dragon mask
  - Fury ornament kit
  - Fury ornament kit
  - Dragon sq shield ornament kit (HD: Dragon sq shield ornament kit (or))
  - Dragon sq shield ornament kit (HD: Dragon sq shield ornament kit (or))
  - Dragon full helm ornament kit (HD: Dragon full helm ornament kit (or))
  - Dragon full helm ornament kit (HD: Dragon full helm ornament kit (or))
  - Druidic wreath (HD: Third-age druidic wreath)
  - Book of war
  - Book of law
  - Book of darkness (HD: Ancient book)
  - Bandos page 1
  - Bandos page 2
  - Bandos page 3
  - Bandos page 4
  - Armadyl page 1
  - Armadyl page 2
  - Armadyl page 3
  - Armadyl page 4
  - Ancient page 1
  - Ancient page 2
  - Ancient page 3
  - Ancient page 4
  - Armadyl rune armour set (lg) (HD: Armadyl armour set (lg))
  - Armadyl rune armour set (sk) (HD: Armadyl armour set (sk))
  - Bandos rune armour set (lg) (HD: Bandos armour set (lg))
  - Bandos rune armour set (sk) (HD: Bandos armour set (sk))
  - Ancient rune armour set (lg) (HD: Ancient armour set (lg))
  - Ancient rune armour set (sk) (HD: Ancient armour set (sk))
  - Armadyl rune armour set (lg) (HD: Armadyl armour set (lg))
  - Armadyl rune armour set (sk) (HD: Armadyl armour set (sk))
  - Bandos rune armour set (lg) (HD: Bandos armour set (lg))
  - Bandos rune armour set (sk) (HD: Bandos armour set (sk))
  - Ancient rune armour set (lg) (HD: Ancient armour set (lg))
  - Ancient rune armour set (sk) (HD: Ancient armour set (sk))
  - Book of war
  - Book of law
  - Book of darkness (HD: Ancient book)
  - Bandos page 1
  - Bandos page 2
  - Bandos page 3
  - Bandos page 4
  - Armadyl page 1
  - Armadyl page 2
  - Armadyl page 3
  - Armadyl page 4
  - Ancient page 1
  - Ancient page 2
  - Ancient page 3
  - Ancient page 4
  - Bandos platebody
  - Bandos platelegs
  - Bandos plateskirt
  - Bandos full helm
  - Bandos kiteshield
  - Armadyl platebody
  - Armadyl platelegs
  - Armadyl plateskirt
  - Armadyl full helm
  - Armadyl kiteshield
  - Ancient platebody
  - Ancient platelegs
  - Ancient plateskirt
  - Ancient full helm
  - Ancient kiteshield
  - Ancient robe top
  - Ancient robe legs
  - Ancient cloak
  - Ancient crozier
  - Ancient stole
  - Ancient mitre
  - Armadyl robe top
  - Armadyl robe legs
  - Armadyl stole
  - Armadyl mitre
  - Armadyl cloak
  - Armadyl crozier
  - Bandos robe top
  - Bandos robe legs
  - Bandos stole
  - Bandos mitre
  - Bandos cloak
  - Bandos crozier
  - Bronze dragon mask
  - Iron dragon mask
  - Steel dragon mask
  - Mithril dragon mask
  - Nardah teleport
  - Tai bwo wannai teleport
  - Dragon platelegs (g) (HD: Dragon platelegs (or))
  - Dragon plateskirt (g) (HD: Dragon plateskirt (or))
  - Dragon full helm (g) (HD: Dragon full helm (or))
  - Amulet of fury (or)
  - Ancient bracers (HD: Ancient vambraces)
  - Ancient d'hide body (HD: Ancient body)
  - Ancient chaps
  - Ancient coif
  - Bandos bracers (HD: Bandos vambraces)
  - Bandos d'hide body (HD: Bandos body)
  - Bandos chaps
  - Bandos coif
  - Armadyl bracers (HD: Armadyl vambraces)
  - Armadyl d'hide body (HD: Armadyl body)
  - Armadyl chaps
  - Armadyl coif
  - Green dragon mask
  - Blue dragon mask
  - Red dragon mask
  - Black dragon mask
  - Fury ornament kit
  - Fury ornament kit
  - Dragon sq shield ornament kit (HD: Dragon sq shield ornament kit (or))
  - Dragon sq shield ornament kit (HD: Dragon sq shield ornament kit (or))
  - Dragon full helm ornament kit (HD: Dragon full helm ornament kit (or))
  - Dragon full helm ornament kit (HD: Dragon full helm ornament kit (or))
  - 3rd age range top (HD: Third-age range top)
  - 3rd age range legs (HD: Third-age range legs)
  - 3rd age range coif (HD: Third-age range coif)
  - 3rd age vambraces (HD: Third-age vambraces)
  - 3rd age robe top (HD: Third-age robe top)
  - 3rd age robe (HD: Third-age robe)
  - 3rd age mage hat (HD: Third-age mage hat)
  - 3rd age amulet (HD: Third-age amulet)
  - 3rd age platelegs (HD: Third-age platelegs)
  - 3rd age platebody (HD: Third-age platebody)
  - 3rd age full helmet (HD: Third-age full helmet)
  - 3rd age kiteshield (HD: Third-age kiteshield)
  - Druidic wreath (HD: Third-age druidic wreath)
  - 3rd age robe top (HD: Third-age robe top)
  - 3rd age robe (HD: Third-age robe)
  - Dragon platebody ornament kit (HD: Dragon platebody ornament kit (or))
  - Dragon platebody ornament kit (HD: Dragon platebody ornament kit (or))
  - Dragon platebody ornament kit (HD: Dragon platebody ornament kit (or))
  - Dragon platebody ornament kit (HD: Dragon platebody ornament kit (or))
  - Dragon platebody (g) (HD: Dragon platebody (or))
  - Dragon platebody (g) (HD: Dragon platebody (or))
  - 3rd age druidic robe top (HD: Third-age druidic robe top)
  - 3rd age druidic robe top (HD: Third-age druidic robe top)
  - 3rd age druidic robe bottoms (HD: Third-age druidic robe)
  - 3rd age druidic robe bottoms (HD: Third-age druidic robe)
  - 3rd age druidic staff (HD: Third-age druidic staff)
  - 3rd age druidic staff (HD: Third-age druidic staff)
  - 3rd age druidic cloak (HD: Third-age druidic cloak)
  - 3rd age druidic cloak (HD: Third-age druidic cloak)
  - 3rd age mage hat (HD: Third-age mage hat)
  - 3rd age range top (HD: Third-age range top)
  - 3rd age range legs (HD: Third-age range legs)
  - 3rd age range coif (HD: Third-age range coif)
  </details>

## [279] 2022-9-16
### Changes
- #### HD Pack 3
  This version marks our third **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets the minigame **Guardians of the Rift** and introduces the following changes:

  - Remastered all objects and NPCs
  - Several ground textures have been added to the zone
  - Added immersive lighting, atmosphere and skybox
  - Added the ability for lighting to be dynamically turned on or off, depending on the state of the object (such as an orb spawning)
  - The Raiments of the Eye have been upgraded with the Great Orb Project runecrafting robes (fixes hdos/issues#1167)
    - A custom set has been created to reflect the red version of the old-school outfit
  - The Amulet of the Eye has been completely remastered and should no longer clip as much
  - The click radius and shadows of the entrance barrier have been improved
  - The lava texture is now bloomed (notable area is the fire altar)
  - Restored Runecrafting Altars
  - Restored Wizard's Tower
  - Remastered the Blood Altar
  - Other notably affected models:
      ```
      - Boots of the Eye (use the Infinity Boots HD rig)
      - Abyssal Lantern
      - Abyssal Protector (pet)
      - Greatish Guardian (pet)
      - Rift Guardian (pet)
      ```
  - Note: *Fixes for the Lost bag should be included in the next update*
- Restored Teleblock (and no longer shoots a tornado) (fixes hdos/issues#542)
- Restored Kree'arra's tornado
- Restored the ground/inventory model of the Imbued Slayer Helmet
- Improved the textures on the Ava's Assembler
- Restored the newer versions of the Spirit Shields
- Several improvements have been made to Draynor (fixes hdos/issues#547)
  - Improve tile heights
  - Fix the dungeon entrances
  - Restore the hay roofing
- Removed Catacombs from map filtering for now
- Improve Motherlode Mine and Kourend Catacombs sky
- Quick settings is now docked by default
### Added
- Idle Logout Plugin
### Fixed
- Improve performance for the new animations in raids3
- Some unintended upgrades of bushes were reverted
- Some textures now emmit their light even when bloom is disabled


## [278] 2022-9-10
### Fixed
- Fixed GWD doors (fixes hdos/issues#1005)

## [277] 2022-9-9
### Fixed
- Improve the new animations in raids 3 (no loading times, less cpu, less memory, and improved tweening)
- Re-deploy audio update from 276, revert TOA audio for now
- Fixed rotated drawer in Ardougne
### Changes
- Restored beekeeper

## [276] 2022-9-8
### Changes
- Update protocol to 208
- Restored waterfiends
- Restored many small people
### Fixed
- Nature's Grotto exit fixed
- Gargoyles now display the defend animation correctly (fixes hdos/issues#786)
- Part of the wall east of the Grand Exchange should no longer disappear when roofs are hidden
- Combat dummies have sprouted from the patches of dirt in the POH (fixes hdos/issues#1276)
#### Audio (thanks to `@RS Orchestra#9880`)
- #### Fixes
  - Fixed an issue in one of the Bechstein 280 Piano samples (Patch 640 - Bank 5, Preset 0)
- #### Changed
  - Replaced the Staccato Opera Voice Oh samples with higher quality (Patch 257 - Bank 2, Preset 1)
  - Replaced the Sustained Opera Voice Oh samples with higher quality (Patch 258 - Bank 2, Preset 2)
- #### Added
  - Added Realistic Church Organ to Patch 659 (Bank 5, Preset 19)
  - Added Realistic Tremolo Strings to Patch 684 (Bank 5, Preset 44)
  - Added Realistic Orchestra Hits to Patch 695 (Bank 5, Preset 55)
  - Added Dubstep Wobble Synth to Patch 735 (Bank 5, Preset 95)
- #### Songs
  - Start (HD)
  - A Taste of Hope (Orchestral Remix)
  - Alchemical Attack! (Remix)
  - Rest In Peace (Death)
  - The Angel's Fury (Angel of Death, Remix)
  - Tombs of Amascut Jingles upgrade to HD sounds
  - Tombs of Amascut OST upgraded to HD soundset

## [275] 2022-9-5
### Fixed
- Fix Port Phasmatys restore

## [274] 2022-9-5
### Changes
- Restored Morytania (fixes hdos/issues#404 hdos/issues#490 hdos/issues#736 hdos/issues#867 hdos/issues#896 hdos/issues#1280)
  - the following zones have been affected:
    ```
    - Silverea
    - Paterdomus Temple (overworld)
    - Canifis
    - Slayer Tower (No remaster)
    - Mausoleum
    - Fenkenstrain's Castle
    - Experiments Dungeon
    - Ectofungus, Farm
    - Port Phasmatys
    - Haunted Woods
    - River opening above Slepe
    - Mort Myre Swamp, Nature's Grotto, The Hollows
    - Barrows, Barrows Crypts
    - Burgh de Rott
    - Haunted Mine
    - Tarns Lair
    - Myreque Hideout, Tomb of Ivandis, Fairy ring
    ```
  - Related changes:
    ```
    - Remastered the Morytania poll booth
    - Children in Burgh de Rott have been restored
    - Restored the Ectophial teleport
    - Remastered barrows heads inside the crypt (for the moment)
    ```
- Improved the texture projection on the regular poll booth
- Remastered clue stashes
- Restored 181 gnomes (fixes hdos/issues#961)
### Fixed
- Add support for the new skeletal system (Beta)
- Increase point light limit from 256 lights to 1024 lights to scale with the extended render distance
- Actor and projectile rotations and movements are now more smoother (removed micro-shaking)
- Fixed the flickering rates on some types point lights
- Fix notification popup rendering (fixes hdos/issues#1187)

## [273] 2022-8-26
### Fixed
- Fix animation stalling (fixes hdos/issues#1292)
- Fix raids 3 black underlays

## [272] 2022-8-25
### Fixed
- Angels in the sky mistaking the Torva full helm chathead for a skybox no longer make the client crash (fixes hdos/issues#1289)
- Fixed an issue where the cache mistakenly exceeded a limit. Booting the client should once again be as fast as before
- Raids 3 no longer suffers crashes from the strange new energy emitted from the menaphite pantheon (they instead will display their power via t-poses for now)
- Add support for 32 bit file identifiers within the cache
### Changes
- Improve the atmosphere for raids3 (until it gets remastered)

## [271] 2022-8-24
### Fixed
- Fix custom assets

## [270] 2022-8-24
### Changes
- Cold update the client
### Fixed
- Fix Paterdormus renovation

## [269] 2022-8-22
### Fixed
- Paterdormus' library has been renovated, and is no longer cursed when hide roofs is toggled off (fixes hdos/issues#1277)
- You can no longer sink through Canifis' rooftop agility course, and should once again be functional (fixes hdos/issues#1281)
### Changes
- The ring of the elements now uses HD teleport animations (fixes hdos/issues#1136)
  - Creating custom colours for each location is on the to-do list, but not a priority right now as it requires engine work
- Paterdormus' library atmosphere has been improved
- All variants of the Soul Wars capes have been restored
- Restored Nomad and Zimberfizz
- #### HD Pack 2
  This version marks our second **HD PACK**. HD Packs aim to upgrade a group of OS models with custom assets, so they align with the HD art style.

  This HD Pack targets **Ungael** and the boss **Vorkath** and introduces the following changes:

  - Added lighting and immersive atmospheres to Ungael
  - Remastered the objects on the island
  - Vorkath and its minions have been remastered
  - Several Vorkath projectiles have been remastered or restored
  - Added shorelines around the island
  - Climbing rocks on the island now performs an improved HD animation
  - Torfinn has been upgraded
  - Other notably affected items:
      ```
      - Ava's Assembler and Vorkath's Head
      - Dragon Crossbow
      - Mythical Cape
      - Dragonfire Ward and Skeletal Visage
      - Wrath Rune and Talisman
      - Superior Dragon Bones
      ```


## [268] - 2022-8-15
### Fixed
- Fixed black artifacts (grass and flags) in M1 GPUs
- Fixed long loading times when entering/exiting full screen on MacOS
- Fixed flicking issues on some UIs when UI Batching is enabled in MacOS (fixes hdos/issues#1275)

## [267] - 2022-8-10
### Added
- Add Attack Styles plugin
### Fixed
- Fixed the remastered tinted hit splats
- Refine extended npc
### Changes
- Restored Varrock (fixes hdos/issues#642 hdos/issues#796 hdos/issues#868)
- Restored Port Khazard
- Increased the contrast between normal and tinted hitsplats described above to improve differentiation between the two
- Remastered the max-hitsplat (adapted the 2011 style to match 2009 style)

## [266] - 2022-8-4
### Fixed
- Fixed downgrades from this weeks game update
- Fixed a gap in the beach by the east coast of yanille
- Fixed rotated RSC fence texture (fixes hdos/issues#1228)
- Fixed an issue where the client would stop allocating more VRAM above a certain threshold (high stress on the client no longer causes it to crash unexpectedly)
### Changes
- Restored the Imbued slayer helmet with the Full slayer helmet
- Restored mining rocks globally (some rocks will look off, this is intentional until those areas get restored by hand)
- Restored dragons
- Restored bears
- Truly restored the dragon pickaxe (animations and inventory icon)
- Improve texture projection for common trees
- The gem rocks in Shilo Village no longer turn brown when depleted
- The depleted version of the infernal axe is now mapped to the inferno adze
#### Audio (thanks to `@RS Orchestra#9880`)
- Reverted the Celesta instrument patch back to fix issues with several tracks
- Added Bechstein 280 Piano to Patch 640 (Bank 5, Preset 0)
- Song of the Elves (Reorchestrated Remix)

## [265] - 2022-7-28
### Changes
- Restored Yanille (fixes hdos/issues#276 hdos/issues#363 hdos/issues#592 hdos/issues#1142)
- Restored the Lighthouse and Lighthouse Dungeon (fixes hdos/issues#1042)
- Restored Fight Arena

## [264] - 2022-7-14
### Changes
- Restored Entrana (and its dungeon)

## [263] - 2022-7-14
### Changes
- Update protocol to 207
### Fixed
- Key remapping: disable remapping when inputs are present (fixes hdos/issues#1232)

## [262] - 2022-7-14
### Fixed
- Remove WIP plugins

## [261] - 2022-7-14
### Changes
- Restored the floor, and lighting in the Chaos Tunnel
- Restore Dragonbone necklace
### Fixed
- Fix the "Walk here" option when used from the right click menu
- The chat lock now automatically re-locks once a message is typed (fixes hdos/issues#1222)
- Box traps now look correct in the inventory (fixes hdos/issues#589)
- Improve region filtering and extended npcs (fixes hdos/issues#1224)
- Fix destination tile from appearing far away (fixes hdos/issues#1204)
#### Plugins
- Added: Key Remapping
- Camera: Move key remapping to the new Key Remapping plugin, and allow for customizable keys
- Tile Marker: Add an option to mark the menu destination
- Tile Marker: Destination tile now has a smooth transition
- FPS Debug: Rename to just FPS, and allow the FPS meter to be adjustable
- MES: Fixed swap from duel arena to pvp arena
### Launcher (v8)
- Add DLL Blocking (thanks to RL/Adam for the help)
- Windows launchers: fix window snapping, and black borders
- Remove proxy request on networking errors

## [260] - 2022-7-6
### Fixed
- Fix item downgrades due to the latest game update

## [259] - 2022-7-6
### Added
- Add toggle for UI batch rendering to offset negated performance on some specific GPUs (ex: ivybridge)

## [258] - 2022-7-5
### Fixed
- Fix login screen freezing (tm)
- The tunnels in Meiyerditch have been corrected (fixes hdos/issues#1155)  
- The Vyrewatch have regained their flexibility and no longer T-pose(fixes hdos/issues#1154)  
- The reclaim pet interface is now fully functional (fixes hdos/issues#1119)
#### Plugins
- Added an option to draw the true tile, even if the render position matches the server position (fixes hdos/issues#1168)

## [257] - 2022-7-5
### Fixed
- Fix known crashes
- Fix lobby screen freezing
- Fix lightbox solution rendering
- Fix ground item dots not rendering on the minimap (fixes hdos/issues#1176)
- Improve region filtering and extended npc wandering ranges

## [256] - 2022-7-2
### Changes
- Improve title screen npc movement
### Fixed
- Fixed UI rendering for older GPUs
- Fixed object examine

## [255] - 2022-7-1
### Changes
- Refine settings names, descriptors, and search tags
### Fixed
- Fix known crashes
- Fix broken context cursors
- Fix the welcome message within the lobby (spinning runes)
- Improve click boxes (fixes hdos/issues#923 hdos/issues#1103 hdos/issues#1106)
- Npc interaction now works when character shadows are disabled (fixes hdos/issues#1145)
- The door to COX now has a full size clickbox  (fixes hdos/issues#683)
- Region filtering: fix known areas that were being cutoff
- Quick settings no longer flashes when hopping in fixed mode, or closing the main settings window (if it was docked)
- Fix pixel artifacts that would accumulate in fixed mode
- Fix actor rotation (fixes hdos/issues#1089 hdos/issues#1112 hdos/issues#1135)
#### Plugins
- Ground markers: now work in instances (fixes hdos/issues#1146)
- Idle notifier: fix known bugs (fixes hdos/issues#923)
- Discord: the plugin now works as expected in f2p
- Discord: Zezima is not longer the default display name within the discord presence plugin
- Agility: All obstacles should now have their clickbox rendering properly (fixes hdos/issues#1049)
- Anti-drag: fix the shift key modifier
- Tile indicator: fixed an issue where you could not use both hover tile, and true tile at the same time (fixes hdos/issues#1156)
- Player indicator: no longer marks your own player

## [254] - 2022-6-29
### Added
- Ground markers
- Discord presence
- True tile markers are now available in the "Tile markers" plugin.
- Idle notifier
- Metronome plugin
### Fixed
- Fixed known crashed

## [253] - 2022-6-29
### Fixed
- Fix crashing when changing AA

## [252] - 2022-6-29
### Added
- Extended render distance
- Seamless region loading
- Region filtering (Beta)
- Extended npcs (Beta)
- Add a `Support` link to the Help menu on the top menu bar
### Changes
- Improve performance (2x peak performance in some cases)
- Restore The Warriors' guild 
- Restore The Crafting guild and Make over Mage buildings (fixes hdos/issues#1090)
- Restore Falador church (fixes hdos/issues#373)
- Restore Falador farm
- Restore various npcs, such as Mutated bloodvelds
### Fixed
- (Audio) `The Guardians Prepare`: Fixed the sustain control so notes don't drag on for a long time
- Camera shake speed (fixes hdos/issues#1026)
- Middle mouse expected behavior on components (fixes hdos/issues#1134)
- Fix the camera plugin from disabling
- Fixed the orange Ket-Zek (fixes hdos/issues#660)

## [251] - 2022-6-23
### Fixes
- Fix `OutOfMemoryError` (:smiwe:)

## [250] - 2022-6-22
### Changes
- Anti-Drag now only applies to items
### Fixes
- Fix known crashes
- Create a fallback for "sync error" faults
- Fix internal GL error notification
- Fix HD chatheads

## [249] - 2022-6-22
### Changed
- Removed the "quest helper" joke plugin
### Fixed
- Update broadcasting now clearly states it's from HDOS
- Added the ability to change your height while using freecam / occulus orb
- The default anti-drag settings have been corrected

## [248] - 2022-6-21
### Changed
- Added update broadcasting
### Fixed
- Antidrag plugin

## [247] - 2022-6-21
### Fixed
- Fixed Item switching when changing tabs (Fixes g-maul switches)

## [246] - 2022-6-21
### Fixed
- Fix interaction when using UI scaling

## [245] - 2022-6-21
### Changed
- Improve UI scaling (fixes hdos/issues#876)
- Add a Quick Settings toggle to the top Toolbar
### Fixed
- Fixed clan members not updating when they join/leave
- Fix known crashes
- Fix MES (fixes hdos/issues#1104)
- Quick Settings now opens properly when in fixed mode, with the sidebar closed
- WASD camera no longer triggers when searching within Quick Settings
- Fix the bank search from closing when Quick Settings search is active

## [244] - 2022-6-17
### Fixed
- Fix known crashes

## [243] - 2022-6-17
### Fixes
- The bank no longer closes when closing Quick Settings in the middle of searching
- Improve interaction detection for large entities (fixes hdos/issues#1041)

## [242] - 2022-6-15
### Fixed
- Update protocol to 206

#### Plugins
- Fixed a crash with the occulus orb plugin

## [241] - 2022-6-09
### Fixes
- Add numpad keys to bank pin plugin (fixes hdos/issues#1062)
- Fix main settings window not opening after world hop (fixes hdos/issues#1055)
- Fix "Use" MES swaps (fixes hdos/issues#1060)
- The bank no longer closes when searching after using Quick Settings search
- Fix inaccurate actor rotations (fixes hdos/issues#1059)
- Fix invisible balloons in the prison pete random event
### Changes
- Restore `Elite Void` and `Amulet of fury (or)`
- Fix atmosphere for the `Giants' Foundry`
- Quick Settings search can now be closed using the `Tab` key
#### Audio Improvements (thanks to `@RS Orchestra#9880`)
- Polished up several instruments: Patches 0-9, 44-49, and 60.
- Added realistic Timpani (Patch 687) and French Horns (Patch 700) to Bank 5.
- The following audio have been remastered:
  - Guardians of the Rift - Failure (Jingle)
  - Guardians of the Rift - Success (Jingle)
  - Sleeping Giants - Quest Complete (Jingle)
  - Delrith
  - Wally the Hero
  - Cain's Tutorial
  - Penguin Plots
  - Sorceress's Garden
  - Dogfight
  - Brain Battle
  - Awful Anthem
  - Crest of a Wave
  - Eye of the Storm
  - The Sinclairs
  - The Adventurer (Unlisted)
  - The Foundry
  - Drunken Dwarf
  - Mined Out
  - The Guardians Prepare
  - Temple of the Eye
  - Guardians of the Rift
  - Venomous
  - Dunes of Eternity
  - The Pharaoh
  - The Forgotten Tomb
  - Ruins of Isolation
  - Thrall of the Devourer
  - The Gates of Menaphos
  - The Spymaster

## [240] - 2022-5-14
### Fixed
- Fix invisible NPCs
- Fix rendering artifacts for Macbooks running integrated graphics.
- Partial fix for a very random and strange bug in newer AMD cards (ex: RX 590, RX 5600). 
  Restarting multiple times will eventually work.

## [239] - 2022-5-12
### Fixed
- Update protocol to 205

# [238] - 2022-5-10
### Fixed
- Fixed crashing

## [237] - 2022-5-10
### Changes
- Refactor plugin backend (fixes misc. quirks)
### Added
- Add Anisotropic Filtering (Display settings)
- Bank pin plugin
- Restore LMS Dragon Defender
### Fixed
- Fixed fullscreen on macOS
- Fixed misc. crashes

## [236] - 2022-4-28
### Fixed
- Actually fix the max cape `:^)`

## [235] - 2022-4-28
### Fixed
- Fixed known crashes
- Fix max cape restore

## [234] - 2022-4-28
**PLUGINS BATCH 1**  
Plugins voted for in batch 1 have been added
- All plugins and settings are now accessed through the _In Game Settings_
- The `Quick Settings` window allows you to edit plugins settings while in combat, it is access via _Double tapping shift_
- Plugin overlays (including the `Quick Settings`) can be resize, dragged and dropped to and from the sidebar.
### Changed
- Migrate and upgrade all existing plugins to be inside the game
- `Rough Walls` are now easier to click
- Remaster the title screen background (static image)
- The default FPS for the client is now set to 500 (from 100)
#### Restored
- Restored/Backported:
- Soul wars cape
- Dragon defender
- Ardougne cloak 1-4
- Falador shield 1-4
- Fremennik sea boots 1-4
- Karamja gloves 4
- Explorer's ring 1-4
- Varrock armour 1-4
- Max Cape (Completionist cape)
- Ancient teleport
- Lunar Teleport
- Superheat Item spell
- Low and High alch animation
### Added
- **The graphics settings within the login screen now exist within the game settings under the `Display` tab**
#### Plugins
  - Boosts Information
  - Clue Scroll Helper
  - Ground Items
  - Loot Tracker
  - Menu Entry Swapper
  - Puzzle Solver
  - Green Screen
  - Quick Settings
  - Tile Indicator
  - Entity Hider
  - Anti Drag
  - Player Indicators
  - Door Kicker
  - Agility
  - Quest Helper
  - FPS Debug
  - Transmog
  - Skybox
  - Camera
  - Slowmo
### Fixed
- Right-Click menu `Walk-here`
- The `Esc` key for the closing the Settings UI
- Fix the `Hide Roofs` setting
- Player notifications messages (logged in/out) now clear after 10 seconds
- Improve safe mode (disables settings if it detects the client failed to boot)
- Fix a long standing bug randomly causing users to no longer be able to type
- Fix Cerberus rotation bug
- Fix world map spin rate and minimap hint arrows
- Fix the bank and other UIs when the client is down-clocked (< 50 FPS target)
- Improve min size and UI scaling limits
- Fortify update detection (cold updates)
#### Graphical Fixes
  - Fix Fog depth
  - Fix particles sizes when using x8 and x16 AA
  - Fix particles sizes when using UI scaling
  - Fix a bug on some GPUs that caused the water to turn green
  - Fix the quest tab icon from overflowing
  - Fix player shadows from rendering over players
  - Fix cannon shadow flickering
  - Improve rendering performance for dynamic shadows
  - Fix player rotation shaking (ex: dancing)


## [233] - 2022-4-27
### Fixed
- Fix the water

## [232] - 2022-4-27
### Fixed
- Partial fix for the Beneath Cursed Sands game update.  
  Beneath Cursed Sands might cause crashes -- full fix will be out in a few hours

## [231] - 2022-4-22
### Fixed
- Fix interaction breaking from the recent cold fix
- Fix broken chatheads :^)

## [230] - 2022-4-20
### Fixed
- Fix alching
- Fix login screen background

## [229] - 2022-4-20
### Fixed
- Fix inventory (fixes hdos/issues#1043)

## [228] - 2022-4-10
### Fixed
- Fix crashing at GOTR (fixes hdos/issues#1036)

## [227] - 2022-3-30
### Added
- Remaster the `Abyssal whip (or)` from Leagues3 (Thanks to `@Maximax#1897`)
### Fixed
- Fix particle rendering when UI scaling is active

## [226] - 2022-3-23
### Changed
- Update protocol to 204

## [225] - 2022-3-8
### Fixed
- Fix ChatHeads that were downgraded in the latest update
### Changed
- Remastered the title screen (login) sprites (thanks to `@DemHunter#7191`)
  - Improved image quality, and cleaning up aliasing and artifacts from compression.
- Added Staccato Strings to Bank 5
- Added Custom "TzHaar" Drums to Bank 6
- The following music tracks have been restored/remixed (thanks to `@RS Orchestra#9880`)
    - Adventure
    - Assault and Battery
    - Attack 1
    - Attack 5
    - Attention
    - Bane (Bane of Summer)
    - Baroque
    - Cave of Beasts
    - Clan Wars
    - Distant Land
    - Easter Jig
    - Emperor
    - Expanse
    - Expecting
    - Fanfare
    - Gnome Village
    - H.A.M. Attack
    - Have an Ice Day
    - Impetuous
    - Inadequacy
    - Inferno (Remix)
    - Itsy Bitsy...
    - Jaws of the Basilisk (Jaws of the Dagannoth)
    - Lair of the Basilisk (Lair of Kang Admi)
    - Long Ago
    - Newbie Melody
    - Pirates of Penance
    - Rune Essence
    - Scape Santa
    - Scape Soft
    - Shining Spirit
    - Showdown
    - Sojourn
    - Soundscape
    - Splendour
    - Tears of Guthix
    - The Power of Tears
    - Theme
    - Tomorrow
    - Too Many Cooks...
    - Tremble
    - Undead Dungeon
    - Upcoming
    - Venture 2
    - Wander


## [224] - 2022-2-27
### Fixed
- Fix occasional JVM crash before the crash screen opens (oh my)
- HD songs should no longer be abruptly stopped before they finish (fixes hdos/issues#1006)
- Fix known crashes
### Changed
- The following music tracks have been restored/remastered (thanks to `@RS Orchestra#9880`)
    - Arboretum
    - Bait (Conspiracy: Part 2)
    - Bloodbath
    - Catacombs and Tombs
    - Conspiracy (Conspiracy: Part 1)
    - Darkmeyer
    - Don't Panic Zanik
    - Duel Arena
    - Dusk in Yu'biusk
    - Horizon
    - Leagues 3 Intro Cutscene
    - Rising Damp
    - Soul Wars
    - Temple of Tribes
    - The Enclave (Command Centre)
    - The Waiting Game
    - Upir Likhyi (Remix)
    - Vanescula (Vanescula Fight)
    - Zanik's Theme


## [223] - 2022-2-23
### Fixed
- Launcher: lower max heap size within the jar launcher for 32 bit operating systems 
  to help mitigate a silent GL_OUT_OF_MEMORY error, which would cause black screens.
- Fix assets that were downgraded from the previous updates (ex: Goblins, Bob)
- Fix overhead clipping issues when in fixed mode
- At long last: Overhead bars no longer randomly draw in the top left corner!

## [222] - 2022-2-13
### Fixed
- fix floor underlays textures that became hard to see (ex: tzhaar)
- fix shader compilation crashing in some specific cards
- refine pallet correction fix from the v220
- improve rendering for item piles that are on top of objects

## [221] - 2022-2-9
### Fixed
- fix crashing due to new content

## [220] - 2022-2-9
### Added
- **Nex has been restored!**
- **Added particle support!**
- **Added billboard support!**
- Restored directional sound
- Restored the "Blow Kiss" animation
- Restored Halos
### Fixed
- **Animations are now smoother!** (fix timing bug that was causing micro-stuttering in-between frames)
- Animations no longer stall in the last keyframe before looping
- Fix chat heads
- Fix up misc. assets in GWD
- The mouse cross now appears when using items and spells on objects
- Player and npc shadows now render on the correct floor when on bridges and overpasses
- Shadows for large npcs now properly animate
- Shadows no longer clip into the terrain and objects on the floor
- Improve the render order for ground items so its more aligned with the OS behavior
- Health bars now fade at the proper rate
- Fixed an apparent bug in the model color pallet which removed a range of colors.
  Most notable color being black, which has been turning into gray
- Fix the oil spill tainting some waters in Gielinor
### Changed
- Update protocol to 203
- Misc. textures have been tweaked slightly
- Santa's imps have gone back home
- Flying (ex: knock back) is now smoother
- Reduced the volume for the bird flapping animations
- Improve region loading performance

- #### Full Music Restore!

    Thanks to `@RS Orchestra#9880` the music restore is now fully completed!  
    We were missing some tracks, jingles, and patches before, but thanks to
    his _Orchestration_, it's now completed in all of its glory!  
    Many tracks and audio may sound higher quality and richer.  
    Audio is one of those details that really helps bring everything together and gives a more fuller experience.  
    Some tracks he personally touched up on:  
    - Arboretum
    - Armadyl Alliance
    - Armageddon
    - Autumn Voyage
    - Bandos Battalion
    - Barb Wire
    - Bloodbath
    - Castle Wars
    - Coil (Remixed)
    - Darkmeyer
    - Dream
    - Garden
    - Harmony
    - Iban
    - Ice and Fire (Remixed)
    - Medieval
    - Ready for Battle
    - Scape Main
    - Shine
    - Soul Wars
    - Still Night
    - Strength of Saradomin
    - The Ancient Prison (Zaros Stirs)
    - The Enclave (Command Centre)
    - The Waiting Game
    - Upir Likhyi (Remix)
    - Vanescula (Vanescula Fight)
    - Zamorak Zoo
    - Zaros Zeitgeist



## [219] - 2022-1-5
### Fixed
- fix known crashes
- fix drag delay UI for mac

## [218] - 2021-12-30
### Fixed
- fix gim chat mode
- Drag delay plugin (the higher the delay, the sooner it's considered not dragging)

## [217] - 2021-12-28
### Fixed
- fix mac full screen and resizing
- fix mac toolbar UI
- fix known crashes

## [216] - 2021-12-26
### Changed
- refine the crash screen
- set the launcher to validate resources on boot
### Fixed
- disable full screen on mac, use undecorated window frames to help fix resizing problems.
- the game engine will attempt to fully shutdown on crash

## [215] - 2021-12-26
### Added
- gl internal error reporting
### Fixed
- improve performance on macos
- fix actor shadows and overheads from disappearing when the right click menu is open

## [214] - 2021-12-26
### Fixed
- fix idle logout
- improve compatibility with older graphics cards

## [213] - 2021-12-25
### Changed
- updated crash screen
### Fixed
- fix recent crashing on macs

## [212] - 2021-12-25
### Added
- add `::uiscale` command (ex: `::uiscale 100` == 100% (normal))
### Fixed
- fix resizing issues on mac
- fix music fading between tracks
- reduce memory usage and improve startup performance

## [211] - 2021-12-24
### Changed
- upgrade LWJGL to 3.3.0
- reduce the minimal GL requirement to 3.1 to support older cards
### Fixed
- add support for macOS/arm64
- fix crashes and bugs

## [210] - 2021-12-24
### Fixed
- improve compatibility with older graphics cards
- fix crashes

## [209] - 2021-12-23
### Fixed
- fix 32 bit support 
- improve compatibility with older graphics cards

## [208] - 2021-12-23
### Fixed
- remove verbose debug

## [207] - 2021-12-23
### Changed
- extend click distance
- `::slowmo` updated to go even more slow, moe.
### Fixed
- fix 2fa login timeout
- fix keyboard rotation rates at lower fps targets
- fix animations at lower at lower fps targets
- fix rotation shaking at higher fps targets
- improve compatibility with older graphics cards
- crash fixes

## [206] - 2021-12-22
### Changed
**NEW CLIENT**
- rewrote the old fixed function rendering pipeline to use GL3.2 core
- unlocked the clients tick rate, and frame rate
- added multi-threading
- improve performance by orders of magnitude
- migrate from JOGL to LWJGL

## [205] - 2021-12-16
### Changed
- Fix camera animation
- Fix random lag spikes around populated areas
- Fix downgrades from latest update
- Touchup Falador a bit

## [204] - 2021-12-9
### Changed
- Update protocol to 202

## [203] - 2021-11-3
### Changed
- Update protocol to 201

## [202] - 2021-10-26
### Fixed
- Client reconnection no longer kicks you to the login screen

## [201] - 2021-10-25
### Fixed
- The in-game brightness setting is no longer backwards
- Steam plugins no longer appear
- Searching in the settings windows has been fixed/improved
- Fixed chatheads and other downgrades due to the last cache update
- Fix GIM chat icons

## [200] - 2021-10-6
### Fixed
- GIM clans now work
- HD Chatheads that were downgraded have been fixed

## [199] - 2021-10-6
### Changed
- Update protocol to 200
### Fixed
- HD Chatbox
- Misc. crashes

## [198] - 2021-8-25
### Fixed
- Chat messages now work as expected
- Fix tutorial island crashing

## [197] - 2021-8-25
### Changed
- Update protocol to 199

## [196] - 2021-7-28
### Changed
- Update protocol to 198
### Fixed
- Fixed the troll npc animations within the Pinball random event (fixes hdos/issues#954)
- Address downgraded assets from recent cache updates
- Fix brightness in SD mode

## [195] - 2021-7-1
### Changed
- Restored Shilo Village (fixes hdos/issues#930)
- Restored Law Altar (fixes hdos/issues#908)
- ```::dark``` now has a second parameter (1 to 100) to adjust the brightness ex.(::dark 75) (fixes hdos/issues#875)
- The eat animation has been restored (fixes hdos/issues#639)
- The Graceful top and bottom are now using the Agile models (rest soon to be done) (fixes hdos/issues#936)
- The infernal axe is now represented by the inferno adze (fixes hdos/issues#929)
### Fixed
- Fix the camera zoom when in HD mode (fixes hdos/issues#163 hdos/issues#537 hdos/issues#640 hdos/issues#795 hdos/issues#801 hdos/issues#818)
- Thordur's Blackholes are darker than ever (fixes hdos/issues#880 hdos/issues#155)
- Message's first letter is now capitalized (fixes hdos/issues#835)
- Goblins in Lumbridge basement now are longer on hover boards (fixes hdos/issues#806)
- The wheat field in Catherby has been fixed up (fixes hdos/issues#862)
- Watered seedlings/saplings should now have the correct item sprite (fixes hdos/issues#860)
- Fluffy's Kitten finally decided to stop hiding inside a book (fixes hdos/issues#622 hdos/issues#919)
- Overhead messages now last the standard time before vanishing (fixes hdos/issues#940)
- Fix misc. crashes
- Fixed all issues at tempeross (fixes hdos/issues#931 hdos/issues#927)
- Bandage tables are now back in soul wars (fixes hdos/issues#838 hdos/issues#665)
- Repaired the signs outside seers (fixes hdos/issues#808)
- Removed random light from man house in Edgeville (fixes hdos/issues#878)
- Fix audio and brightness settings (fixes hdos/issues#950)

## [194] - 2021-6-23
### Fixed
- Fix the chatbox (fixes hdos/issues#942)
- Fix the chatbox color settings

## [193] - 2021-6-23
### Fixed
- Fixed some checker flooring (fixes hdos/issues#928)
- Fixed gargoyle animations (untextured death is intended for now) (fixes hdos/issues#786)
- Fix the chatbox

## [192] - 2021-6-16
### Changed
- Update protocol to 197
- Revert the pickaxe models back the OS style
- Upgraded the orange `Ket-Zek` in the fight caves (fixes hdos/issues#660)
- Upgraded health bars to HD (fixes hdos/issues#320 hdos/issues#788)
- Improved texture upscaling slightly
- Improved bloom on the `Twisted Bow`
### Fixed
- Excluded the `Basilisk Knight` attack from tweening and should no longer twirl (fixes hdos/issues#184)
- Excluded the `Mogre` death animation from tweening (fixes hdos/issues#299)
- Various NPCs can no longer go stiff (fixes hdos/issues#587 hdos/issues#700)

## [191] - 2021-6-11
### Changed
- Update the LAF (fixes hdos/issues#419 hdos/issues#620)
- Restore the `Air Guitar` animation (fixes hdos/issues#918)
- The `Verac's flail` idle animation now tweens
- Upgrade the `Viggora' chainmace` to the `Verac's flail` (excluding attack) (fixes hdos/issues#748)
- Upgrade the `Agility skillcape` (fixes hdos/issues#395 hdos/issues#907)
### Fixed
- Fix a bug where the buggy sacks near buggie in the kalphite cave would display as a floating tile (fixes hdos/issues#376)
- The motherlode mine no longer makes the dragon pickaxe cower in fear (fixes hdos/issues#841 hdos/issues#828 hdos/issues#787 hdos/issues#489)
- Performing the spin emote will no longer haunt you with the crafting context cursor (fixes hdos/issues#785)
- Spells/Item now cancel when you click on a tile (fixes hdos/issues#871)
- Fix an oversight where unpowered orbs would have the same model colour as air orbs ([this was fixed in 2009](https://runescape.wiki/w/Update:Patch_Notes_(6_May_2009))) 

## [190] - 2021-6-5
### Changed
- Revert the `Scythe of vitur` attack animation
- Taking fish from a barrel in piscarilius will no longer atomically rearrange that barrel
### Fixed
- Fixed misc crashes
- The client can now enter HD mode when using Java 16
- Improve highDPI scaling

## [189] - 2021-6-2
### Added
- 2FA will now ask if you want to authorize for 30 days or one time
### Fixed
- Chatting within clans now works (fixes hdos/issues#897)
- The favorite world now applies correctly (fixes hdos/issues#826)

## [188] - 2021-6-2
### Changed
- The `F-Keys`, `Esc`, and `Space` keys can be used when the chat lock is enabled in the WASD Plugin
- Upgraded the new clan icons in the UI
    - Thanks to `@Adam#9751` for making them, and `@Zukitwo#3440` for testing them!
### Fixed
- Implement the new clan system (fixes hdos/issues#820 hdos/issues#834)
- Improve dpi-scaling for high resolution displays
- Camera zoom now persists (fixes hdos/issues#863)
- Resolve a bug that would corrupt the jagex_cl.dat files
- Players can now finally escape Petes Prison! (fixes hdos/issues#181)
    - Thanks to `@Im2Hi2Play#7412` for letting us use his account!
- Remove the gap at the bottom of the context menu
- Father Aereck and Donie have switched back to their correct bodies

## [187] - 2021-5-28
### Fixed
- Fixed interaction problems due to the recent update

## [186] - 2021-5-28
### Changed
__***!!!OPEN BETA!!!***__

## [185] - 2021-5-27
### Added
- UI Scaling
### Changed
- Upgrade `Poll Booth` models
- Scuffed alters have been restored
- The first favorited world now defaults selected on launch.
    - pvp worlds will are ignored
    - worlds over 512 won't work due to script limitations
### Fixed
- Plugin: Player Markers plugin's clan logic has been fixed
- Plugin: Player Markers settings will now save
- Plugin: Ground Markers will now account for items that are on top of objects
- Plugin: Tile Hover and Tile Click no longer randomly hide when in GL mode 
- The scroll texture is no longer upscaled in interfaces
- HP bars no longer red bar when they are low
- Roofing in the barrows chambers has been fixed
- Dead clicks no longer occur when the Tile Hover plugin is enable
- Possible fix for freezing in linux
- Fixed the path between Lumbridge and Varrock

## [184] - 2021-5-19
### Changed
- Update protocol to 196
- Improved "Player Marker" plugin, settings may now change independently

## [183] - 2021-5-17
### Removed
- Removed the `Allocating Memory` loading step (should make startup times faster)
- Removed the `Idle Logout` feature
- Removed the `Unsafe Mode` feature
### Fixed
- The ground no longer cracks when you use the Dragon Warhammer spec
- Fixed chatheads and other downgrades due to the last cache update
### Changed
- Organized the menu bar UI
- Hiding interfaces now applies to the login screen (Useful for recording the background)
- Replace the command prefix with `::` instead of `:` (ex: `::issue` instead of `:issue`)
### Added
- The following plugins have been added:

    | Plugin | Description |
    |------|-------|
    | Tile Hover | Draws the tile the mouse is hovering |
    | Tile Click | Draws the tile your player is moving to |
    | Ground Marker | Draws information about items that are on the ground |
    | Player Marker | Draws information about players |


- Plugins now save their settings

## [182] - 2021-4-26
### Fixed
- Misc. NPC rotations have been fixed (fixes hdos/issues#792)
- Fix downgrades caused by the latest cache update

## [181] - 2021-4-23
### Fixed
- Whip idle animation now works in LMS (fixes hdos/issues#608)
- The Ancient Doors in the Legend's Quest have been corrected (fixes hdos/issues#670) 
- World Map: fix label rendering (fixes hdos/issues#479) 
- World Map: fix lag when zoomed out (fixes hdos/issues#728)
- World Map: fix Left click options (fixes hdos/issues#209)
- Seren now renders when in HD Mode (fixes hdos/issues#378)
- The Security Guard has decided to give users the key to unlock the gate (fixes hdos/issues#357)
- Seren environment has been fixed (fixes hdos/issues#582)
- Water near TOB had been upgraded to HD water (fixes hdos/issues#701)
- Trap door has been restored in Old Man Ral's Hideout (fixes hdos/issues#535)
- Troll Stronghold staircase has been fixed (fixes hdos/issues#401)
- Windows mode now saves when changed in menu (fixes hdos/issues#663)
- Lil' Creator pet should now be fixed (fixes hdos/issues#615)

## [180] - 2021-4-16
### Added
- The `:mog` command is now available to users!  
This command allows you to change into any NPC in the game!  
For example: `:mog 3127` will turn you into Jad!  
A nice free and public database for looking up NPCs is [osrsbox](https://www.osrsbox.com/tools/npc-search/)  
**Disclaimer: This command is just for fun and is expected to be glitchy and unstable, please do not report any problems**
### Fixed 
- Fixed NPCs that were downgraded due to the latest cache update (fixes hdos/issues#790)

## [179] - 2021-4-14
### Changed
- Update protocol to 195
- The Ivandis Flail has been restored, and subsequently, the Blisterwood Flail has been upgraded
### Fixed 
- Fixed the XP tracker (fixes hdos/issues#7)

## [178] - 2021-4-3
### Removed
- Due to a minor coding error, Summoning has been rescheduled to 2022
### Fixed 
- The Wizards have cleaned up the Asgarnian Ice Dungeon after an accident underneath their tower (fixes hdos/issues#680)
- Improved Unsafe Mode for NPCs (fixes hdos/issues#783)
- Duradel has been so kind as to apply a magical salve to a variety of monsters, which has calmed their animations (fixes hdos/issues#678 hdos/issues#652 hdos/issues#771 hdos/issues#661 hdos/issues#255 hdos/issues#656)
- Fixed a crash due to corrupt jagex_cl.dat files (it even crashes the official client, go figure)
- Fixed AMD detection when using ATI drivers
### Changed
- The following weapon animations have been upgraded:

    | Affected | Upgraded to |
    |------|-------|
    | Dragon Sword (special) | Vesta's Longsword (special) |
    | Dragon Warhammer (special) | Statius' Warhammer (special) |
    | Abyssal Dagger | Dragon Dagger (except stab) |
    | Elder Maul | Chaotic Maul |
    | Abyssal Bludgeon | Chaotic Maul |
    | Scythe of Vitur | Generic Spear |
    
- The following weapon models have been upgraded: 

    | Affected | Upgraded to |
    |------|-------|
    | Zamorakian Hasta    |   Zamorakian Spear   |
    | Magic Shortbow (i)   |   Magic Shortbow   |
    | Amulet of glory (5/6) | Amulet of glory |
    

- The Dharok's Greataxe running animation has been restored
- The Leaf-bladed sword have been restored
- The context cursor for 'Climb' now only shows the ladder cursor on stairs and ladders,
  in other cases (e.g. climbing rocks) it will show the agility cursor
- Tweening for some old fire animations has been disabled
- Added missing mods to the acknowledgements


## [177] - 2021-4-1
### Added
- New skill: Summoning
- Drop context cursor
### Changed
- Improved the accuracy on context cursors even more (fixes hdos/issues#782)
- The defend and jab animations for spears and halberds have been improved

## [176] - 2021-3-30
### Added
- **Auto Updating**  
    ```
    The client should now be able to auto-magically deal with random cache updates from Jagex.  
    The client will adapt itself accordingly based on detected changes, and attempt to retain  
    content integrity and upgrades. This means there should be no downtime for non-protocol updates. 
    This feature is bound to cause some new bugs, so be on the lookout! 
    ``` 
- Added Lvl-7 Enchant spell icon and cursor
- Restored remaining HD cursors
- Restored teletab teleport
### Changed
- Improved the accuracy on context cursors
- Restored the crush animation for the Zamorakian Spear
- Corrected the Defend and Smash animations for 2-handed weaponry
### Fixed
- Lots of items impersonating the pink skirt have been whipped into submission (fixes hdos/issues#283 hdos/issues#231 hdos/issues#139)
- The snow in Goblin Village has melted
- The darkness from camelot castle has been vanquished
- Context cursors now work in fullscreen (fixes hdos/issues#775)
- The wooden doors in Morton have been taught how to open properly (fixes hdos/issues#682)
- The dark energy core no longer turns incorporeal when jumping to other players (fixes hdos/issues#691)
- The leaf-bladed battleaxe animations are now HD, and no longer make you invisible (fixes hdos/issues#452)
- The black squares in the pyramid plunder entrance have been resolved (fixes hdos/issues#305)
- When using the NPC Contact spell, the dark mage no longer hides in a chest (fixes hdos/issues#517)
- The mischievous fairy that replaced the letter R with green pants in the fairy ring interface has been forced to return it (fixes hdos/issues#288)
- The blood shard has been pulled out of the shadow realm, and has taken on a new visible form (fixes hdos/issues#713)
- The correct icon in the level up window for prayer has been found in the heavens (fixes hdos/issues#673)
- Animations within interfaces now tween (fixes hdos/issues#653)
- The monkey council has banned smoke bombs, you can no longer turn invisible while using a greegree (fixes hdos/issues#675 hdos/issues#676)


## [175] - 2021-3-24
### Changed
- Update cache
- Custom cursors now persist into the entire application
### Fixed
- Various broken HD Textures have been fixed
### Added
- Restored custom context cursors

## [174] - 2021-3-17
### Changed
- Fix crashing from shooting stars (fixes hdos/issues#770)

## [173] - 2021-3-17
### Changed
- Updated cache
- The dragon pickaxe has been restored
### Fixed
- Improve hitsplat rendering (fixes hdos/issues#749)
- Exclude Texture Upscaling from textures where the results are undesired
- Exclude Tweening from animations where the results are undesired
- Fix: Dhc causes graphical stretch with armadyl platelegs (female)

## [172] - 2021-3-3
### Changed
- Update cache

## [171] - 2021-3-1
### Added
- Restore large HP bars
### Fixed
- Glitching animations introduced in v170 (fixes hdos/issues#745)
- The menu selection is now accurate (fixes hdos/issues#746)
- Fishing pools have been put back into the water (fixes hdos/issues#742)
- Goblins have rejoined the HD tribe

## [170] - 2021-2-28
### Changed
- The following UI elements have been restored
  ```
   - Game Frame
   - Choose Option (right click menu)
   - Hit splats 
   - Health bars 
   - Chat box 
   - Normal Spells 
   - Lunar Spells
   - Ancient Spells
   - Skill Icons 
   - Default Cursor (golden)

- OS Humanoid npcs now have HD animations

## [169] - 2021-2-24
### Changed
- Update protocol to 194
### Fixed
- misc. spot animations/projectiles that were wrong are now correct (fixed hdos/issues#720)
- Fixed cursed banana related issues (fixed hdos/issues#730 hdos/issues#732)
- Holding down a key bind for UI toggles no longer deadlocks the client (fixed hdos/issues#712)
- Made the lighting in the Stronghold Slayer Caves more consistent (fixed hdos/issues#724)

## [168] - 2021-2-17
### Changed
- Fix birthday event

## [167] - 2021-2-17
### Changed
- Update cache

## [166] - 2021-2-9
### Added
- Add commands 
    - :recommended - sets the graphics to a happy medium recommended settings.
    - :streaming   - sets the canvas to 720p HD screen res, and ramps up the settings.
    - :lowDetail   - sets the graphics to the lowest possible settings.
### Changed
- #### HD Pack 1
    
    This version marks our first _HD PACK_  
    HD Packs aim to *upgrade* a group of OS models with custom assets, so they align with the HD art style.   
    This HD Pack targets **Zulrah**  

    - Major effected items: 
        ```
        - Zulrah scales 
        - Magic fang
        - Trident of the seas & Trident of the swamp
        - Staff of the dead & Toxic staff of the dead
        - Tanzanite fang
        - Toxic blowpipe
        - Serpentine visage
        - Serpentine helmet
        - Magma helmet
        - Tanzanite helmet
        - Zul-andra teleport scroll
        ```
    
    - Major effected npcs:
        ```
        - Zulrah (all forms)
        - Pet Snakeling
        - Snakeling
        - Priestess Zul-Gwenwynig
        ```
    - Lighting, and environment has been added to the areas to create a more immersive feeling
- Many projectiles that were not restored, should now be HD
### Fixed
- The teleport scrolls in Zulrah Shine has returned (fixed hdos/issues#703)
- Fixed the roof in Sisterhood sanctuary(fixed hdos/issues#708)

## [165] - 2021-2-3
### Changed
- `Scape Theme` is now the default login music
- Update the cache
### Fixed
- Fix capacity check when adding to the friends or the ignore list
- Fix a crash if trying to turn Unsafe Mode on while the client is loading
- The whip has been disciplined to no longer stretch your hand off when it is idle

## [164] - 2021-1-30
### Fixed
- Fix crashing after updating from 163 (whoopsie)

## [163] - 2021-1-30
### Changed
- Increase the memory limit of the client to 512m (from 384m)
- Update the login screen sprites
- Verbose Debug will now render your current zoom
### Fixed
- Fixed the crystal ball glitching out in Varrock (fixes hdos/issues#662)
- Fixed incorrect stairs in East Ardy mansion (fixes hdos/issues#581)
- Upgrade the twisted bow (fixes hdos/issues#664)

## [162] - 2021-1-27
### Fixed
- Fixed remaining map load crashes (oopsie)

## [161] - 2021-1-27
### Fixed
- Fix peekaboo chairs in Edgeville
- Fix window mode switching
- Fix crashing near Wizard Tower

## [160] - 2021-1-27
### Changed
- Update cache
- Restore: Abyss (fixes hdos/issues#498)
- Added a check to prevent users from trying to change windows mode before the client initializes (fixes hdos/issues#238)
### Fixed
- Artifacting in interfaces caused by x16 anti-aliasing has been resolved (fixes hdos/issues#483)
- Braziers in Enakhra's Temple are now animating properly (fixes hdos/issues#402 hdos/issues#81)
- Fixed the stove in Goblin Tunnel (Observatory Quest) (fixes hdos/issues#291)

## [159] - 2021-1-22
### Changed
- Improve idle and movement animations
- Upgrade/Fixed the `Frozen abyssal whip` and `Volcanic abyssal whip`
- Restore: `Slayer Helmet`
- Rename `Hide HUD` to `Hide Interfaces`, remove `Hide` prefix.
### Fixed
- `Hide Players` now hides player overheads also (fixes hdos/issues#617)
- Fix poll booth in Edgeville (fixes hdos/issues#649)
- Fix `Abyssal Tentacle` for female players (fixes hdos/issues#647)
- Fixed the urns inside pyramid plunder (fixes hdos/issues#641)
- Fixed Shadow Giants (Ice Giants are now back to ice) (fixes hdos/issues#533)
- Players (mostly female) have visited the chiropractor again, and had their heads re-attached to their body!

## [158] - 2021-1-20
### Added
- Added "Scape Theme" to login music choices.
- The new area in Taverly (WIP)
### Changed
- Updated cache
- Upgrade/Fixed the `Abyssal Tenticle`
### Fixed
- Fixed whip attack sounds
- Varrock: fixed the floors in banks
- Camels no longer die every hit (fixes hdos/issues#624 hdos/issues#322)
- Fixed various issues with npcs and their animations (fixes hdos/issues#73 hdos/issues#142)

## [157] - 2021-1-14
### Fixed
- Fix bad deploy (POH)

## [156] - 2021-1-14
### Fixed
- Fixed 95% of Z-Fighting cases(flickering) (fixes hdos/issues#326 hdos/issues#104 hdos/issues#56 hdos/issues#40 hdos/issues#148)
- Fixed feet going through floors (fixes hdos/issues#502 )

## [155] - 2021-1-13
### Changed
- Update cache

## [154] - 2021-1-12
### Fixed
- Fixed AMD artifacts!!

## [153] - 2021-1-12
### Changed
- Upgrade idle animations (smoother)
### Fixed
- Fix text spacing (I hope)
- Slayer tower basement environment has been brightened

## [152] - 2021-1-11
### Changed
- Engine cleanup
- Updated the whip
    - Thanks to `jimqc` for finding the compatible ones
- Restore: Barbarian Assault/Outpost
- Restore: Baxtorian Falls
- Woodcutting Guild: improve path overlay
- Burgh de Rot : more accurate lighting
- Seer's Village : updated the bank floor
- Jatizo : updated floors, removed bad lights
- Al Kharid : improved flooring in the temple (full restoration on hold until open beta)
### Fixed
- Fix floating in GE
- Fix invisible model rendering (fixes hdos/issues#311 hdos/issues#509 hdos/issues#76 hdos/issues#118)
- Fixed space rendering is player names (fixes hdos/issues#309 hdos/issues#10 hdos/issues#9)
- Fixed home teleport animation (fixes hdos/issues#417)
- Fixed members world detection (fixes hdos/issues#577 hdos/issues#571 hdos/issues#60)

## [151] - 2021-1-6
### Changed
- Update cache

## [150] - 2021-1-5
### Added
- Add `Anti-Aliasing` option to Graphics
### Changed
- GL only menu options will now be disabled when not in HD mode
### Fixed
- Fix object area sound effects
- Fix a performance bug when the camera is aimed up
- Fix `Many Idle Animations` setting
- Fix castle wars bad wall on sara side.
- Fixed the under-lighting in Mor Ui Rek (inferno) (fixes hdos/issues#342)
- Fixed the pillars around the Astral rune alter  (fixes hdos/issues#341)
- Fixed the gaping holes in the wilderness agility area (fixes hdos/issues#228)
- Fixed magic carpet ride animation (fixes hdos/issues#153)
- Salarin the Twisted's cave now has walls (fixes hdos/issues#355)
- Hellcat now stays one once he gets overgrown (fixes hdos/issues#576)
- Fixed the flames in Burgh de Rott (fixes hdos/issues#338)
- Hans hands are back (fixes hdos/issues#151)
- Because we removed them from Thessalia's shop (fixes hdos/issues#150)
- Added floor overlays to the Tower of Life (fixes hdos/issues#220)
- All Lumbridge combat tutors have been upgraded (fixes hdos/issues#383)
- Lunar Isle : the pond
- Lunar Isle : repaired the roofs
- Taverly: Fixed a missing wall at fishing quest start (fixes hdos/issues#566)
- Taverly: Improved fencing (fixes hdos/issues#564)
- Taverly: Smoothed out paths (fixes hdos/issues#565)
- Taverly: Drawers now remain the same when opening and closing (fixes hdos/issues#544)
- Catherby : Improved drawers peeking through the walls (fixes hdos/issues#556)
- Catherby : Moved improperly placed stone (fixes hdos/issues#567 hdos/issues#50 )
- White Wolf Mountain : Restored south side (fixes hdos/issues#554)
- Canifis Tunnel : Gates now are HD when open (fixes hdos/issues#543)
- Lumbridge : Touched up the flour mill (fixes hdos/issues#511)
- Lumbridge : Cleaned up Hay Storage Wall (fixes hdos/issues#513)
- Lumbridge : Repaired Furnace building (fixes hdos/issues#508)
- Lumbridge : Removed random ice berg (fixes hdos/issues#493)
- Lumbridge : Restore RFD instance (fixes hdos/issues#442)
- EdgeVille : Fixed crack in floor near G.E. (fixes hdos/issues#495)
- EdgeVille : Fixed backwards door (fixes hdos/issues#461)
- Silvarea : Removed extra objects (fixes hdos/issues#541)
- Varrock : Fixed tunnels from G.E to EdgeVille (fixes hdos/issues#450 hdos/issues#226)
- Varrock : G.E. now has the proper roof texture

## [149] - 2021-1-1
### Fixed
- WASD Camera chat lock no longer applies when logging in

## [148] - 2021-1-1
### Added
- WASD Camera
### Changed
- Restore: Draynor Village (wip)
- Restore: Draynor Sewers
- Disable interaction while Free Cam is active
- Idle Logout no longer last forever, and is now limited to 15 minutes (from 5 minutes)
### Fixed
- Taverly: Fixed the skybox in the Witch's house (fixes hdos/issues#549)
- Taverly: Sanfew's table has been upgraded (fixes hdos/issues#553)
- Taverly: Fixed the carpet under the poll booth (fixes hdos/issues#552)
- Taverly: Flipped a bench to be facing the correct way

## [147] - 2020-12-31
### Fixed
- Fixed PVP priority rendering (fixes hdos/issues#337)

## [146] - 2020-12-31
### Changed
- Restore: Canifis Tunnel
- Restore: Silvarea
- Restore: H.A.M. Lair
- Restore: Taverly
- Restore: White Wolf Mountain
- Restore: Catherby
- Improve `Verbose Debug`
- Barbarian Village: fix lighting in village, add lighting to fishing spot fire.
- Lumbridge: improve the passage west of the castle
- Lumbridge: improve lighting in the church
- Improve default fog in undergroud areas to be easier to see
### Fixed
- Anti-Aliasing and the `:aa` command finally saves correctly
- Fix lag spike in some areas when users switched from SD to HD mode
- Fix H.A.M doors (fixes hdos/issues#475 hdos/issues#476)
- Freminnik Slayer Dungeon: put rocks back, fix Kurasks expansion (fixes hdos/issues#485)
- Fix broken/incorrect music tracks (fixes hdos/issues#536)
- Exclude the remaining scroll baackgrounds from texture up scaling
- Fix custom fog in dynamic regions
- Fix the gauntlet fog so its no longer bright

## [145] - 2020-12-28
### Added
- `Born Login` setting (plays "Born to do this" when in the login screen)
### Changed
**Restore Audio**

| type | total | percent |
|------|--------|---------|
| TRACKS | 523 | 100% |
| JINGLES | 273 | 100% |
| SOUNDFX | 3,664 | 95.74% |

- Improve the `:dark` command so that you can still see things
- Remove the `Cancel` option in the exit confirmation dialog
### Fixed
- Music jingles (ex: level up sounds) now work (they were never implemented!, whoopsie)
- Fix Texture3D support detection for Mac
- Improve Texture Upscale in interfaces (fixes hdos/issues#531) 
- Fix the Dharok attack animations
- Lumbridge: delete an extra piece of bridge (fixes hdos/issues#506)
- Lumbridge: fix Al Kharid gate (fixes hdos/issues#525 hdos/issues#501)

## [144] - 2020-12-24
### Fixed
 - Enable Texture3D support (it has disabled since v81, whoopsie!)  
   This greatly improves the water and lava!   

## [143] - 2020-12-24
### Fixed
- Fix specular highlighting (the 'shine' effect) in textures!
- Lumbridge: fix a `Suit of armour` in the entrance
- Lumbridge: fix the east bank booth to match the rest

## [142] - 2020-12-23
### Changed
- Restored graphics assets from [Animation Pack 1](https://runescape.fandom.com/wiki/Update:Blog_-_Animation_Update:_Pack_1)
- Improve Dragon scimitar special attack
### Fixed
- Fixed flickering problems in login screen animation
- Fixed ZGS special attack

## [141] - 2020-12-23
### Changed
- **Restored [Animation Pack 1](https://runescape.fandom.com/wiki/Update:Blog_-_Animation_Update:_Pack_1)**
    - 331 items (mostly polishing)
    - 584 animations (mostly audio improvements, and smoothing/tween)
- Restored Dragon platebody
- Restored Dagon'hai
- Restored Santa outfit
- Restored Zamorakian spear animations
- The Ghrazi rapier attack animation has been improved
- Change Debug KeyBind to `Alt D` (from `Shift D`)
### Fixed
- Lumbridge: Place the correct cannon on top of castle entrance
- Improve performance in crowded areas
- Removed carpet in the Motherlode Mine
- Fix Bank/Equipment lag (fixes hdos/issues#491)
- Varrock Sewers: Fix random torches (fixes hdos/issues#465)
- Ice Mountain restore has been put back (whoopsie) (fixes hdos/issues#464)

## [140] - 2020-12-21
### Added
- Add options: `Hide Players`, `Hide NPCs`, `Hide Graphics`, `Take Look`
### Changed
- Organized the UI to be less cluttered
- The `Take Look` option has been disabled by default,  
  and can be enabled using the new option.
- Texture Upscale no longer applies to interfaces
### Fixed
- Fix dynamic objects in restored areas
- Upscaled textures now animate at the correct speeds
- Lumbridge:
    - Fix ham trapdoor (fixes hdos/issues#467)
    - Deleted an extra wall in swamp (fixes hdos/issues#466)
    - Remove dirt over Deaths Domain (fixes hdos/issues#463)
    - Fix church door (fixes hdos/issues#462)
    - Fixed random overlay on the grass near lumby church
    - Fixed issue with a tile in basement
    - Restored the path leading to the north farm, towards draynor
    - Fixed the color of the wall surrounding wheat on the same path
    - Restore `Death to the Dorgeshuun` area under the watermill (fixes hdos/issues#459)
    - Fix chest and drawers when opened (fixes hdos/issues#458)
    - Fix ham trapdoor (fixes hdos/issues#467)
    - Fix misc. fencing to have less gaps
    - Add lights to the passage west of the castle
    - Fixed castle doors (fixes hdos/issues#457)
    - Enabled environment for basement (fixes hdos/issues#455)
    - Fixed light position at the general store
    - Upgraded the tavern (fixes hdos/issues#456)

## [139] - 2020-12-18
### Changed
- **Lumbridge has been restored!**
### Added
- Texture Upscale option (increases texture resolution)
### Fixed
- Edgeville Dungeon: improve `odd looking door` when it's opened
- Varrock Sewers: improve cave roofing near Bryophyta 
- Corp: fix atmosphere, re-enable rain
- Falador Party Room: re-enable flickering lights
- Castle Wars: torches in the waiting room are now fixed to the wall (fixes hdos/issues#411)
- Fixed Minimap in restored areas

## [138] - 2020-12-14
### Fixed
- Hotfix : fixed the red, and green marked squares floating around the map.

## [137] - 2020-12-14
### Changed
- Varrock Sewers
### Fixed
- Edgeville: fix floor near barbarian village
- You may now add the correct amount of users to your friends list in members worlds (fixes hdos/issues#380)
- Fixed issues in fally party room

## [136] - 2020-12-14
### Fixed
- Fix random sound effects in restored areas
- Edgeville Dungeon: improve `odd looking door`
- Edgeville: fix missplaced lights in platebody shop

## [135] - 2020-12-13
### Changed
**The following has been restored**
- Edgeville (wip)
- Edgeville Dungeon
- Falador Party Room (wip)
- Ice Mountain
- Black Knight's Fortress
- Barbarian Village


## [134] - 2020-12-11
### Fixed
- castle wars: fix the stairs?! (fixes hdos/issues#415)
- castle wars: fix misplaced overlays along the east pathway

## [133] - 2020-12-10
### Changed
- update the cache

## [132] - 2020-12-10
### Changed
- **The Monastery near Edgeville has been restored!** 

## [131] - 2020-12-9
### Changed
- update protocol to 193
### Fixed
- castle wars: fix balcony tile heights (fixes hdos/issues#410)
- castle wars: delete extra box above spawn rooms (fixes hdos/issues#409)
- castle wars: moved the skeleton causing teleporting issues in zammy lobby (fixes hdos/issues#412)

## [130] - 2020-12-8
### Changed
#### **CASTLE WARS HAS BEEN RESTORED!**

        This is marks our first area restore, as we begin to finish off the restoration!  
        We were designing and refining our tooling, along with engine work, as we did castle wars,  
        so this update took some time to do! Future updates should not take nearly as long.  
        Thank you for your patience!

We collected some spoiler images.  
We will replace this with a offical gallery soon!  
- [OS](https://imgur.com/Th4SN2k)
- [v129 (before)](https://imgur.com/KaNZMGZ)
- [v130 (after)](https://imgur.com/cv9BQzV)
- [2008 (goal)](https://imgur.com/F9pVyQS)

### Added
- Free Cam (Orb Of Oculus)
### Changed
- `Unsafe Mode` should now permit more things to HD.

## [129] - 2020-12-2
### Changed
- Updated cache
### Added
- Artist: Dump model tool can be toggled with `:dumpmodels` command 

## [128] - 2020-11-25
### Changed
- Updated cache

## [127] - 2020-11-23
### Fixed
- Players have visited the chiropractor again!  
  They now have their heads and ankles in the correct spots!
### Added
- Artist: The players gender can be toggled with the `:gender` command (how progressive!)
- Artist: HD models can now be hot-swapped
- Artist: HD models can now use OS textures

## [126] - 2020-11-22
### Added
- Art mode
### Changed
- Remove `Exit` option on macOS

## [125] - 2020-11-20
### Fixed
- Crossbow animations are now HD
- The MenuBar should render in the correct spot on MacOS? (hdos/issues#307)
- Keyboard shortcuts on macOS will now use the Command key. (fixes hdos/issues#308)
- Fix Tab key in Fullscreen (fixes hdos/issues#302)
- The camel's roller blades have been removed (fixes hdos/issues#317)
- Fixed straggler hand issue (fixes hdos/issues#286)

## [124] - 2020-11-18
### Changed
- Updated cache
- The `:aa` command will now be saved between restarts

## [123] - 2020-11-16
### Changed
- Set the minimum size of the client to fixed-size (765 x 503)
- The `Walk here` action will now use the tile where the menu was opened. This is to match standard client behavior. (fixes hdos/issues#289)
- General interactions will no longer verify the target is reachable. This is to match standard client behavior.
### Fixed
- The `:aa` command will now clamp to your GPUs maximum supported sampling (fixes hdos/issues#290)
- Fix linux freezing when entering HD mode on boot. (fixes hdos/issues#137)
- Fix linux 2fa prompt not displaying. (fixes hdos/issues#140)
- HP bar is now 100% accurate (fixes hdos/issues#287 hdos/issues#144)

## [122] - 2020-11-16
### Fixed
- Eadgar's Ruse: Kitchen drawers have been restored (fixes hdos/issues#280)
- You will no longer get stuck when leaving troll stronghold (fixes hdos/issues#278)
- Fixed the intended visuals of gauntlet floors. Disco room improved. (fixes hdos/issues#270)
- Skybox color has been upgraded, when going deep into the jungle (fixes hdos/issues#259)
- Fixed Father Urhney's roof (fixes hdos/issues#254)
- The pharaohs have called apon the gods (me and brain) to fix their doors (fixes hdos/issues#256)
- Fixed the icon of rock-climbing boots (fixes hdos/issues#247)
- Abyss entrances are no longer gates... (fixes hdos/issues#248)
- Fixed a crash caused by models translations within interfaces (fixes hdos/issues#284)
- Fixed animation sound effect audio (fixes hdos/issues#274)
- Many NPC's have regrown their limbs (fixes hdos/issues#257 hdos/issues#151 hdos/issues#286 hdos/issues#214 hdos/issues#212)
- Even more body parts have been acquired (fixes hdos/issues#150 hdos/issues#152)

## [121] - 2020-11-14
### Fixed
- Fixed a straggler cat
- Fixed the stairs leading to the viewing platform at Castle Wars
- World map surface now preforming much better (fixes hdos/issues#3)
- It must be May, roses have reappeared at Edge Monastery (fixes hdos/issues#264) 
- Fixed fire near Eagle' Peak Mountain (fixes hdos/issues#265) 
- Canafis floors are no longer moving (fixes hdos/issues#263)
- Fixed the missing shortcut on the north side of Yanille (fixes hdos/issues#262)


- ***AA BLOOM*** (fixes hdos/issues#107) 
    > Multisampling now work in post processing (BLOOM)

- ***FIXED PLAYER TELEPORT ON REGION LOAD***  (fixes hdos/issues#37) 
    > This issue was another allusive one!  
      Thanks to **GottaSlay** for helping us reproduce the issue consistently!

- ***AUDIO HAS BEEN FIXED!*** (fixes hdos/issues#106 hdos/issues#172) 
    > The audio engine has been reworked, and no longer persists crackling!

## [120] - 2020-11-11
### Changed
- update cache
### Fixed
- Cairn Isle cave now has an exit (fixes hdos/issues#242)
- Tomb of Rashiliyia rocks are now visible and scalable (fixes hdos/issues#241)

## [119] - 2020-11-10
### Changed
- Remove keybinds from the Help menu (fixes hdos/issues#227)
### Fixed
- Fix layered animations (fixes hdos/issues#158)
- The viewport is now fully visible in the smoke dungeon (fixes hdos/issues#223)
- Messages will now stay cleared, when hopping or relogging (fixes hdos/issues#133)
- Jungle potion cave can now be exited (fixes hdos/issues#229)
- Fally has been once again set right ((fixes hdos/issues#229) )
##### Last Man Standing
- items are now HD (fixes hdos/issues#175)
- A crash has been resolved
- fog now rendering correctly in SD mode
- fog is now working in HD mode (fixes hdos/issues#174)


## [118] - 2020-11-9
### Fixed
- Fixed spell-on-object interactions (fixes hdos/issues#191)
- you may now leave the Spirit Grotto after entering (fix hdos/issues#157)
- Fixed zulrah gnome glitching out (fix hdos/issues#161)
- Charge spell now has the correct graphic (fix hdos/issues#164)
- You may now see players underneath yourself when not self rendering (fix hdos/issues#207)
- fixed an issue with the wall kits inside the dwarven mines (fix hdos/issues#206)
- Arrows now display correctly on the ground (fix hdos/issues#211)
- Fixed an issue with rotated pillars at g.e. (fix hdos/issues#215)
- Olaf Hradson's fire is now animating as it should (fix hdos/issues#196)
- Cats in POH pet house are now upgraded (fix hdos/issues#205)
- Fixed various overlays (fix hdos/issues#169)

## [117] - 2020-11-7
### Added
- add `Draw Debug` option for verbose debug information
### Changed
- bloom will now work in dark areas just as well as light
### Fixed
##### Taverly Dungeon
    - fixed the gate leading to the Cauldron of Thunder
    - fixed the tiles inside of the black dragon area
    - fixed the agility shortcut pipe
- fixed Prifddinas portals
- wilderness building is now looking accurate
- fixed all godwars doors, they are now enterable
- necklaces may now be seen through OS bodies 
- fixed Wintertodt and corrected Vorkath
- fix full screen world map stall (fix hdos/issues#130)


## [116] - 2020-11-5
### Changed
- update cache

## [115] - 2020-11-5
### Changed
- Exiting the client will now prompt to confirm (fixes hdos/issues#135)
### Fixed
- Fullscreen now works with displays > 1080p

## [114] - 2020-11-5
### Changed
- Improve scene rendering performance (+15% FPS boost)
### Fixed
- Login screen crash has been fixed (fixes hdos/issues#131)

## [113] - 2020-11-4
### Added
- Added `Lock fullscreen` setting (fixes hdos/issues#114)
### Changed
- The client can now exit fullscreen by pressing CTRL+F 
### Removed
- Removed `Light Theme` setting keybind
- Removed `Show MenuBar` setting keybind
### Fixed
- The Stealing creation guys wanted their nets back... (fixes hdos/issues#126)
- Fix private message name spacing (fixes hdos/issues#9)
- Fix league clan chat (fixes hdos/issues#124)
- MenuBar clicking is now more responsive, and should not ignore clicks (fixes hdos/issues#125)
- KeyBinds now work when in fullscreen

## [112] - 2020-11-3
### Fixed
- **Fixed Make-X**  (fixes hdos/issues#127)

This bug was the most allusive so far!  
It's only thanks our community of testers that it was fix.  
This brings us great joy to see and experience.  
We would like to thank our testers who helped directly:  

| **TESTER** |
|----------|
|Birthday Cat|
|Surg1n|
|Ling|
|Gloc|


## [111] - 2020-11-2
### Added
- Hint arrows (fixes hdos/issues#122)
### Fixed
- Remove duplicate keybind: `Control T`

## [110] - 2020-11-2
### Added
- Add Hide Username setting
- Add Save Username setting
- The following settings will now save between restarts
  - Hide Username
  - Force HDR
  - Tween
### Fixed
- Purged pink skirt issues. All pink skirts should now be resolved. (fixes hdos/issues#99)
- Fixed trap door in HAM hideout, quest is now completable. (fixes hdos/issues#103)
- Fixed world list, now displaying the correct flag (fixes hdos/issues#100)
- Fixed invisible door issue leading into East Ardy (fixes hdos/issues#75)
- Fixed broken interface buttons (fixes hdos/issues#110 hdos/issues#64 hdos/issues#11)
- Fixed Environment in Ferox Enclave (fixes hdos/issues#115)
- Fixed Alpha issue with OS Models (fixes hdos/issues#84)
- Fixed Lumbridge basement tunnels (fixes hdos/issues#117)
- Clue Scrolls have been updated to use the new sprites over the old (fixes hdos/issues#102)
- Fixed appearance of COX door. (Click box WIP)
- Fixed Vorkath's nest flickering issue. (fixes hdos/issues#96)


## [109] - 2020-10-31
### Fixed
- Corrected the water inside of DKS and other various places
- Disable HDR in Fixed-HD window mode (temporary solution) (fixes hdos/issues#94)
- HP bar
### Changed
- Temporarily removed the waterfall in the Enchanted Valley  
- Rename `Force Tween` to `Tween` in the menu UI

## [108] - 2020-10-30
### Fixed
- Fix Zulrah atmosphere

## [107] - 2020-10-30
### Fixed
- Correct Theater Of Blood and Darkmeyer environments
- Fixed stack-able items (Fletching items [arrows/bolts] appearing as pink skirts)(fixes hdos/issues#95)

## [106] - 2020-10-29
### Added
- Add GL debug data to FPS debug
### Changed
- Collapse About menu
- Performance improvements for dense areas
### Fixed
- Remove duplicate keybindings
- Fix random crashing when teleporting from POH to GE (_finally_!)
- Fix Tutorial Island crashing (fixes hdos/issues#89)
- Fixed fairy ring interface showing pants as (r) (fixes hdos/issues#90)
- Fixed various overlays (fixes hdos/issues#91)


## [105] - 2020-10-28
### Added
- added acknowledgement section (credits) 
### Changed
- removed various unneeded logs


## [104] - 2020-10-28
### Fixed
- fix script crashing due to instruction limits.

## [103] - 2020-10-28
### Changed
- update cache

## [102] - 2020-10-28
### Added
- Force HDR option
- Unsafe operations option
- UI Tooltips
### Changed
- Force tweening will no longer apply to HD animations
### Fixed
- Tab key
- Crash screen
- Shanty pass tent 
- Fremennik Slayer Dungeon
- Misc see-through floor (mainly around Zeah)

## [101] - 2020-10-24
### Added
- Dark/Light Theme setting
- Ability to change windows modes from the toolbar
- UI Options:  
    - Bloom (HDR)
    - No Logout
    - Zoom Limit
    - Draw FPS
    - Disable HUD
    - Render Self
    - Custom Fog
### Changed
- Use Dark Theme by default in the UI
### Fixed
- JVM crash on close
- Closing is now faster and more responsive
- Discord presence should clear sooner on close
- Networking bug when opening specific web pages
- LAF not loading when ran in direct mode from the launcher (native-launcher)

## [100] - 2020-10-22
### Added
- New UI (work in progress)
### Changed
- update protocol to 192
- update launcher to v1.0.4
- update cache
- the client will now open in the center of the screen instead of the top-left
### Fixed
- memory leak whenever windows users try to stream with discord. (fixes hdos/issues#87)

## [99] - 2020-10-14
### Added
- **New Launcher!**  
Please see the [issues release](https://gitlab.com/hdos/issues/-/releases/) for more information.

## [98] - 2020-10-14
### Changed
- update cache
### Added
-further preparations for new launcher 

## [97] - 2020-10-08
### Added
- preparations for new launcher

## [96] - 2020-10-07
### Changed
- update cache

## [95] - 2020-10-05
##@ Fixed
- crashing on world hop
- crashing in LMS

## [94] - 2020-10-03
### Changed
- refine compilation output
### Fixed
- fixed floating npcs in the login screen (fixes hdos/issues#13)
- custom login responses

## [93] - 2020-09-30
### Changed
- update cache
### Fixed
- dark lighting in varrock 

## [92] - 2020-09-29
### Added
- **Mac support**
### Changed
- update jogl to 2.4.0-rc-20200429
- swap out standard water color to the lighter one.
- disable castle wars high-water-detail for now.
- improve default environment/atmosphere (ex: bright caves, wintertod)

## [91] - 2020-09-28
### Fixed
- clicked tile when in HD mode
- crashing when switching between fullscreen
- `:afk` command now works correctly

## [90] - 2020-09-26
### Added
- SD mode zoom
### Fixed
- G.e. npcs have been further upgraded

## [89] - 2020-09-24
### Added
- **Linux support**
### Changed
- update cache
### Fixed
- SD mode (_WIP, zoom next update_)

## [88] - 2020-09-22
### Added
- **hd chatheads**

### Fixed
- blue water flickering in Morytania 

## [87] - 2020-09-21
### Added
- hd interfaces

### Fixed
- z-ordering problems on interfaces

## [86] - 2020-09-20
### Fixed
- fix launcher (oopsie)

## [85] - 2020-09-20
### Added
- World hopping

### Fixed
- _white water_ when High Water Detail is enabled.
- players now have most of their limbs in the correct places!
- various floors have been fixed (Overlay/Underlay)
- the landscapers have come again, various unwanted grass objects have been removed
- fixed the door leading to West Ardy(hdos/issues#75)

### Changed
- overgrown cats have been replaced with normal cats for the time being.

## [84] - 2020-09-16
### Changed
- update cache

## [83] - 2020-09-16
### Added
- HDR (High Detail Rendering)
- Bloom (Makes things glow)
* _HDR and Bloom can be toggled using the `:hdr` command (it will be persisted)_
### Fixed
- Minimap scene icons have been restored
- Corp pet scaling issue
### Changed
- improve GL model rendering performance
- improve mipmap generation
- removed rain temporarily from corp lair

## [82] - 2020-09-13
### Fixed
- Lumbridge has landscaped its floors (no more random grass spawns)
- Saradomin Sword has been upgraded
- Saradomin GodSword has been upgraded
- Missing tiles inside Corp have been fixed
- Fixed Corp minimap icons
### Changed
- **Dragon Claws are now HD**


## [81] - 2020-09-12
### Changed
- **Corp is now HD**
### Fixed
- Lumbridge has landscaped its roof (no more random grass spawns)

## [80] - 2020-09-11
### Changed
- **God Wars (and associated items) are now fully\* restored**
- Crossbow movements are now HD
- Punching is now HD
- (*) Bloodveld, Pyrefiend, Icefiend were downgraded to fix glitches.
  They will be restored later on.

## [79] - 2020-09-10
### Changed
- **GE is now HD**
- Restore original water color
- update cache

## [78] - 2020-09-8
### Fixed
- ectophial teleport crash

## [77] - 2020-09-7
### Fixed
- home teleport animation
### Changed
- upgrade teleport animation
- Floors are now **100%** restored!

*Current Progress*:

| type | total | percent |
|------|--------|---------|
| OBJECTS | 24,589 | 96.02% |
| NPCS | 5,782 | 95.80% |
| ANIMATIONS | 6,503 | 93.75% |
| ITEMS | 11,751 | 100% |
| KITS | 152 | 100% |
| GRAPHICS | 1,120 | 94.22% |
| UNDERLAY | 97 | 100% |
| OVERLAY | 173 | 100% |


## [76] - 2020-09-5
### Fixed
- [equipment options](hdos/issues#66)
### Changed
- **MANY** graphical improvements.
This milestone marks the restoration at **95%** complete.
This does *not* include 'backports' (GE,GWD,Corp), though
they are restored to some random extents, they were not the focus
of this update. (They will all be added very soon!)

| type | total | percent |
|------|--------|---------|
| OBJECTS | 24,589 | 96.02% |
| NPCS  | 5,782 | 95.80% |
| ANIMATIONS | 6,503 | 93.75% |
| ITEMS  | 11,751 | 100% |
| KITS  | 152 | 100% |
| GRAPHICS  | 1,120 | 94.22% |

_Floors will be completed on in the next update._

If you guys want even more HD, dont forget about the `:unsafe` command.



## [75] - 2020-09-2
### Fixed
- update cache

## [74] - 2020-08-27
### Fixed
- [game links](hdos/issues#65)
- [auto chat](hdos/issues#61)
- [system update time](hdos/issues#63)

## [73] - 2020-08-26
### Fixed
- update protocol to 191

## [72] - 2020-08-24
### Added
- Discord Presence
- [`:zoom` command](hdos/issues#27). 
    This will increase the max zoom-out of the client.
    You can configure how much to increase, and reset the limit:
   - `:zoom` will increase the limit by 48 (default)
   - `:zoom X` will increase the limit by X (caped to 64)
   - `:zoom 0` will reset the limit back to normal
- error display (ex: cashes)
- `:aa` command. Set the MSAA sample buffers.
    - `:aa 0` will disable anti-aliasing
    - `:aa 1` MSAAx2
    - `:aa 2` MSAAx4
    - `:aa 3` MSAAx8
    - `:aa 4` MSAAx16
    - `:aa 5` MSAAx32
### Fixed
- feet clipping on most objects
### Changed
- HD GE NPCs

## [71] - 2020-08-22
### Added
- `:afk` command to toggle idle-logout
- `:unsafe` command to toggle unsafe animations (enables more HD, might need to re-log)
### Fixed
- fix player animations that were downgraded in v69
### Changed
- graphics backend improvements, and stability.
  This provides a wide range of upgrades to objects and npcs:
    - notable objects: fires, lights, rocks, flags
    - notable npcs: (unsafe) TzHaar, some slayer monsters, fishing pools

## [70] - 2020-08-18
### Changed
- update JOGL to 2.3.2

## [69] - 2020-08-18
### Fixed
- [invisible players](hdos/issues#12)
- [player options](hdos/issues#12)
- [misc. animations](hdos/issues#1)

## [68] - 2020-08-13
### Added
- god wars items
- spirit shields

## [67] - 2020-08-12
### Changes
- update cache

## [66] - 2020-08-12
### Fixed
- [Idle Logout Timer](hdos/issues#4)
- [Account Type Icons](hdos/issues#2)
- [item on player](hdos/issues#8)
- soundfx packet
- [follower option priority](hdos/issues#5)

## [65] - 2020-08-11
### Changes
- fix auth problems

## [64] - 2020-08-11
### Changes
- refine updating mechanisms

## [63] - 2020-08-11
### Changes
- new version control backend

## [62] - 2020-08-09
### Added
- `:issue` command to help report a issue

### Fixed
- [Tab Key](hdos/issues#6)