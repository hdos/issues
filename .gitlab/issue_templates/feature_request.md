**Note**: *Plugin requests are no longer submitted here, and are community-voted. See the Discord server for more details.*

Please check the following:

 - [ ] This feature request is not part of an external plug-in.

----

**Is your feature request or suggestion related to an already existing problem? Please describe** 
##
A clear and concise description of your feature request or suggestion e.g. HDOS should have [...]

**Additional context**  
##
Please provide screenshots or video footage of the issue described when possible. (You can drag images into Gitlab)