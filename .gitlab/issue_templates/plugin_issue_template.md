**Note**: *Plugin requests are no longer submitted here, and are community-voted. See the Discord server for more details.*
 
If you would like to request a feature, please use the **Feature request** template instead.

Please check the following:

 - [ ] This issue is not a duplicate.
 - [ ] This issue does not occur on other clients.

----

## HDOS version:
The HDOS client version you experienced your problem on.

## Plugin
List the plugin you think is the culprit.

## Describe the bug
A clear and concise description of what the bug is.

## To Reproduce
Provide a link to a live example, or clear steps to reproduce the bug. **If we cannot reproduce the bug, we cannot fix it**.

## Screenshots
Please provide screenshots or video footage of the issue described when possible. (You can drag images into Gitlab)