**Note**: *Plugin requests are no longer submitted here, and are community-voted. See the Discord server for more details.*

Please check the following:

 - [ ] This issue is not a duplicate.
 - [ ] This issue does not occur on the vanilla Oldschool RuneScape clients.

----

## HDOS version:
The HDOS client version you experienced your problem on.

## Describe the bug
A clear and concise description of what the bug is.

## To Reproduce
Provide a link to a live example, or an unambiguous set of steps to
reproduce this bug.

## Screenshots
Please provide screenshots or video footage of the issue described when possible. (You can drag images into Gitlab)